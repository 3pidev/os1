#!/bin/bash
# keycloak-import.sh

set -o errexit
set -o errtrace
set -o nounset
set -o pipefail

# If something goes wrong, this script does not run forever but times out
TIMEOUT_SECONDS=300
# Logfile for the keycloak export instance
LOGFILE=/tmp/standalone.sh.log
JSON_EXPORT_DIR=/opt/jboss/keycloak/conf/export
rm -rf ${LOGFILE}

# Start a new keycloak instance with exporting options enabled.
# Use prot offset to prevent port conflicts with the "real" keycloak instance.
#       -Dkeycloak.migration.realmName="{{realm}}" \ does noet work
timeout ${TIMEOUT_SECONDS}s \
    /opt/jboss/keycloak/bin/standalone.sh \
        -Dkeycloak.migration.action=import \
        -Dkeycloak.migration.provider=dir \
        -Dkeycloak.migration.dir=${JSON_EXPORT_DIR} \
        -Dkeycloak.migration.realmName="{{realm}}" \
        -Dkeycloak.migration.strategy=IGNORE_EXISTING \
        -Djboss.socket.binding.port-offset=99 \
    > ${LOGFILE} 2>&1 &

# Grab the keycloak export instance process id
PID="${!}"

# Wait for the export to finish
timeout ${TIMEOUT_SECONDS}s \
    grep -m 1 "Import finished successfully" <(tail -f ${LOGFILE})

# Stop the keycloak export instance
kill ${PID}
