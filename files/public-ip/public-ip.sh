#!/bin/bash
DOMAIN={{DOMAIN}}
DYNV6_TOKEN={{DYNV6_TOKEN}}
export VULTR_API_KEY={{VULTR_API_KEY}}
DDNS_SERVER=dynv6.com
DDNS_DOMAIN=dynv6.net
DDNS_NAME=${DOMAIN//./-}
DDNS_HOST="${DDNS_NAME}.${DDNS_DOMAIN}"

#Change_if needed
frp_version=0.37.1
vultr_version=2.5.2
script_version=0.1

ARCH=""
case $(uname -m) in
    i386)   ARCH="386" ;;
    i686)   ARCH="386" ;;
    x86_64) ARCH="amd64" ;;
    aarch64) ARCH="arm64" ;;
    arm)    dpkg --print-architecture | grep -q "arm64" && ARCH="arm64" || ARCH="arm" ;;
esac
PLATFORM=$(uname)

boot_script=$(cat <<-EOC
#!/bin/bash
#Version $script_version
VULTR_API_KEY=$VULTR_API_KEY
DYNV6_TOKEN=$DYNV6_TOKEN
INACTIVE_TIMEOUT={{PUBLIC_IP_TIMEOUT}}
INACTIVE_TIMEOUT=\${INACTIVE_TIMEOUT:-10}

#------------------------------
# DEFAULT CONSTANST
#------------------------------
#password for admin equals the OS1_PWD or the accesstoken
auth_pwd={{OS1_PWD}}
auth_pwd=\${auth_pwd:-$DYNV6_TOKEN}
frp_token=\$(echo -n "admin:\$auth_pwd" | base64)

#Setting for ddns service
DDNS_SERVER=dynv6.com
DDNS_DOMAIN=dynv6.net
DDNS_LOGIN=none
DDNS_HOST=\$(hostname).\${DDNS_DOMAIN}

#Get the architecture and platform of machine
ARCH=""
case \$(uname -m) in
    i386)   ARCH="386" ;;
    i686)   ARCH="386" ;;
    x86_64) ARCH="amd64" ;;
    arm)    dpkg --print-architecture | grep -q "arm64" && ARCH="arm64" || ARCH="arm" ;;
esac
PLATFORM=\$(uname)

#------------------------------
#install the ddclient
#------------------------------
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get -yq install jq ddclient

cat >  /etc/ssh/sshd_config.d/port.conf <<EOF
#Change the ssh port from 22 to 922
Port 922
EOF
systemctl reload sshd

#Create the ddclient file
cat << EOF >  /etc/ddclient.conf
daemon=350
ssl=yes
protocol=dyndns2
use=web, web=checkip.dyndns.com/
server=\${DDNS_SERVER}
login=\${DDNS_LOGIN}
password=\${DYNV6_TOKEN}
\${DDNS_HOST}
EOF

#Register the domain under dynv6.com within your account
curl -sX POST "https://\${DDNS_SERVER}/api/v2/zones" \
    -H "Authorization: Bearer \${DYNV6_TOKEN}" \
    -H 'content-type: application/json' \
    --data-binary "{\"name\":\"\${DDNS_HOST}\",\"ipv4address\":\"\$(hostname -I| awk '{print \$1}')\"}"

systemctl restart ddclient
#Force the ip address update
ddclient force

#------------------------------
#Install the frp
#------------------------------
wget https://github.com/fatedier/frp/releases/download/v${frp_version}/frp_${frp_version}_\${PLATFORM,,}_\${ARCH,,}.tar.gz
mkdir -p /etc/frp
tar -xf frp_${frp_version}_\${PLATFORM,,}_\${ARCH,,}.tar.gz --strip 1 -C /etc/frp
rm frp_${frp_version}_\${PLATFORM,,}_\${ARCH,,}.tar.gz

ln -sf /etc/frp/frpc /usr/bin/frpc
ln -sf /etc/frp/frps /usr/bin/frps
ln -sf /etc/frp/systemd/frps.service /etc/systemd/system/frps.service
ln -sf /etc/frp/systemd/frpc.service /etc/systemd/system/frpc.service
sed -i 's/nobody/root/g' /etc/systemd/system/frps.service
sed -i 's/nobody/root/g' /etc/systemd/system/frpc.service

cat > /etc/frp/frps.ini <<EOF
# [common] is integral section
[common]
# A literal address or host name for IPv6 must be enclosed
# in square brackets, as in "[::1]:80", "[ipv6-host]:http" or "[ipv6-host%zone]:80"
# For single "bind_addr" field, no need square brackets, like "bind_addr = ::".
bind_addr = 0.0.0.0
bind_port = 7990
kcp_bind_port = 7990

# udp port to help make udp hole to penetrate nat
bind_udp_port = 7991

# udp port used for kcp protocol, it can be same with 'bind_port'
# if not set, kcp is disabled in frps
kcp_bind_port = 7990

# if you want to support virtual host, you must set the http port for listening (optional)
# Note: http port and https port can be same with bind_port
vhost_http_port = 80
vhost_https_port = 443

# response header timeout(seconds) for vhost http server, default is 60s
# vhost_http_timeout = 60

# tcpmux_httpconnect_port specifies the port that the server listens for TCP
# HTTP CONNECT requests. If the value is 0, the server will not multiplex TCP
# requests on one single port. If it's not - it will listen on this value for
# HTTP CONNECT requests. By default, this value is 0.
tcpmux_httpconnect_port = 7993

# set dashboard_addr and dashboard_port to view dashboard of frps
# dashboard_addr's default value is same with bind_addr
# dashboard is available only if dashboard_port is set
dashboard_addr = 0.0.0.0
dashboard_port = 7992

# dashboard user and passwd for basic auth protect, if not set, both default value is admin
dashboard_user = admin
dashboard_pwd = \$auth_pwd

# enable_prometheus will export prometheus metrics on {dashboard_addr}:{dashboard_port} in /metrics api.
enable_prometheus = true

# console or real logFile path like ./frps.log
log_file = /var/log/frps.log

# trace, debug, info, warn, error
log_level = info

log_max_days = 3

# auth token
token = \$DYNV6_TOKEN

# only allow frpc to bind ports you list, if you set nothing, there won't be any limit
#allow_ports = 2000-3000,3001,3003,4000-50000

# pool_count in each proxy will change to max_pool_count if they exceed the maximum value
max_pool_count = 10

# max ports can be used for each client, default value is 0 means no limit
max_ports_per_client = 0

# tls_only specifies whether to only accept TLS-encrypted connections. By default, the value is false.
tls_only = false

# if subdomain_host is not empty, you can set subdomain when type is http or https in frpc's configure file
# when subdomain is test, the host used by routing is test.frps.com
#subdomain_host = $DOMAIN

EOF

#Now start the frps server
systemctl enable frps
systemctl restart frps

#Enable unattended upgrades
sed -i 's|//Unattended-Upgrade::Automatic-Reboot "true";|Unattended-Upgrade::Automatic-Reboot "true";|g' /etc/apt/apt.conf.d/50unattended-upgrades
sed -i 's|//Unattended-Upgrade::Automatic-Reboot-Time "02:00";|Unattended-Upgrade::Automatic-Reboot-Time "04:30";|g' /etc/apt/apt.conf.d/50unattended-upgrades
unattended-upgrade -v

#------------------------------
# Install vultr-cli
#------------------------------
if [ "\$(uname -m)" == "x86_64" ] && [ "\$(uname)" == "Linux" ] ; then
 curl -sfL https://github.com/vultr/vultr-cli/releases/download/v${vultr_version}/vultr-cli_${vultr_version}_linux_64-bit.tar.gz | sudo tar -xz -C /usr/bin
elif [ "\$(uname -m)" == "x86_64" ] && [ "\$(uname)" == "Darwin" ] ; then
 curl -sfL https://github.com/vultr/vultr-cli/releases/download/v${vultr_version}/vultr-cli_${vultr_version}_macOs_64-bit.tar.gz | sudo tar -xz -C /usr/bin
elif [ "\$(uname -m)" == "arm" ] && [ "\$(uname)" == "Darwin" ] ; then
 curl -sfL https://github.com/vultr/vultr-cli/releases/download/v${vultr_version}/vultr-cli_${vultr_version}_macOs_arm64-bit.tar.gz | sudo tar -xz -C /usr/bin
elif [ "\$(uname -m)" == "arm" ] && [ "\$(dpkg --print-architecture | grep -q 'arm64')" ] ; then
 curl -sfL https://github.com/vultr/vultr-cli/releases/download/v${vultr_version}/vultr-cli_${vultr_version}_linux_arm64-bit.tar.gz | sudo tar -xz -C /usr/bin
else
 echo "Not supported architecture"
 exit 1
fi

mkdir -p /root/.os1
#Create and start script to check the number of frp clients
cat > /root/.os1/public-ip.sh <<EOF
#!/bin/bash
i=0
export VULTR_API_KEY=$VULTR_API_KEY
while true ;do
  sleep 60
  count=\\\$(curl -s http://127.0.0.1:7992/api/serverinfo  -H "Authorization: basic \$frp_token" | jq -e -r ".client_counts")
  if [ "\\\$count" -eq "0" ]; then
    i=\\\$(( i + 1 ))
  else
    i=0
  fi
  if [ "\$INACTIVE_TIMEOUT" -ne "0" ] && [ "\\\$i" -ge "\$INACTIVE_TIMEOUT" ] ; then
    #Get the current host id by name
   id=\\\$(vultr-cli instance list | grep \\\$(hostname -s) | awk '{print \\\$1}')
   if [ -n "\\\$id" ] ;then
    #Destory the host
    vultr-cli instance delete \\\$id
    exit 1
   fi
  fi
done
EOF

#Now start the job
chmod +x /root/.os1/public-ip.sh
cat > /root/.os1/public-ip.service <<EOF
[Unit]
Description=Public IP Service
After=network.target

[Service]
Type=simple
User=root
Restart=on-failure
RestartSec=5s
ExecStart=/bin/bash /root/.os1/public-ip.sh
ExecReload=/bin/bash /root/.os1/public-ip.sh

[Install]
WantedBy=multi-user.target
EOF
ln -sf /root/.os1/public-ip.service /etc/systemd/system/public-ip.service
systemctl enable public-ip
systemctl restart public-ip
EOC
)
#Encode the bootscript without line breaks
boot_script=$(echo "$boot_script" | base64 -w 0)
reload=0
public_route=0
sleep_timer=60
while true ; do
  #Check if there is network, if not sleep till its there
  if ping -q -c 1 -W 1 google.com >/dev/null; then
    #Check if openresty is running,
    if systemctl is-active --quiet openresty; then
      #Get our external IP address
      my_ip="$(dig +short myip.opendns.com @resolver1.opendns.com)"
      #If external and internal version info match we assume we have a direct connection
      if [ "${PUBLIC_IP}" != "${my_ip}" ] ; then
       PUBLIC_IP=$my_ip
       #Get version info and check if it is the same
       VERSION_INFO=$(curl --connect-timeout 1 -ks https://${my_ip}/version)
       if [ "${VERSION_INFO}" == "{{SUBJECT}},v{{VERSION}}" ]; then
        #Create the DDNS pointer
        curl -sX POST "https://${DDNS_SERVER}/api/v2/zones" \
          -H "Authorization: Bearer ${DYNV6_TOKEN}" \
          -H 'content-type: application/json' \
          --data-binary "{\"name\":\"${DDNS_HOST}\",\"ipv4address\":\"${PUBLIC_IP}\"}" 2>/dev/null
        #Update the DDNS pointer
         curl -s "https://${DDNS_SERVER}/api/update?hostname=${DDNS_HOST}&token=${DYNV6_TOKEN}&ipv4=${PUBLIC_IP}" 2>/dev/null
         public_route=1
         if systemctl is-active --quiet frpc; then
           systemctl stop frpc
         fi
         sleep_timer=300
        else
         public_route=0
         sleep_timer=60
        fi
      fi
    fi

    if [ $public_route -eq 0 ]; then
      #Check if vultr-cli is install
      if [ ! -f  /usr/bin/vultr-cli ] ;then
        if [ "$(uname -m)" == "x86_64" ] && [ "$(uname)" == "Linux" ] ; then
         curl -sfL https://github.com/vultr/vultr-cli/releases/download/v${vultr_version}/vultr-cli_${vultr_version}_linux_64-bit.tar.gz | sudo tar -xz -C /usr/bin
        elif [ "$(uname -m)" == "x86_64" ] && [ "$(uname)" == "Darwin" ] ; then
         curl -sfL https://github.com/vultr/vultr-cli/releases/download/v${vultr_version}/vultr-cli_${vultr_version}_macOs_64-bit.tar.gz | sudo tar -xz -C /usr/bin
        elif [ "$(uname -m)" == "arm" ] && [ "$(uname)" == "Darwin" ] ; then
         curl -sfL https://github.com/vultr/vultr-cli/releases/download/v${vultr_version}/vultr-cli_${vultr_version}_macOs_arm64-bit.tar.gz | sudo tar -xz -C /usr/bin
        elif [ "$(uname -m)" == "arm" ] && [ "$(dpkg --print-architecture | grep -q 'arm64')" ] ; then
         curl -sfL https://github.com/vultr/vultr-cli/releases/download/v${vultr_version}/vultr-cli_${vultr_version}_linux_arm64-bit.tar.gz | sudo tar -xz -C /usr/bin
        else
         echo "Not supported architecture"
         exit 1
        fi
      fi

      #Check if there is a public server, if not create it
      vultr-cli instance list |  grep -q ${DDNS_NAME}
      if [[ $? != 0 ]]; then
        #Server not found, create it
        os_id=$(vultr-cli os list | grep "Ubuntu 21.04 x64" | awk '{print $1}')
        ssh_key=$(vultr-cli ssh-key list | grep "os1-public-ip" | awk '{print $1}')
        if [ -n "$ssh_key" ]; then
          ssh_key="--ssh-keys $ssh_key"
        fi
        script_id=$(vultr-cli script list | grep "os1-public-ip-v${script_version}" | awk '{print $1}')
        if [ -z "$script_id" ] ;then
          #There is not script so create it
          vultr-cli script create -n "os1-public-ip-v${script_version}" -t boot -s $boot_script
          script_id=$(vultr-cli script list | grep "os1-public-ip-v${script_version}" | awk '{print $1}')
        else
          #We update the script to ensure we always have the last one
          vultr-cli script update $script_id -n "os1-public-ip-v${script_version}" -t boot -s $boot_script
        fi
        vultr-cli instance create --region ams --plan vc2-1c-1gb --os $os_id --host $DDNS_NAME --label $DDNS_NAME $ssh_key --script-id $script_id
        #Wait till the server is running
        while [ "$(vultr-cli instance list | grep "$DDNS_NAME" | awk '{print $7}')" != "active" ]; do
          sleep 5
        done
        #Wait for script to finish
        sleep 20
        reload=1
      fi

      #Check if frp client is installed
      if ping -q -c 1 -W 1 $DDNS_NAME.$DDNS_DOMAIN >/dev/null; then
       if [ ! -f /etc/frp/frpc ] ;then
        wget https://github.com/fatedier/frp/releases/download/v${frp_version}/frp_${frp_version}_${PLATFORM,,}_${ARCH,,}.tar.gz
        mkdir -p /etc/frp
        tar -xf frp_${frp_version}_${PLATFORM,,}_${ARCH,,}.tar.gz --strip 1 -C /etc/frp
        rm frp_${frp_version}_${PLATFORM,,}_${ARCH,,}.tar.gz
        ln -sf /etc/frp/frpc /usr/bin/frpc
        ln -sf /etc/frp/frps /usr/bin/frps
        ln -sf /etc/frp/systemd/frps.service /etc/systemd/system/frps.service
        ln -sf /etc/frp/systemd/frpc.service /etc/systemd/system/frpc.service
        sed -i 's/nobody/root/g' /etc/systemd/system/frps.service
        sed -i 's/nobody/root/g' /etc/systemd/system/frpc.service
        systemctl enable frpc
        reload=1
       fi

        if [ -n "$(tail -1 /var/log/frpc.log 2>/dev/null | grep 'login to server failed')" ] ; then
          reload=1
        fi

        if [ "$reload" -eq 1 ]; then
        #configure the basic setting
        frp_server_ip=$(getent hosts $DDNS_NAME.$DDNS_DOMAIN | awk '{ print $1 }')
        cat > /etc/frp/frpc.ini <<EOF
[common]
# A literal address or host name for IPv6 must be enclosed
# in square brackets, as in "[::1]:80", "[ipv6-host]:http" or "[ipv6-host%zone]:80"
# For single "server_addr" field, no need square brackets, like "server_addr = ::".
server_addr = $frp_server_ip
server_port = 7990
# Protocal can be set to kcp to reduce latency but increase bandwidth
#protocol = kcp
# Enable and specify the number of connection pool
#pool_count = 1

# tcpmux_httpconnect_port specifies the port that the server listens for TCP
# HTTP CONNECT requests. If the value is 0, the server will not multiplex TCP
# requests on one single port. If it's not - it will listen on this value for
# HTTP CONNECT requests. By default, this value is 0.
tcpmux_httpconnect_port = 7993
tcp_mux = true

# console or real logFile path like ./frpc.log
log_file = /var/log/frpc.log

# trace, debug, info, warn, error
log_level = info

log_max_days = 3

# auth token
token = $DYNV6_TOKEN

[http]
type = tcp
local_port = 80
remote_port = 80

[https]
type = tcp
local_port = 443
remote_port = 443

[kubectl]
type = tcp
local_port = 6443
remote_port = 6443

[ssh]
type = tcp
local_port = 22
remote_port = 22
EOF

        #Now start the frp client
        systemctl restart frpc
        reload=0
        sleep_timer=300
      fi
     fi
   fi

   #Check if the fpc client is running if not restart it
  fi
  sleep $sleep_timer
done
