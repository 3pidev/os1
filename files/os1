#!/bin/bash
#Script to create DNS nanems and NGINX map
readonly PROGNAME=$(basename $0)
OS1_REPO="https://gitlab.com/3pidev/os1/-/raw/master"
VERSION="0.1.1"
SUBJECT=3pidev-os1
#We create a temp file to store our records
ohosts="/tmp/os1.hosts"
USAGE="Usage: $PROGNAME -iephv action
 -h show this help
 -v show version number
 -s <setting> an option for one of the modules
 -p password used for installation (can be encoded)
 -u user linux used during installation

Currently we support the following:
 hosts <domain> <default cluster>  : Find out the openstack host and add them to /etc/host
"

#We build cluster names based on enabled networks
networks=""
#List with settings
settings_list=""
settings_str=""
password=""
cmd_line=""
for i in "$@"; do 
    i="${i//\\/\\\\}"
    cmd_line="$cmd_line \"${i//\"/\\\"}\""
done
#user
user=$USER

ARCH=""
case $(uname -m) in
    i386)   ARCH="386" ;;
    i686)   ARCH="386" ;;
    x86_64) ARCH="amd64" ;;
    aarch64) ARCH="arm64" ;;
    arm)    dpkg --print-architecture | grep -q "arm64" && ARCH="arm64" || ARCH="arm" ;;
esac
PLATFORM=$(uname)

# --- Options processing -------------------------------------------
if [ "$#" -eq "0" ] ;then
    echo "$USAGE"
    exit 1;
fi

while getopts ":s:u:p:vh" optname
  do
    case "$optname" in
      "v")
        echo "Version $VERSION"
        exit 0;
        ;;
      "p")
        if [ -z "$OPTARG" ] ;then
          read -s -p "Please enter password: " password
        else
           password=$OPTARG
        fi
        if [ "${password:0:1}" == "~" ] ;then
          epassword=$(echo "${password:1}" | base64 --decode 2>/dev/null)
          if [ "$?" == "0" ] ;then
             password=$epassword
          fi
        fi
      ;;
      "u")
        user="$OPTARG"
      ;;
      "s")
        if [  -z "$settings_list" ]; then
          settings_list="$OPTARG"
          settings_str="-s $OPTARG"
        else
          settings_list="${settings_list} $OPTARG"
          settings_str="$settings_str -s $OPTARG"
        fi
        ;;
      "h")
        echo "$USAGE"
        exit 0;
        ;;
      ":")
        echo "No argument value for option $OPTARG"
        exit 0;
        ;;
    esac
  done
shift $(($OPTIND - 1))


# Function to get value from settings list
setting=""
setting_count=0
get_setting() {
  setting="" #Set the global value to empty
  local arr=(${settings_list})
  local i
  local values=""
  local found=0
  for i in "${arr[@]}" ;do
    local kv=(${i//=/ })
    if [ "$kv" == "${1}" ] ;then
      found=1
      if [ -z "$values" ] ;then
      values="${kv[1]}"
      else
        values="${values} ${kv[1]}"
      fi
    fi
  done
  setting=$(echo $values | sed 's/,/ /g')
  setting_count=0
  for i in $setting ;do
    setting_count=$((setting_count + 1 ))
  done
  if [ "$found" -eq "0" ] ;then
    return 1
  fi
  return 0
}

#Add Replace the etc/hosts section
update_dns_hostfile(){
  sudo sed -i '/# Begin OpenStack/,/# End of OpenStack/d' /etc/hosts
  echo "# Begin OpenStack" | sudo tee -a /etc/hosts >/dev/null
  if [ -f "$ohosts" ] ;then
    cat "$ohosts" | sudo tee -a /etc/hosts > /dev/null
  fi  
  echo "# End of OpenStack" | sudo tee -a /etc/hosts >/dev/null
  #We also need to restart dnsmasq
  sudo systemctl restart dnsmasq
  #We also need to restart the reverse proxy
  sudo systemctl restart openresty
}

find_openstack_hosts(){
    #Check if openstack is installed
    if [ -z "$(snap aliases 2>/dev/null | grep openstack | awk {'print $1'})" ]; then
      return 1
    fi
    domain=${1:-$DOMAIN}
    defCluster=${2:-local}
    local server_ip=$SERVER_IP
    #Find all networks that are enabled
    for network in $(openstack network list -c ID -f value --enable)
    do
     eval $(openstack network show $network -f shell -c name -c subnets)
     networks="${networks} ${name}"
     #make sure network name is unset
     unset $name
    done

    #Find all servers and create host file
    for server in $(openstack server list -c ID -f value --all)
    do
        # the -f shell option sets environment variables
        eval $(openstack server show $server -f shell -c name -c addresses -c OS-EXT-SRV-ATTR:host)
        #Replace the external network
        eval $(echo $addresses | sed 's/10.20.20/external=10.20.20/g' | sed 's/,//g')
        for network in $networks
        do
          if [ -n "${!network}" ] ;then
            #Remove network from name because it will be added later
            local shortname=$(echo "$name" | cut -f1 -d".")
            #For external network we point the domain name to the proxy_server
            if [ "$network" == "external" ]; then
              local cluster=""
              if [[ $shortname = *0* ]] ;then
               cluster="$(echo $shortname | cut -f1 -d'0').$network"
               #check if we should add the default cluster
               if [ "$(echo $shortname | cut -f1 -d'0')" == "$defCluster" ];then
                cluster="default.$network $cluster"
               fi
              fi              
              echo "${!network} $shortname.$network $name.$network $shortname.$domain $name.$domain $cluster" >> $ohosts
            else  
              echo "${!network} $shortname.$network" >> $ohosts 
            fi

            unset $network
          fi
        done

        #os_ext_srv_attr_host is host where node is running, should we add routing ?
    done

    #TODO find solution to add CTRL0/Node0 to host file

    if [ -f "$ohosts.old" ] ;then
     #compare if file are different
     if cmp -s "$ohosts" "$ohosts.old" ; then
       #Same no action
       :
     else
       update_dns_hostfile
     fi 
    else
      update_dns_hostfile
    fi

    if [ -f "$ohosts" ] ;then
     sudo mv -f $ohosts $ohosts.old
    fi
    
    #Clean out unallocated floating IPs
    for ip in $(openstack floating ip list  --status 'DOWN' -c ID -f value)
    do
      local ipdate=$(openstack floating ip show $ip -c created_at -f value)
      #Only relase ip which is 5 min unallocated
      ipdate=$(( (`date +%s` - `date +%s -d $ipdate`) > (5*60) ))
      if [ "$ipdate" == "1" ] ;then
         openstack floating ip delete $ip
      fi   
    done 
}

#Check what we should do
case "$1" in

  "hosts") 
    LOCK_FILE=/tmp/$SUBJECT.lock
    if [ -f "$LOCK_FILE" ]; then
      echo "Hosts is already running"
      exit 0
    fi

    #Create lockfile and temp directory
    trap "rm -rf $LOCK_FILE" EXIT
    touch $LOCK_FILE
    find_openstack_hosts $2 $3
  ;;
  
  "update")
   cmd_line="bash <(curl -sfLN \"$OS1_REPO/run.sh\") $settings_str install os1"
   bash -c "$cmd_line" &>/dev/null
  ;;
  
  #Default we will run the installer program
  *) 
   cmd_line="bash <(curl -sfLN \"$OS1_REPO/run.sh\") $cmd_line"
   bash -c "$cmd_line"
  ;;


esac
#Exit without error
exit 0