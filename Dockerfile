FROM alpine:latest
#Install kubectl
RUN apk --no-cache add curl jq ca-certificates openssl bash && \
    curl -Lo /usr/local/bin/kubectl "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl" && \
    chmod +x /usr/local/bin/kubectl && \
    mkdir -p /bin/scripts/mod &&\
    wget https://github.com/mikefarah/yq/releases/download/v4.4.1/yq_linux_amd64 -O /usr/bin/yq && \
    chmod +x /usr/bin/yq && \
    curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 && \
    chmod +x get_helm.sh && \
    ./get_helm.sh && \
    rm get_helm.sh

#Copy scripts
COPY ./run.sh /bin/scripts
COPY ./mod/constants.mod ./mod/templater.mod ./mod/functions.mod /bin/scripts/mod/
ENV RAW_REPO=/bin/scripts
ENTRYPOINT ["/bin/bash"]
CMD ["/bin/scripts/run.sh","drone_io"]
