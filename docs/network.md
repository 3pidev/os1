# Configure the EdgeRouter X

Connect you PC to new EdgeRouter port eth1, static ip 192.168.1.1 (setup pc static ip 192.168.1.2) and > login using ubnt/ubnt

Configure DHCP router 10.0.1.1 / 255.255.255.0 and change password 
Set DHCP host range from 10.0.1.100-10.0.1.150 allowing 51 DHCP clients
Within Server section set the hostname (OS1) and domain name (NL) 

![network-setup](img/network-setup.png)
![Network DHCP](img/network-dhcp.png)

## Add a Static route for OpenStack brigde
Because we will install OpenStack (by installing MicroStack) we will create a bridge network on the
Control (n0) host and therefor we need to add the route to the external network(10.20.20.0/24)
of openstack. Within the Routes click on Add static route and add gateway

![network gateway](img/network-gateway.png)

## Route All traffic to Control host
We are making the n0 host the main entry point for our cloud and therefor route all trafic to the host, by adding NAT firewall rule: 22,80,443,1000-10000 to 10.0.1.10. Go into Firewall/NAT and setup the port forwarding

![network nat](img/network-nat.png)

## External Routing
Setup an external domain in our case os1.nl to end up at your EdgeRouter, by creating a forwarding of NAT rule on you internal router.

## Name Server
Setup the internal nameserver allowing the nodes to find each other
<TODO: Add Picture>