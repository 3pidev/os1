# Installing ubuntu on ChromeBox
I had problems with the installer of the ubuntu live cd and therefor use the ubuntu image from pendrive

* Make a bootable usb with ubuntu and install it on all devices 
* Machines are called **n**[00-99] compute nodes.
* During the installation process you will be asked to enter user name and password.
* Enable SSH Server, Base Server at the end of the installation

To be able to connect to your ChromeBox login to the EdgeRouter and find the IP address within the services section. 

## Assigning the correct IP address
Login to you EdgeRouter UI and on the service tab click actions->edit and on the lease tab make the IP static for the nodes you want. After change unplug and plug Network cable to assign new IP. Machines are called n[0-9]  nodes, control nodes have ip 10.0.1.10-49

![Network static](img/network-staticip.png)

## Enabling Easy login from you local machine
We assume you have a local machine that is connected to the EdgeRouter directly. To make you live easier during installation we copy our SSH keys to ctrl0. If you have not yet create a ssh key set on you pc please rune ssh-keygen first

```
ssh-copy-id <your-user>@<your-dhcp-ip-op-edgerouter>
```

Now login using ssh **<your-user>@<your-dhcp-ip-op-edgerouter>** without supplying a password. 
 
