# Reverse Proxy
We setup a reverse proxy on the main host allowing to redirect the all trafic to the correct host. You can run the installation automaticly


```
sh <(curl -sfL https://gitlab.com/3pidev/k3s/-/raw/master/os1.sh) \
   -s domain=<your domain>,email=<your domain email> \
    reverse-proxy
```

## Instal nginx
We install nginx as webproxy and remove the default web pages

```
sudo apt-get install -y nginx
sudo mkdir -p/etc/nginx/rproxy/http/available 
sudo mkdir -p/etc/nginx/rproxy/http/available/enabled
sudo mkdir -p/etc/nginx/rproxy/stream/available stream/enabled
sudo mkdir -p/etc/nginx/rproxy/stream/enabled
#Remove the nginx sites and add streams and proxys 
sudo sed -i '/\/etc\/nginx\/sites-enabled\/*/c\#REMOVED#  include \/etc\/nginx\/sites-enabled\/*;' /etc/nginx/nginx.conf
sudo sed -i '/#REMOVED#/a   include /etc/nginx/rproxy/http/enabled/*.conf; \
} \
stream {  \
    include /etc/nginx/rproxy/streams/enabled/*.conf;'  /etc/nginx/nginx.conf
sudo ln -s /etc/nginx/rproxy/http/available/*.conf /etc/nginx/rproxy/http/enabled
sudo ln -s /etc/nginx/rproxy/stream/available/*.conf /etc/nginx/rproxy/stream/enabled
```

## Installing Certificates
We use certbot to get wildcard certificates for our domain
```
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot
sudo certbot --server https://acme-v02.api.letsencrypt.org/directory -d "os1.nl,*.os1.nl" --manual --preferred-challenges dns-01 certonly
sudo certbot --server https://acme-v02.api.letsencrypt.org/directory -d "*.dev.os1.nll" --manual --preferred-challenges dns-01 certonly
sudo certbot --server https://acme-v02.api.letsencrypt.org/directory -d "*.tst.os1.nl" --manual --preferred-challenges dns-01 certonly
sudo certbot --server https://acme-v02.api.letsencrypt.org/directory -d "*.acc.os1.nl" --manual --preferred-challenges dns-01 certonly
sudo certbot --server https://acme-v02.api.letsencrypt.org/directory -d "*.yan.os1.nl" --manual --preferred-challenges dns-01 certonly
```


## Install the proxies services
We can now copy all proxies and the default web page


## Enable the all proxies
Make all config proxies enabled

```
sudo ln -s /etc/nginx/rproxy/http/available/*.conf /etc/nginx/rproxy/http/enabled
sudo ln -s /etc/nginx/rproxy/stream/available/*.conf /etc/nginx/rproxy/stream/enabled
#To test changes run: nginx -T
sudo systemctl restart nginx
```
