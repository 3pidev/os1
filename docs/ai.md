# Configure the Jetson NX en Nano in the Jeston Mate

1. Download the image for NX: https://developer.nvidia.com/jetson-nx-developer-kit-sd-card-image
1. Download the image for Nanno:
1. Flash the images to the SD with Etcher: https://www.balena.io/etcher repeat for all disks.
1. Insert the SD card into the master (first slot), connect screen and keyboard and boot and complete the gui-setup; 
* For NX (accept,english,english(us),YourTime Zone,"user:ubuntu,name:jai[0-9],ubuntu,pwd,pwd",accept the max disk size,power 20W-6Core)
* For Nanno
5. Login; open terminal
```bash
#Allow todo sudo without password %sudo ALL=(ALL:ALL) NOPASSWD:ALL
sudo sed -i "s/%sudo.*/%sudo ALL=(ALL:ALL) NOPASSWD:ALL/g" /etc/sudoers
#Update and install packages
sudo apt update -y && sudo apt upgrade -y && sudo apt autoremove -y
sudo apt install curl apt-utils usbmount nfs-kernel-server  -y 

#Configure NFS
#https://www.server-world.info/en/note?os=Ubuntu_18.04&p=nfs&f=1


#Change to multi user mode
sudo systemctl set-default multi-user.target
#Install the auto-update
bash <(curl -sfLN get.os1.nl) install auto-update
#Fix docker to use nvidia as default
sudo bash -c 'cat > /etc/docker/daemon.json <<EOF
{
    "default-runtime" : "nvidia",
    "runtimes": {
        "nvidia": {
            "path": "nvidia-container-runtime",
            "runtimeArgs": []
        }
    }
}
EOF'
sudo systemctl daemon-reload && sudo systemctl restart docker

#We need to fix the /etc/resolv.conf because it add by default the search domain of local network
sudo rm -rf /etc/resolv.conf
sudo systemctl restart NetworkManager

#Install K3s Master or Slave
## K3s Master
REPO="k3s-io/k3s"
K3S_VERSION=$(curl --silent "https://api.github.com/repos/$REPO/releases/latest" |grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/' )
#On current server install k3s master, docker required for 
mkdir -p $HOME/.kube/
curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="server --docker --node-label=gpu=nvidia --write-kubeconfig-mode 644 --write-kubeconfig $HOME/.kube/config" INSTALL_K3S_VERSION=${K3S_VERSION} sh -
#sudo cp /etc/rancher/k3s/k3s.yaml .kube/config && sudo chown -R $USER.$USER .kube
#Enable CUDA, https://blogs.windriver.com/wind_river_blog/2020/06/nvidia-k8s-device-plugin-for-wind-river-linux/
cat <<EOF | kubectl apply -f -
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: nvidia-device-plugin-daemonset
  namespace: kube-system
spec:
  selector:
    matchLabels:
      name: nvidia-device-plugin-ds
  updateStrategy:
    type: RollingUpdate
  template:
    metadata:
      # This annotation is deprecated. Kept here for backward compatibility
      # See https://kubernetes.io/docs/tasks/administer-cluster/guaranteed-scheduling-critical-addon-pods/
      annotations:
        scheduler.alpha.kubernetes.io/critical-pod: ""
      labels:
        name: nvidia-device-plugin-ds
    spec:
      tolerations:
      # This toleration is deprecated. Kept here for backward compatibility
      # See https://kubernetes.io/docs/tasks/administer-cluster/guaranteed-scheduling-critical-addon-pods/
      - key: CriticalAddonsOnly
        operator: Exists
      - key: nvidia.com/gpu
        operator: Exists
        effect: NoSchedule
      # Mark this pod as a critical add-on; when enabled, the critical add-on
      # scheduler reserves resources for critical add-on pods so that they can
      # be rescheduled after a failure.
      # See https://kubernetes.io/docs/tasks/administer-cluster/guaranteed-scheduling-critical-addon-pods/
      priorityClassName: "system-node-critical"
      containers:
      - image: 3pidev/k8s-device-plugin:latest
        name: nvidia-device-plugin-ctr
        securityContext:
          allowPrivilegeEscalation: false
          capabilities:
            drop: ["ALL"]
        volumeMounts:
          - name: device-plugin
            mountPath: /var/lib/kubelet/device-plugins
      volumes:
        - name: device-plugin
          hostPath:
            path: /var/lib/kubelet/device-plugins
EOF

#Test
cat <<EOF | kubectl create -f -
apiVersion: v1
kind: Pod
metadata:
  name: gpu
spec:
  restartPolicy: Never
  containers:
    - name: gpu
      image: "nvidia/cuda:11.4.1-base-ubuntu18.04"
      command: [ "/bin/bash", "-c", "--" ]
      args: [ "while true; do sleep 30; done;" ]
      resources:
        limits:
          nvidia.com/gpu: 1
EOF

cat <<EOF | kubectl create -f -
apiVersion: v1
kind: Pod
metadata:
  name: jetson-tf
spec:
  restartPolicy: OnFailure
  containers:
  - name: nvidia-l4t-ml
    image: "nvcr.io/nvidia/l4t-ml:r32.7.1-py3"
    command: [ "/bin/bash", "-c", "--" ]
    args: [ "while true; do sleep 30; done;" ]
    resources:
      limits:
        nvidia.com/gpu: 1
EOF

## Configure a USB drive
$mkfs.ext4 /dev/sda1
mkdir -p /mnt/data
blkid
#Change the UUID below
cat >> /etc/fstab <<EOF
UUID="103fb93f-99af-4def-a476-205000033f3e" /mnt/data ext4 defaults 0 0
EOF
mount -a
#We will local-path change from SD tot USB
mkdir -p /mnt/data/local-path
#We create share directory
mkdir -p /mnt/data/shared
#We create backup directory
mkdir -p /mnt/data/backup

## K3s Slave
#On  jai0 run: sudo cat /var/lib/rancher/k3s/server/node-token
export k3s_token=
REPO="k3s-io/k3s"
K3S_VERSION=$(curl --silent "https://api.github.com/repos/$REPO/releases/latest" |grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/' )
export k3s_url="https://jai0:6443"
#On current server install k3s master, docker required for 
curl -sfL https://get.k3s.io | K3S_URL=${k3s_url} K3S_TOKEN=${k3s_token} INSTALL_K3S_EXEC="agent --docker --node-label=gpu=nvidia" INSTALL_K3S_VERSION=${K3S_VERSION} sh -

#Reboot the system
sudo reboot
``` 

1. Join the ai cluster to rancher

```bash
#Run the join command for rancher

#Fix bug if  names are not resolved correctly
#search dnsPolicy: ClusterFirst and replace with: dnsPolicy: Default using: kubectl edit deployment cattle-cluster-agent -n cattle-system
kubectl patch deployment cattle-cluster-agent -n cattle-system -p '{"spec": {"dnsPolicy": "Default"}}'
``` 

#Helm is required for Jhub
sudo snap install helm --classic

#Add the repo jupyterhub
helm repo add jupyterhub https://jupyterhub.github.io/helm-chart/
helm repo update

#Jupyter hub
DOMAIN=os1.nl
clientSecret=
secretToken=$(openssl rand -hex 32)
cat > jhub.yaml <<EOF
proxy:
    secretToken: "$secretToken"
    service:
        type: ClusterIP
ingress:
    enabled: true
    hosts:
        - jhub.ai.$DOMAIN
singleuser:
    defaultUrl: "/lab"
    extraEnv:
      JUPYTERHUB_SINGLEUSER_APP: "jupyter_server.serverapp.ServerApp"
    image:
      name: jupyter/scipy-notebook
      tag: latest
hub:
  config:
    GenericOAuthenticator:
      client_id: 'os1'
      client_secret: '$clientSecret
      oauth_callback_url: https://jhub.ai.$DOMAIN/hub/oauth_callback
      authorize_url: https://sso.$DOMAIN/auth/realms/master/protocol/openid-connect/auth
      token_url: https://sso.$DOMAIN/auth/realms/master/protocol/openid-connect/token
      userdata_url: https://sso.$DOMAIN/auth/realms/master/protocol/openid-connect/userinfo
      login_service: keycloak
      username_key: preferred_username
      tls_verify: false
      userdata_params:
        state: state
    JupyterHub:
      authenticator_class: generic-oauth
EOF
  helm upgrade jhub jupyterhub/jupyterhub  --values jhub.yaml --namespace jhub --create-namespace --cleanup-on-fail 

#kubectl port-forward deployment/proxy 8080:8000 -n jhub >/dev/null 2>&1 &


# Mac Tensor
pip install https://storage.googleapis.com/tensorflow/mac/cpu/tensorflow-1.14.0-py3-none-any.whl


#See all GPU
kubectl describe nodes  |  tr -d '\000' | sed -n -e '/^Name/,/Roles/p' -e '/^Capacity/,/Allocatable/p' -e '/^Allocated resources/,/Events/p'  | grep -e Name  -e  nvidia.com  | perl -pe 's/\n//'  |  perl -pe 's/Name:/\n/g' | sed 's/nvidia.com\/gpu:\?//g'  | sed '1s/^/Node Available(GPUs)  Used(GPUs)/' | sed 's/$/ 0 0 0/'  | awk '{print $1, $2, $3}'  | column -t




#Use USB as local path
cat << EOC | kubectl replace -f -
kind: ConfigMap
apiVersion: v1
metadata:
  name: local-path-config
  namespace: kube-system
data:
  config.json: |-
    {
      "nodePathMap":[
        {
            "node":"DEFAULT_PATH_FOR_NON_LISTED_NODES",
            "paths":["/mnt/data/local-path"]
        }
      ]
    }
EOC
EOF
