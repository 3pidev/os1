# Configuring a Chromebox as normal machine

Open the box and remove the big screw above the memory by sticker (REMOVING WRITE PROTECT FIRMWARE)
![Removing Write Protect](img/chromebox.jpg)
* Close the box
* Push Paperclip(RECOVERY MODE) in hole above kabel lock and power on,
* On screen press CTRL+D
* Confirm by pushing paperclip(RECOVERY MODE)
* On screen press CTRL+D
* Wait till main screen

## Enter Developer mode
Within the developermode we will flash ChromeBox bios with one which supports harddisk boot
* ALT+CTRL+F2
* Login - chronos
* cd; curl -LO mrchromebox.tech/firmware-util.sh && sudo bash firmware-util.sh
* Option 2: Firmware, I ACCEPT, I UNDERSTAND, Y, N, return
* Reboot

Now you have a fully working PC and need to install linux [Ubuntu](ubuntu.md)