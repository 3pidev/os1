# External domain access
We want to make services available from the outside and therefor need to configure external DNS and internal forwarding. You can do this on your local network but we wanted our solution to be indepent of the internal network we use and therefor created a solution which uses an external host and a tunnel.

## Wildcard Domain
During this installation we will create a number of Web Applications, who will be accessible by name only. This mean we need to buy/setup a domain name (ex: os1.nl). Within the DNS settings of you domain provider please point a wildcard towards the IP adress of your cluster-master or load balancer. In case you use our solution for a public ip address you can add a CNAME towards you DDNS name.

## Creating a dynamic domain name pointing to our public IP
First we created a free account at [dynv6](https://dynv6.com/) and setup a ddns entry for our server (os1-nl.dynv6.net). Our services will be available on this address and you can use a CNAME in your domain registar to link your custom domain (in our case os1.nl). Copy your API key under your user-settings tokens and place it in the ~/.os1/config as DYNV6_TOKEN='your token' or pass it during installation with the -v option

## Create a public cloud server
We use [vultr.com](https://www.vultr.com/?ref=8559229) to host our public IP, when you would be running this config the public IP cost 5Euro a month. The way we have created the scripts public machines without a client will be destoryed after 10 min of inactivity.
You will need to create an account first and activate you API key and allow any IP4 and IPV6 server to use it. Save you api key in the ~/.os1/config as VULTR_API_KEY='your api token' or pass it during installation with the -v option

If you want a different provider you will need to change the /root/.os1/public-ip.sh script to match your cloud provider.

# Install
To install a public IP just run **os1 install public-ip** and a public server will be created base on your domain-name, a ddns-name will be create base on you domain name. Now when you start you local server it will check if the public server is there if not it will created it. 10 minutues after all your local client has stop the public server will be destoryed.

When you also installed openresty and reverse-proxy you will be able to see the frp admin console on https://frp.yourdomin and login with the admin and your overall choosen admin password
