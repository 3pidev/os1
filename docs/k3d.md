# K3D support on localhost
You can install K3D on the local server is it is big enough and you require additional kubernetes engines to play.

Just run **os1 install k3d**

To create a server run
```bash
port="01"
# create a local cluster
k3d cluster create ${port} -p "89${port}:80@loadbalancer"
k3d kubeconfig get ${port} > ~/.kube/config-k3d-${port}
#Load the kubectx list
source .bash_aliases
#TODO register the cluster
```

You can switch to the server using **kubectx k3d-\${port}** to activate the kubectl or helm on this cluster.
When using the proxy you can access the cluster using **.k3d-\${port}.\${domain}**