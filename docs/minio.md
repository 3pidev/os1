# Configure Minio for SSO 

Go into keycloak and assure that the user you want to login with as the attribute **minio** and one of the values below.

* **consoleAdmin**: grants access to the configuration of the MinIO instance
* **readonly**: grants read-only permissions to all objects
* **readwrite**: grants read and write permissions for all buckets and objects
* **diagnostics**: grants permission to read diagnostic logs
* **writeonly**: provides write-only permissions to buckets and objects, but prevents reading and listing of objects or buckets

Login to minio with the default admin and password and configuration -> Identity OpenID and fill the information as shown below. The Secret ID needs to be copied form keycloak

![Minio Config](img/minio.png)