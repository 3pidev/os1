# Rancher Kubernetes plugin for drone.io
#### [Docker Repository on Docker Hub](https://hub.docker.com/repository/docker/3pidev/drone-rancher-os1)

This plugin allows to perform deployments and other actions on your Rancher Kubernetes environment. 
It is build for the OpenStackOne project but can also be used on other Rancher servers

## Usage

This pipeline will apply a yaml file using kubectl.

```yaml
---
kind: pipeline
type: kubernetes
name: deploy

trigger:
  branch:
    exclude:
    - master
steps:
  - name: deploy
    image: 3pidev/drone-rancher-os1
    pull: always  #For testing
    settings:
      project: Hello
      namespace: hello
      cluster: dev
      token:
        from_secret: rancher_token
      before:
        - echo "Hello $$VALUE"
      yaml: deployment.yml
      after:
        - echo "$${GREEN}After Hello$${NC}"
    environment:
      - VALUE=world
```

## Secrets
- token: `A rancher token valid to perform actions on cluster`
- user: `NOT ADVICED: Username for login on cluster`
- password: `NOT ADVICED: Password for login on cluster`

## OS1 Default Enivronment variables
Within the OpenStackOne project we use default variables, which you will need to over rule using them. You can also use helm commands and we
download the correct kubeconfig file for the cluster and make it avaialble using the global kubeconfig varaible.

```yaml
settings:
    project: Hello       #Name of the project within Rancher [defaults to default]
    namespace: hello     #The default name space to use [defaults to project]
    namespaces:
      - another          #A list of other namespaces to create
    cluster: dev         #The clustername to used [required]
    token:               #The token used to login [strongly adviced]
      from_secret: rancher_token
    before:              #List of command to execute before yaml files
      - echo "Hello $$VALUE"
    yaml:                #List of yaml file to be applyed
      - deployment.yml 
    after:               #List of commands to be executed after yaml files
      - echo "$${GREEN}After Hello$${NC}"
    configfile: confg.sh #Bash file with additional varaible definitions used in templating
envrionment:
  - DOMAIN=os1.nl        #domain name for rancher server
  - RANCHER_SERVERNAME=rancher #name for rancher server
```

## Templating YAML files

For all *yaml* template files we use replacement of all **{{VAR}}** by the **$VAR** values before applying it using kubectl

## Internal functions
- rancher_cluster_ready *clustername* `:Checks if cluster is ready`
- rancher_project *project* `:Will lookup/create a project, returning projectid`
- rancher_project_namespace *project* *namespace* `:Will lookup/create a namespace within project, returning namespaceid`
- rancher_add_user *user* *description* *password* *changeonlogin=true* *userrole=user* `:Create a user`
- rancher_add_role *user* *clustername* *role=cluster-member* `:Will add role to user on specified cluster`
