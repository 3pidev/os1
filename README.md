# OS1 - Open-Source-One

For development purposes we created a datacenter in a box based on OpenNebula or OpenStack and Rancher we call Open-Source-One or in short os1. To keep the cost down (<1.500Euro) we have reused 4 x Chromeboxes(i7,16Gb,128Gb) as our servers and one EdgeRouter-X. In the picture below we give a highlevel insight in the [architeture](docs/architecture.md) we deploy.

![opensourceone](docs/img/opensourceone.png)

## Preparations
Before we can begin, we need to do some hardware configuration. 
* [Setup Network on EdgeRoute-X](docs/network.md)
* [Enable ChromeBox to boot Linux](docs/chromebox.md)
* [Install Ubuntu](docs/ubuntu.md)

## Other documentation
The following documents can help you with more advanced cases
* [Understanding the Architecture](docs/arcihtecture.md)
* [CI/CD Deployment using Drone.io](docs/drone-rancher-os1.md)
* [Configure Public Domain and Public IP](docs/public-ip.md)

# Installing OpenNebula/OpenStack and Rancher

We assume from this point on that you have install ubuntu on your bare-metal machines, did the setup of edgerouter-x and optionally configured the mango. Login to you ctrl0 node and start the installation and configuration. You can adjust the packages or just run the default installation

When not already done during ubuntu configuration
```
#Run on the control node and create config directory
ssh-copy-id ubuntu@os1.nl
ssh ubuntu@os1.nl
#Make sure we have te latest version
sudo apt update 
sudo apt upgrade -y
#Reboot your system afterwards
mkdir -p ~/.os1
```

```bash
#Run on the control node, just installing OS1 script
bash <(curl -sfLN get.os1.nl) install os1
```

```bash
#Installing single node machine, with public-ip and the some base functions
#get.os1.nl = https://gitlab.com/3pidev/os1/-/raw/master/run.sh
cat > ~/.os1/config <<EOF
DOMAIN=os1.nl
OS1_PWD=$(echo -n "$USER@$HOST$SERVER_IP" | md5sum | cut -d ' ' -f 1)
SECRET_KEY=$(echo -n "$USER@$HOST$SERVER_IP" |  shasum -a 256 | cut -d ' ' -f 1)
EOF
export $(cat ~/.os1/config | sed 's/#.*//g' | xargs)

bash <(curl -sfLN get.os1.nl) \
-p '<password for sudoers>' \
-s local=true
-s VULTR_API_KEY=<API key of Vutlr.com>\
-s DYNV6_TOKEN=<API key of DYNV6.com>\
install sudoers os1 kernel docker dns keycloak minio rancherd openresty proxy public-ip welcome
```

During installation, you can decide which software packages you would like to install. All packages marked with "default" are inluded when you run install. You can add addtional packages/group by using the include-option `*-i*` or exclude by `*-e*`. All options in the table below are optional, defaults are set in the module [constants.mod](mod/constant.mod) but can be overrulled using the `*-s*` or `*-p*`. Passwords can be encoded using the encode option.

Also we install a script called **os1** which is shorthand for the bash command above, but also contains some operational commands.

## Adding a cluster
We have provide a example scripts (dev-cluster) to install a development Kubernetes cluster (dev) into you created openstack environment and register it in the [rancher](https://rancher.os1.nl) hosts.

```bash
#Run on the control node, to install an dev cluster
os1 install dev-cluster
```

By default the proxy we use will filter the cluster-name from requesturl allowing you to use the same domain inside the different clusters allowing easy migration of scripts. To explain ***.dev.os1.nl** will be routered to the **dev cluster** inside the cluster you can just use the ***.os1.nl** domain. You can set default_cluster (=local), which will route all *.os1.nl trafic to the specified cluster.


```bash
#Run on the control node, to change you default cluster
os1 -s default_cluster='<your clustername>' update
```

## Admin password
The admin password and url's can be display using the following command

```bash
#Run on the control node, so see your admin password and urls
os1 welcome
```

## Packages
Settings for packages can be set using `-s `command line options, but also using local configurations files within the directory `.os1`.
There can be one general config by cluster `config-<clustername>` or specific for a package `config-<cluster>-<package>`. Within the config file
you can set the variables (in uppercase) with your value.

|Package|Group|Description|
|:---|:---|:---|
|||OS1 uses a number of common settings which can be set in the `~/.os1/config` or using the **-** option during startup.<br>`-s cluster=<the clustername>`<br>`-s default_cluster=<default clustername>`<br>`-s domain=<yourdomain>`<br>`-s networks=<list of networks>`<br>`-u <linux user>`<br>`-p <linux password>`|
|sudoers|default|Allows current user to sudo withou password|
|kernel|default|Makes kernel changes, to support more file handles<br>`-s timezone=<Europe/Amsterdam>`|
|os1|default|Shorthand for installer and hostfile updater|
|dns|default|Add dns service|
|microstack|default|Install the snap version of OpenStack (microstack) and the dns service.<br>`-s networks=<list of networks>`<br>`-s nodes=<list of nodes to add>`|
|docker|default|Install docker|
|openresty|default|Install the openresty nginx version with on the fly letsencrypt|
|keycloak|default|Install the keycloak user and access magement|
|proxy|default|Install the reverse proxy scripts in the openresty deployment.|
|rancher|default|Install the rancher 2.x within an openstack client. Admin password is same as OpenStack.|
|minio|default|Install the minio object store.|
|welcome|default|Show welcome message with how to login to **OpenStack** and **Rancher**|
|nodes||Add nodes to the just created cluster.<br>`-s nodes=<list of nodes>`<br>`-s os_user=<os user>`|
|tools||Install aliases and kubectx to the control node making it easier to run kubectl commands|
|longhorn||Install the longhorn local storage.|
|drone-io||Install the drone.io CICD pipleline.<br>`-s cicd_type=<gitlab/github/..>`<br>`-s cicd_client=<clientid>`<br>`-s cicd_secret=<client secrect>`<br>`-s docker_user=<username of docker hub>`<br>`-s docker_password=<password for docker hub>`|
|public-ip||Install a public ip tunnel for you local nat hosted machine, see [Configure Public Domain and Public IP](docs/public-ip.md) for more info.</br> `-s VULTR_API_KEY=<API key of Vutlr.com>`<br>` -s DYNV6_TOKEN=<API key of DYNV6.com>`|
|short-url||Install the kutt.it short url tool|
|reboot||Will reboot the instance you are working on|
