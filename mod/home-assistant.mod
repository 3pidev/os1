mod_home-assistant(){
sudo mkdir -p $DATA_DIR/home-assistant
docker run \
  -d \
  --name home-assistant \
  --pull=always \
  -v $DATA_DIR/home-assistant:/config \
  -p 18123:8123 \
  --restart unless-stopped \
  homeassistant/home-assistant:latest
#  --device=/PATH_TO_YOUR_USB_STICK \

  load_source "proxy"
  local_proxy "ha" "18123"
}

umod_home-assistant(){
docker rm --force home-assistant
}

upgrade_home-assistant(){
    umod_home-assistant
    mod_home-assistant
}