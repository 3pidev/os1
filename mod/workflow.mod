mod_workflow (){
    sudo mkdir -p $DATA_DIR/workflow
    sudo chmod o+w $DATA_DIR/workflow
    docker run -d \
    --restart=unless-stopped\
    --name worklfow \
    -p 5678:5678 \
    -e GENERIC_TIMEZONE="Europe/Amsterdam" \
    -e TZ="Europe/Amsterdam" \
    -v $DATA_DIR/workflow:/home/node/.n8n \
    docker.n8n.io/n8nio/n8n start --tunnel

   load_source "proxy"
   local_proxy "workflow" "5678"
}

umod_workflow(){
    docker stop workflow
    docker rm --force workflow
}