# BI database for dummy's
# https://redash.io/integrations
# https://github.com/getredash/contrib-helm-chart

mod_redash(){
helm repo add redash https://getredash.github.io/contrib-helm-chart/
mkdir -p $config_dir/redash
if [ ! -f "$config_dir/redash/redash-values.yaml" ]; then
cat > $config_dir/redash/redash-values.yaml <<- EOM
redash:
  cookieSecret: "$(echo $RANDOM | md5sum | head -c 32)"
  secretKey: "$(echo $RANDOM | md5sum | head -c 32)"
postgresql:
  postgresqlPassword: "$(echo $RANDOM | md5sum | head -c 32;)"
redis:
  password: "$(echo $RANDOM | md5sum | head -c 32;)"
EOM
fi
#No install it
helm upgrade --install -f $config_dir/redash/redash-values.yaml redash redash/redash --namespace redash --create-namespace 

#We need to expose the service
cluster=$CLUSTER
if [ -n "$cluster" ]; then
  cluster="${cluster}."
fi

cat <<EOC | kubectl -n redash apply  -f -
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: redash-gateway
spec:
  rules:
  - host: redash.${cluster}$DOMAIN
    http:
      paths:
      - path:
        pathType: ImplementationSpecific
        backend:
          service:
            name: redash
            port:
              number: 80
EOC

}


umod_redash(){
  kubectl delete ingress redash-gateway --namespace redash
  helm delete redash  --namespace redash --wait
  kubectl delete namespace redash
}