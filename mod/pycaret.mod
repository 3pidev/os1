
mod_pycaret(){
   docker run  -d --name pycaret --restart unless-stopped  -p 19181:8888 pycaret/full start.sh jupyter lab
   load_source "proxy"
   local_proxy "pycaret" "19181"
}

umod_pycaret(){
 docker rm --force pycaret
}