mod_kubeflow(){
# We will use templater 
load_source "templater"

#Ensure apache
if [ ! -f /usr/bin/htpasswd ];then
sudo apt install apache2-utils -y
fi

#Install kustomize
if [ ! -f /usr/local/bin/kustomize ] ; then
KUSTOMIZE_VERSION=v3.10.0 && \
wget https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2F${KUSTOMIZE_VERSION}/kustomize_${KUSTOMIZE_VERSION}_linux_amd64.tar.gz && \
tar xzf kustomize_${KUSTOMIZE_VERSION}_linux_amd64.tar.gz && \
sudo mv kustomize /usr/local/bin/kustomize && \
rm -rf kustomize_${KUSTOMIZE_VERSION}_linux_amd64.tar.gz
fi

git clone https://github.com/sjhoeksma/manifests.git 
cd manifests
OS1_PWD_HASH=$(htpasswd -bnBC 10 "" "${OS1_PWD}" | tr -d ':\n')

#PATCH the variables in the ingress files and patches
templater_patch "kubeflow-os1/ingress"
templater_patch "kubeflow-os1/patches"

cat > common/user-namespace/base/params.env <<EOF
user=admin@${DOMAIN}
profile-name=kubeflow-admin-${DOMAIN//"."/"-"}
EOF

while ! kustomize build kubeflow-os1 | kubectl apply -f -; do echo "Retrying to apply resources"; sleep 10; done
cd
load_source "proxy"
add_proxy "20-kubeflow"

}

umod_kubeflow(){
 if [ -d ./manifests ] ; then
   cd manifests
   kustomize build kubeflow-os1 | kubectl delete -f -
   cd ..
   rm -rf manifests
 fi
}