mod_stackstorm(){
# Add Helm StackStorm repository
helm repo add stackstorm https://helm.stackstorm.com/
helm repo update


#Add the ingress access
cluster=$CLUSTER
if [ -n "$cluster" ]; then
  cluster="${cluster}."
fi

cat > $config_dir/stackstorm.yaml <<EOF
st2:
  # Username, used to login to StackStorm system
  username: admin@$DOMAIN
  # Password, used to login to StackStorm system
  # If set, st2.password always overrides any existing password.
  # If not set, the password is auto-generated on install and preserved across upgrades.
  password: $OS1_PWD
  rbac:
    enabled: false
    assignments:
      # TIP: set files to an empty string to remove them (admin.yaml: "")
      admin.yaml: |
        ---
        username: admin@$DOMAIN
        roles:
          - system_admin
st2web:
  service:
    type: "ClusterIP"
EOF

helm upgrade --install --create-namespace --namespace stackstorm --cleanup-on-fail --values $config_dir/stackstorm.yaml  \
     stackstorm stackstorm/stackstorm-ha

cat <<EOC | kubectl -n stackstorm apply  -f -
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: stackstorm-gateway
spec:
  rules:
  - host: stackstorm.${cluster}${DOMAIN}
    http:
      paths:
      - path:
        pathType: ImplementationSpecific
        backend:
          service:
            name: stackstorm-st2web
            port:
              number: 80
EOC
}

umod_stackstorm(){
 kubectl delete ingress stackstorm-gateway --namespace stackstorm
 helm uninstall --namespace stackstorm stackstorm --wait
 kubectl delete namespace stackstorm
}