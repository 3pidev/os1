mod_firewall(){
sudo bash << END_SUDO
  ufw allow 22/tcp
  ufw allow 80/tcp
  ufw allow 443/tcp
  ufw allow 6443/tcp
  #Mail Server
  ufw allow 25/tcp
  ufw allow 465/tcp
  ufw allow 587/tcp
  ufw allow 110/tcp
  ufw allow 995/tcp
  ufw allow 143/tcp
  ufw allow 993/tcp
  echo "y" | ufw enable
END_SUDO
}

umod_firewall(){
  ufw disable
}