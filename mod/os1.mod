#Install the os1 script

mod_os1(){
local domain=${1:-$DOMAIN}
local cluster=${2:-$DEFAULT_CLUSTER}
#Copy the os1 file
get_files "os1" "/usr/bin" "{{FILES_PATH}}" "sudo"
sudo bash << END_SUDO
chmod +x /usr/bin/os1
cat > /etc/cron.d/os1 <<EOF
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/snap/bin
*/5 * * * *  root  /usr/bin/os1 'hosts' $domain $cluster >/tmp/os1.hosts.log 2>&1
EOF
END_SUDO
}

umod_os1 (){
sudo rm -rf /usr/bin/os1 /etc/cron.d/os1 
}
