mod_budibase(){
#We need to disable ingress
helm repo add budibase https://budibase.github.io/budibase/
helm repo update

cluster=$CLUSTER
if [ -n "$cluster" ]; then
  cluster="${cluster}."
fi

cat > $config_dir/budibase.yaml <<EOF
ingress:
  enabled: false
  nginx: false
EOF

#if  is_installed "minio" ; then
#cat >> budibase.yaml <<EOF
#services:
#  objectStore:
#    minio: true
#   # accessKey: "" # AWS_ACCESS_KEY if using S3 or existing minio access key
#   # secretKey: "" # AWS_SECRET_ACCESS_KEY if using S3 or existing minio secret
#   # region: "" # AWS_REGION if using S3 or existing minio secret
#    url: "https://minio.${DOMAIN}" # only change if pointing to existing minio cluster or S3 and minio: false
#EOF
helm upgrade --install --create-namespace --namespace budibase --values $config_dir/budibase.yaml --cleanup-on-fail budibase budibase/budibase

cat <<EOC | kubectl -n budibase apply  -f -
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: budibase-gateway
spec:
  rules:
  - host: budibase.$cluster${DOMAIN}
    http:
      paths:
      - path:
        pathType: ImplementationSpecific
        backend:
          service:
            name: proxy-service
            port:
              number: 10000
EOC
}

umod_budibase(){
 kubectl delete ingress budibase-gateway --namespace budibase
 helm uninstall --namespace budibase budibase --wait
 kubectl delete namespace budibase
}