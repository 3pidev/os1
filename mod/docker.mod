# --- Software Functions -------------------------------------------
# All software install/uninstall actions will be performed on ctrl
# Software functions must start with mod_
# This script can be used to install docker

version_docker_compose="2.6.0"

mod_docker(){
local inet=$OS_NETWORK
local mtu=${OS_MTU:-\$(ip -c a | grep $inet | awk '{print \$4}')}
#Skip default network
if [ "$mtu" == "1500" ]; then
 mtu=''
fi

# To prevent adding sudo for all commands we do it once
sudo bash << END_SUDO
#------------------------------------------------------

# Install additional software, curl, wget expect
ln -snf /usr/share/zoneinfo/$TIMEZONE /etc/localtime 
echo "$TIMEZONE" > /etc/timezone
apt-get update 
DEBIAN_FRONTEND=noninteractive apt-get install jq docker.io -y	
snap install yq
mtu=$mtu
if [ -n "\$mtu" ] ;then
 cp /lib/systemd/system/docker.service /etc/systemd/system/docker.service
 sed -i "/ExecStart/ s/\$/ --mtu \$mtu/g" /etc/systemd/system/docker.service
 systemctl daemon-reload
fi
systemctl restart docker		
systemctl enable docker

curl -sL "https://github.com/docker/compose/releases/download/${version_docker_compose}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# To prevent adding sudo for all commands we do it once
END_SUDO

#Add our self to the docker group
sudo usermod -a -G docker $USER
#------------------------------------------------------
}

#Remove the docker software
umod_docker(){
 sudo systemctl stop docker
 sudo rm -rf /etc/systemd/system/docker.service
 sudo systemctl daemon-reload
 sudo apt-get remove docker.io -y
 sudo rm -rf /usr/local/bin/docker-compose
 sudo snap remove yq
}
