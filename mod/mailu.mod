mod_mailu(){
  #Copy the files required
  MAILU_SECRET_KEY=${MAILU_SECRET_KEY:-$(echo $RANDOM | md5sum | head -c 16; echo;)}
  for file in "docker-compose.yml" "mailu.env"  ; do
    get_files "${file}" $config_dir/mailu "{{FILES_PATH}}/mailu" "sudo" "yes"
  done
  #Change to dir
  cd $config_dir/mailu
  docker-compose -p mailu up -d
  #Add the admin after 10 seconds
  sleep 10
  docker-compose exec admin flask mailu admin admin ${DOMAIN} "$OS1_PWD"
  # Change back directory
  cd
}

umod_mailu(){
  cd $config_dir/mailu
  docker-compose -p mailu down
  cd
  sudo rm -rf $config_dir/mailu
  sudo rm -rf $DATA_DIR/mailu
}

upgrade_mailu(){
 get_files "docker-compose.yml" $config_dir/mailu "{{FILES_PATH}}/mailu" "sudo" "yes"
 cd $config_dir/mailu
 docker-compose -p mailu pull
 docker-compose -p mailu down
 docker-compose -p mailu up -d
 cd
}