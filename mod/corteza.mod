corteza_config(){
cat > $config_dir/corteza/.env <<EOF
# General settings
DOMAIN=corteza.${DOMAIN}
VERSION=2022.3.0

########################################################################################################################
# Database connection
DB_DSN=postgres://corteza:corteza@db:5432/corteza?sslmode=disable

########################################################################################################################
# Server settings

# Serve Corteza webapps alongside API
HTTP_WEBAPP_ENABLED=true

# Send action log to container logs as well
# ACTIONLOG_DEBUG=true

# Uncomment for extra debug info if something goes wrong
# LOG_LEVEL=debug

# Use nicer and colorful log instead of JSON
# LOG_DEBUG=true

# Authentication
# Secret to use for JWT token
AUTH_JWT_SECRET=${SECRET_KEY}
EOF

if [ -n "$NOREPLY_SMTP_PWD" ]; then
cat >> $config_dir/corteza/.env <<EOF
# SMTP (mail sending) settings
SMTP_HOST=mail.${DOMAIN}:587
SMTP_USER=no-reply@${DOMAIN}
SMTP_PASS=${NOREPLY_SMTP_PWD}
SMTP_FROM='"corteza" <no-reply@${DOMAIN}>'
EOF
fi
}

mod_corteza(){
get_files "docker-compose.yml" $config_dir/corteza "{{FILES_PATH}}/corteza" "" "yes"
corteza_config

cd $config_dir/corteza
docker-compose up -d
#TODO 
# Add admin account
# Add keycloak

cd
load_source "proxy"
add_proxy "20-corteza" 
}

umod_corteza(){
  cd $config_dir/corteza
  docker-compose down
  cd 
  sudo rm -rf $config_dir/corteza
  sudo rm -rf ${DATA_DIR}/corteza
}

upgrade_corteza(){
  get_files "docker-compose.yml" $config_dir/corteza "{{FILES_PATH}}/corteza" "" "yes"
  corteza_config 
  cd $config_dir/corteza
  docker-compose pull
  docker-compose down
  docker-compose up -d
  cd
}