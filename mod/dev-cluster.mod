mod_dev-cluster(){
 load_source "functions"
 CLUSTER="dev"
 #Create dev cluster on dev network
 rancher_create_cluster $CLUSTER 'dev' 'ctrl.node1' 'ctrl.node0'
 #Wait for cluster
 if rancher_cluster_ready ;then
   #Install local kube_config
   rancher_kubectl "${CLUSTER}" "" "no" "$DOMAIN" "yes"
   echo "Adding users"
   rancher_add_user "Sierk" "Sierk Hoeksma" "Hoeksma"
   echo "Assigning roles"
   rancher_add_role "Sierk"  "cluster-owner"
   install "metallb,longhorn,drone-io,kubeless"
 fi
 rancher_logout
}

umod_dev-cluster(){
 echo "Removing cluster"
 load_source "functions"
 CLUSTER="dev"
 rancher_destroy_cluster
 echo "Removing users"
 rancher_remove_role "Sierk"
 rancher_logout
}

#We don't support upgrade
upgrade_dev-cluster(){
return 0
}