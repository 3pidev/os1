# --- Software Functions -------------------------------------------
# All software install/uninstall actions will be performed on ctrl
# Software functions must start with mod_
# This script can be used to configure the kernel settings

mod_kernel(){

user=$USER
# To prevent adding sudo for all commands we do it once
sudo bash << END_SUDO
#------------------------------------------------------
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get upgrade -y

#Set Timezone
ln -snf /usr/share/zoneinfo/$TIMEZONE /etc/localtime 
echo "$TIMEZONE" > /etc/timezone
#Change the kernel parameters
sed -i '/# Begin OpenStack/,/# End of OpenStack/d' /etc/sysctl.conf 
echo '# Begin OpenStack' | tee -a /etc/sysctl.conf >/dev/null
echo fs.inotify.max_queued_events=1048576 | tee -a /etc/sysctl.conf >/dev/null
echo fs.inotify.max_user_instances=1048576 | tee -a /etc/sysctl.conf >/dev/null
echo fs.inotify.max_user_watches=1048576 | tee -a /etc/sysctl.conf >/dev/null
echo vm.max_map_count=262144 | tee -a /etc/sysctl.conf >/dev/null
echo vm.swappiness=1 | tee -a /etc/sysctl.conf >/dev/null
echo net.ipv4.ip_forward=1 | tee -a /etc/sysctl.conf >/dev/null
echo net.ipv6.conf.all.forwarding=1 | tee -a /etc/sysctl.conf >/dev/null
echo '# End of OpenStack' | tee -a /etc/sysctl.conf >/dev/null
sysctl -p

#Sensors
sudo apt-get install lm-sensors -y

if [ "$ARCH" != "arm"  ] &&  [ "$ARCH" != "arm64"  ];then
apt-get install -y lm-sensors fancontrol
sed -i "/GRUB_CMDLINE_LINUX=/c\GRUB_CMDLINE_LINUX=\"acpi_enforce_resources=lax cgroup_enable=memory swapaccount=1\""  /etc/default/grub
update-grub
else
sudo sed -i 's/ cgroup_enable=memory cgroup_memory=1//g' /boot/firmware/cmdline.txt
sudo sed -i '1s/.*/& cgroup_enable=memory cgroup_memory=1/' /boot/firmware/cmdline.txt
fi

#Enabled multi user mode
systemctl set-default multi-user.target

# Install additional software, curl, wget expect
apt-get update 
DEBIAN_FRONTEND=noninteractive apt-get install expect curl -y

# To prevent adding sudo for all commands we do it once
END_SUDO
#------------------------------------------------------
}

umod_kernel(){
sudo bash << END_SUDO
#------------------------------------------------------
apt-get autoremove -y
sed -i '/# Begin OpenStack/,/# End of OpenStack/d' /etc/sysctl.conf
sysctl -p
END_SUDO
}

#Upgrade not supported
upgrade_kernel(){
 return 0
}