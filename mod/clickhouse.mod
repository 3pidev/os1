mod_clickhouse() {

sudo mkdir -p $DATA_DIR/clickhouse/data
sudo mkdir -p $DATA_DIR/clickhouse/logs

#Run the client
docker run -d \
   -p 8123:8123 -p9000:9000 \
   --restart=unless-stopped\
   --name clickhouse \
   --ulimit nofile=262144:262144 \
   -v $DATA_DIR/clickhouse/data:/var/lib/clickhouse/ \
   -v $DATA_DIR/clickhouse/log:/var/log/clickhouse-server/ \
   clickhouse/clickhouse-server
}

umod_clickhouse(){
    docker stop clickhouse
    docker rm --force clickhouse
}