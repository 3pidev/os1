#Install the frpc client
#TODO: Build service that will restart on connection loss
frp_version=0.42.0

mod_frpc(){

# To prevent adding sudo for all commands we do it once
sudo bash << END_SUDO
#Get the architecture of machine
ARCH=""
case $(uname -m) in
    i386)   ARCH="386" ;;
    i686)   ARCH="386" ;;
    x86_64) ARCH="amd64" ;;
    aarch64) ARCH="arm64" ;;
    arm)    dpkg --print-architecture | grep -q "arm64" && ARCH="arm64" || ARCH="arm" ;;
esac
PLATFORM=$(uname)
#------------------------------------------------------
wget https://github.com/fatedier/frp/releases/download/v${frp_version}/frp_${frp_version}_\${PLATFORM,,}_\${ARCH,,}.tar.gz
mkdir -p /etc/frp
tar -xf frp_${frp_version}_${PLATFORM,,}_${ARCH,,}.tar.gz --strip 1 -C /etc/frp
rm frp_${frp_version}_${PLATFORM,,}_\${ARCH,,}.tar.gz

ln -sf /etc/frp/frpc /usr/bin/frpc
ln -sf /etc/frp/frps /usr/bin/frps
ln -sf /etc/frp/systemd/frps.service /etc/systemd/system/frps.service
ln -sf /etc/frp/systemd/frpc.service /etc/systemd/system/frpc.service
sed -i 's/nobody/root/g' /etc/systemd/system/frps.service
sed -i 's/nobody/root/g' /etc/systemd/system/frpc.service

frp_server_ip=$(getent hosts $DOMAIN | awk '{ print $1 }')

cat > /etc/frp/frpc.ini <<EOF
[common]
# A literal address or host name for IPv6 must be enclosed
# in square brackets, as in "[::1]:80", "[ipv6-host]:http" or "[ipv6-host%zone]:80"
# For single "server_addr" field, no need square brackets, like "server_addr = ::".
server_addr = \$frp_server_ip
server_port = 7990
# Protocal can be set to kcp to reduce latency but increase bandwidth
#protocol = kcp
# Enable and specify the number of connection pool
#pool_count = 1

# tcpmux_httpconnect_port specifies the port that the server listens for TCP
# HTTP CONNECT requests. If the value is 0, the server will not multiplex TCP
# requests on one single port. If it's not - it will listen on this value for
# HTTP CONNECT requests. By default, this value is 0.
tcpmux_httpconnect_port = 7993
tcp_mux = true

# console or real logFile path like ./frpc.log
log_file = /var/log/frpc.log

# trace, debug, info, warn, error
log_level = info

log_max_days = 3

# auth token
token = $FRP_TOKEN

[http]
type = http
local_port = 2080
custom_domains = $DOMAIN,*.$DOMAIN

[https]
type = https
local_port = 2443
custom_domains = $DOMAIN,*.$DOMAIN

EOF

#Append if MAILU installed
if is_installed 'mailu' ; then
cat >> /etc/frp/frpc.ini <<EOF
#Mailu portmapping
[mailu-25]
type=tcp
local_ip=$SERVER_IP
local_port=25
remote_port=25

[mailu-465]
type=tcp
local_ip=$SERVER_IP
local_port=465
remote_port=465

[mailu-587]
type=tcp
local_ip=$SERVER_IP
local_port=587
remote_port=587

[mailu-110]
type=tcp
local_ip=$SERVER_IP
local_port=110
remote_port=110

[mailu-995]
type=tcp
local_ip=$SERVER_IP
local_port=995
remote_port=995

[mailu-143]
type=tcp
local_ip=$SERVER_IP
local_port=143
remote_port=143

[mailu-993]
type=tcp
local_ip=$SERVER_IP
local_port=993
remote_port=993

[wiregaurd-tcp]
type=tcp
local_ip=$SERVER_IP
local_port=41194
remote_port=41194

[wiregaurd-udp]
type=udp
local_ip=$SERVER_IP
local_port=51820
remote_port=4119

[openvpn]
type=udp
local_ip=$SERVER_IP
local_port=1194
remote_port=1194

[mqtt-1883]
type=tcp
local_ip=0.0.0.0
local_port=1883
remote_port=1883
EOF
fi

#Now start the frp client
systemctl restart frpc
systemctl enable frpc

#------------------------------------------------------
END_SUDO

}

umod_frpc(){
# To prevent adding sudo for all commands we do it once
sudo bash << END_SUDO
systemctl stop frpc
systemctl disable frpc
rm -rf /root/.os1/public-ip.service /root/.os1/public-ip.sh /etc/frp
systemctl daemon-reload
# To prevent adding sudo for all commands we do it once
END_SUDO
}
