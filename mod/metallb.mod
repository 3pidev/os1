# --- Software Functions -------------------------------------------
# All software install/uninstall actions will be performed on first_master
# Software functions must start with k3s_
# https://medium.com/@ipuustin/using-metallb-as-kubernetes-load-balancer-with-ubiquiti-edgerouter-7ff680e9dca3
# https://www.reddit.com/r/kubernetes/comments/cqwnnf/bgp_peering_between_edgerouter_and_k8s_nodes_via/

metallb_version="0.9.6"
mod_metallb_edgerouter(){
BGP=64512
${SSH}${BASE_IP}.1 bash << EOF
 configure
  set protocols bgp ${BGP} parameters router-id ${BASE_IP}.1
  set protocols bgp ${BGP} neighbor ${BASE_IP}.10 remote-as 64512
  set protocols bgp ${BGP} neighbor ${BASE_IP}.11 remote-as 64512
  set protocols bgp ${BGP} neighbor ${BASE_IP}.12 remote-as 64512
  set protocols bgp ${BGP} maximum-paths ibgp 32
  commit
  save
  exit
  exit
EOF

kubectl apply -f https://raw.githubusercontent.com/google/metallb/v${metallb_version}/manifests/metallb.yaml
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: metallb-system
  name: config
data:
  config: |
    peers:
    - peer-address: ${BASE_IP}.1
      peer-asn: ${BGP}
      my-asn: ${BGP}
    address-pools:
    - name: default
      protocol: bgp
      addresses:
      - ${BASE_IP}.224/27
EOF
}

#Metallb the hardware loadbalancer
mod_metallb() {
 local startR=$(( ${1:-${METALLB_START:-0}} + 224 ))
 local cluster=${2:-$CLUSTER}
 local _TMP=/tmp/metalb
 mkdir -p $_TMP
 local kubectl="$_TMP/kubectl"
 load_source "functions"
 if rancher_kubectl $cluster $kubectl ;then
  export KUBECONFIG=$kubectl
  helm repo add bitnami https://charts.bitnami.com/bitnami
  helm repo update
  #TODO READ THE START FROM CLUSTER CONFIG FILE AND WRITE IT BACK
  #getClusterValue setClusterValue getNetworkValue setNetworkValue
  local endR=$(( startR + 5 ))
  local METALLB_RANGE=$(host $cluster.external | awk '/address/ { print $4 }' | awk -v start=$startR -v end=$endR -F. '{print $1 "." $2 "." $3 "." start "-" $1 "." $2 "." $3 "." end }')
  helm upgrade -i metallb bitnami/metallb \
  --namespace kube-system \
  --wait \
  --set configInline.address-pools[0].name=default \
  --set configInline.address-pools[0].protocol=layer2 \
  --set configInline.address-pools[0].addresses[0]=$METALLB_RANGE 
  fi
rm -rf $_TMP
}

#Metallb the hardware loadbalancer
umod_metallb() {
 local cluster=${1:-$CLUSTER}
 local _TMP=/tmp/metalb
 mkdir -p $_TMP
 local kubectl="$_TMP/kubectl"
 load_source "functions"
 if rancher_kubectl $cluster $kubectl ;then
   export KUBECONFIG=$kubectl
   helm delete metallb --namespace kube-system
 fi
 rm -rf $_TMP
}

upgrade_metallb(){
 #Is helm chart so just run install
 mod_metallb $1 $2
}
