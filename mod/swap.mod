mod_swap(){
  local size=${1:-1}
  sudo bash << END_SUDO
  if [ ! -f /swapfile ]; then
     fallocate -l ${size}G /swapfile
     chmod 600 /swapfile
     mkswap /swapfile
     swapon /swapfile
     cat >> /etc/fstab <<EOF
/swapfile swap swap defaults 0 0
EOF
  fi
END_SUDO
}

umod_swap(){
    sudo bash << END_SUDO
    swapoff /swapfile
    rm -rf /swapfile
    sed -i '/\/swapfile/d' /etc/fstab
END_SUDO
}
