# --- Software Functions -------------------------------------------
# All software install/uninstall actions will be performed on ctrl
# Software functions must start with mod_

loc_nginx="/usr/local/openresty/nginx"
loc_rproxy="${loc_nginx}/conf/rproxy"

add_proxy(){
local proxy_http=${1}
local proxy_streams=${2}
local location=${3:-"{{FILES_PATH}}/rproxy"}
local domain=$DOMAIN
local server_ip=$SERVER_IP
if [ -n "$proxy_http" ]; then
  local dest="${loc_rproxy}/http/available"
  for file in $proxy_http ; do
    get_files "${file}.conf" $dest "$location" "sudo" "yes"
    sudo ln -sf $dest/$file.conf ${loc_rproxy}/http/enabled/$file.conf
  done
fi
if [ -n "$proxy_streams" ]; then
  local dest="${loc_rproxy}/stream/available"
  for file in $proxy_streams ; do
    get_files "${file}.conf" $dest "$location" "sudo" "yes"
    sudo ln -sf $dest/$file.conf ${loc_rproxy}/stream/enabled/$file.conf
  done
fi
sudo systemctl restart openresty
}

local_proxy(){
 local proxy_name=${1}
 local proxy_port=${2}
 local proxy_protocol=${3:-http}
 local domain=${4:-${DOMAIN}}
 local keepalive=${5:-8}
 local header_timeout=${6:-10s}
 local body_timeout=${7:-10s}
 local max_body_size=${8:-50M}
 get_files "local_proxy.conf" "${loc_rproxy}/http/available" "{{FILES_PATH}}/rproxy" "sudo" "yes"
 sudo mv -f ${loc_rproxy}/http/available/local_proxy.conf ${loc_rproxy}/http/available/30-${proxy_name}.conf
 sudo ln -sf ${loc_rproxy}/http/available/30-${proxy_name}.conf ${loc_rproxy}/http/enabled/30-${proxy_name}.conf
 sudo systemctl restart openresty
}

mod_proxy(){
# $1 - The domain name used
# $2 - List with networks to install
local domain="${1:-$DOMAIN}"
local networks="${2:-$NETWORKS}"
local proxy_http=${3:-${PROXY_HTTP:-'20-rancher 20-router 20-openstack 20-frp 20-keycloak 40-k3d 40-mailu 70-ip 80-name'}}
local proxy_streams=${4:-${PROXY_STREAM:-'20-kubectl'}}
local localrancher=false
local server_ip=$SERVER_IP

if [ -n "$HARVESTER_IP" ];then
  proxy_http="${proxy_http} 20-harvester"
fi

#Check if openresty is installed when not install it
if ! is_installed 'openresty' ; then
 install 'openresty'
fi

if contains "$CONFIG_OPTIONS" 'local' ;then
 localrancher=true
fi
if [ -z "$(snap aliases 2>/dev/null | grep openstack | awk {'print $1'})" ] ;then
 localrancher=true
fi


if [ -z "$RANCHER_HOST" ]; then
 RANCHER_HOST="rancher"
 if [ "$localrancher" != "true" ] ;then
  local RANCHER_HOST=localhost
 fi
fi

if [ -z "$RANCHER_PORT" ]; then
 RANCHER_PORT="443"
 if [ "$localrancher" == "true" ] ;then
  RANCHER_PORT="8443"
 fi
fi 

local OS_HOST=localhost
if [ -n "$OS_PROJECT" ]; then
  os_project="--os-project-name $OS_PROJECT"
fi

#Copy the rproxy config files from the git hub
local dest="${loc_rproxy}/http/available"
sudo mkdir -p $dest
sudo touch ${dest}/empty.conf
sudo mkdir -p ${loc_rproxy}/http/enabled
sudo mkdir -p ${loc_rproxy}/stream/enabled

local ng_subnet=$(echo $BASE_IP | sed 's/\./\\./g')
for file in $proxy_http ; do
  get_files "${file}.conf" $dest "{{FILES_PATH}}/rproxy" "sudo" "yes"
done

#Stream are not defined yet
local dest="${loc_rproxy}/stream/available"
sudo mkdir -p $dest
sudo touch ${dest}/empty.conf
#kubectl is by default enabled on local rancher so don't install it
if [ "$localrancher" != "true" ] ;then
  for file in $proxy_streams ; do
     get_files "${file}.conf" $dest "{{FILES_PATH}}/rproxy" "sudo" "yes"
  done
fi

#Copy the oops html file
get_files "oops.html" "${loc_nginx}/html" "{{FILES_PATH}}/rproxy" "sudo" "yes"
#Copy the nginx.conf file
get_files "nginx.conf" "${loc_nginx}/conf" "{{FILES_PATH}}/rproxy" "sudo" "yes"

# To prevent adding sudo for all commands we do it once
sudo bash << END_SUDO
#------------------------------------------------------

#Enabling the services and streams
echo "Enabling reverse proxy"
ln -sf ${loc_rproxy}/http/available/*.conf ${loc_rproxy}/http/enabled
ln -sf ${loc_rproxy}/stream/available/*.conf ${loc_rproxy}/stream/enabled

cat > /usr/local/openresty/nginx/domains <<EOF
^${domain}\$
^([^.]+)\\.${domain}\$
^([^.]+)\\.dev.${domain}\$
^k3d-([0-9]+)\\.${domain}\$
^k3d-(.+)-([0-9]+)\\.${domain}\$
^([^.]+)\\.k3d-([0-9]+)\\.${domain}\$
^([^.]+)\\.k3d-(.+)-([0-9]+)\\.${domain}\$
EOF

systemctl restart openresty

# To prevent adding sudo for all commands we do it once
END_SUDO
#------------------------------------------------------
}

#uninstall
umod_proxy(){
# To prevent adding sudo for all commands we do it once
sudo bash << END_SUDO

rm -f  $loc_nginx/html/oops.html
cp -f  $loc_nginx/conf/nginx.conf.default $loc_nginx/conf/nginx.conf
rm -rf $loc_rproxy

systemctl restart openresty

# To prevent adding sudo for all commands we do it once
END_SUDO
#------------------------------------------------------

}