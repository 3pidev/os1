mod_hostname{
 local oldname=$hostname
 local name=${1:-${OS1_HOSTNAME:-${oldname:-os1}}}
    sudo bash << END_SUDO
    cat > /etc/hostname << EOF
$name
EOF
#Swap the hostname in etc/hosts
END_SUDO
}