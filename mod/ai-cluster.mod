#https://www.suse.com/c/running-edge-artificial-intelligence-k3s-cluster-with-nvidia-jetson-nano-boards-src/
#https://github.com/xiftai/jetson_nano_k3s_cluster_gpu/tree/main/k3s
#https://k3d.io/v4.4.8/usage/guides/cuda/

#FIX Rancher on install password is not changed

#Copy tmpl from sd
#remove Whoopsie
#Tag the nodes as GPU

K3S_VERSION=v1.21.5+k3s2

mod_ai-cluster(){
  local nodes="10.0.9.11"
#On current server install k3s master
curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION=${K3S_VERSION} sh -
sudo cp /etc/rancher/k3s/k3s.yaml .kube/config && sudo chown -R $USER.$USER .kube
#Register cluster to rancher

#Install the nodes
K3S_TOKEN=$(sudo cat /var/lib/rancher/k3s/server/node-token)
list=$(echo "$nodes" | sed 's/,/ /g')
for listitem in $list ; do
  if [ -n "$listitem" ] ;then
    # Install remote os1, kernel, sudoers

    # Install K3s and
    ${SSH}${listitem} bash << EOC

#On the Jetson
sudo bash << END_SUDO
#GPU Support
mkdir -p /var/lib/rancher/k3s/agent/etc/containerd
cat > /var/lib/rancher/k3s/agent/etc/containerd/config.toml.tmpl <<EOF
[plugins.opt]
  path = "{{ .NodeConfig.Containerd.Opt }}"

[plugins.cri]
  stream_server_address = "127.0.0.1"
  stream_server_port = "10010"

{{- if .IsRunningInUserNS }}
  disable_cgroup = true
  disable_apparmor = true
  restrict_oom_score_adj = true
{{end}}

{{- if .NodeConfig.AgentConfig.PauseImage }}
  sandbox_image = "{{ .NodeConfig.AgentConfig.PauseImage }}"
{{end}}

{{- if not .NodeConfig.NoFlannel }}
[plugins.cri.cni]
  bin_dir = "{{ .NodeConfig.AgentConfig.CNIBinDir }}"
  conf_dir = "{{ .NodeConfig.AgentConfig.CNIConfDir }}"
{{end}}

[plugins.cri.containerd.runtimes.runc]
  # ---- changed from 'io.containerd.runc.v2' for GPU support
  runtime_type = "io.containerd.runtime.v1.linux"

# ---- added for GPU support
[plugins.linux]
  runtime = "nvidia-container-runtime"

{{ if .PrivateRegistryConfig }}
{{ if .PrivateRegistryConfig.Mirrors }}
[plugins.cri.registry.mirrors]{{end}}
{{range \$k, \$v := .PrivateRegistryConfig.Mirrors }}
[plugins.cri.registry.mirrors."{{\$k}}"]
  endpoint = [{{range \$i, \$j := \$v.Endpoints}}{{if \$i}}, {{end}}{{printf "%q" .}}{{end}}]
{{end}}

{{range \$k, \$v := .PrivateRegistryConfig.Configs }}
{{ if \$v.Auth }}
[plugins.cri.registry.configs."{{\$k}}".auth]
  {{ if \$v.Auth.Username }}username = "{{ \$v.Auth.Username }}"{{end}}
  {{ if \$v.Auth.Password }}password = "{{ \$v.Auth.Password }}"{{end}}
  {{ if \$v.Auth.Auth }}auth = "{{ \$v.Auth.Auth }}"{{end}}
  {{ if \$v.Auth.IdentityToken }}identitytoken = "{{ \$v.Auth.IdentityToken }}"{{end}}
{{end}}
{{ if \$v.TLS }}
[plugins.cri.registry.configs."{{\$k}}".tls]
  {{ if \$v.TLS.CAFile }}ca_file = "{{ \$v.TLS.CAFile }}"{{end}}
  {{ if \$v.TLS.CertFile }}cert_file = "{{ \$v.TLS.CertFile }}"{{end}}
  {{ if \$v.TLS.KeyFile }}key_file = "{{ \$v.TLS.KeyFile }}"{{end}}
{{end}}
{{end}}
{{end}}
EOF

#CUDA Support
mkdir -p /var/lib/rancher/k3s/server/manifests
cat > /var/lib/rancher/k3s/server/manifests/nvidia-device-plugin-daemonset.yaml <<EOF
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: nvidia-device-plugin-daemonset
  namespace: kube-system
spec:
  selector:
    matchLabels:
      name: nvidia-device-plugin-ds
  template:
    metadata:
      # Mark this pod as a critical add-on; when enabled, the critical add-on scheduler
      # reserves resources for critical add-on pods so that they can be rescheduled after
      # a failure.  This annotation works in tandem with the toleration below.
      annotations:
        scheduler.alpha.kubernetes.io/critical-pod: ""
      labels:
        name: nvidia-device-plugin-ds
    spec:
      tolerations:
      # Allow this pod to be rescheduled while the node is in "critical add-ons only" mode.
      # This, along with the annotation above marks this pod as a critical add-on.
      - key: CriticalAddonsOnly
        operator: Exists
      containers:
      - env:
        - name: DP_DISABLE_HEALTHCHECKS
          value: xids
        # image: nvidia/k8s-device-plugin:1.11
        image: pitchayasak/k8s-device-plugin:1.0.0-arm64
        name: nvidia-device-plugin-ctr
        securityContext:
          allowPrivilegeEscalation: true
          capabilities:
            drop: ["ALL"]
        volumeMounts:
          - name: device-plugin
            mountPath: /var/lib/kubelet/device-plugins
      volumes:
        - name: device-plugin
          hostPath:
            path: /var/lib/kubelet/device-plugins
EOF
END_SUDO
curl -sfL https://get.k3s.io | K3S_URL=https://os1-ai-0:6443 K3S_TOKEN=${K3S_TOKEN} sh -
EOC
  fi
done



#Install longhorn 

#Install kubeflow
install kubeflow

}




#Create .os1/config with OS1_PWD
if [ ! -f ~/.os1/config ] ; then
mkdir -p ~/.os1
TOKEN=$(date +%s | sha256sum | base64 | head -c 32 ; echo)
cat > ~/.os1/config << EOF
OS1_PWD=$TOKEN
DOMAIN=ai.os1.nl
FRP_TOKEN=$(echo -n "admin:$TOKEN" | base64)
EOF
fi

bash <(curl -sfLN get.os1.nl) -p <USER_PASSWORD> install "sudoers,kernel,os1,openresty,frpc,docker,minio,rancher,proxy,tools,welcome"






#Install K3s

K3S_TOKEN=$(sudo cat /var/lib/rancher/k3s/server/node-token)
#TODO REMOTE LOGIN
K3S_VERSION=v1.21.5+k3s2
curl -sfL https://get.k3s.io | K3S_URL=https://os1-ai-0:6443 K3S_TOKEN=${K3S_TOKEN} sh -
kubectl label nodes jetson-nx GPU=enabled

curl -sfL https://get.k3s.io | K3S_URL=https://myserver:6443 K3S_TOKEN=mynodetoken sh -

curl -sfL https://get.k3s.io | sh -
alias kubectl="/usr/local/bin/k3s kubectl"



umod_ai-cluster(){
/usr/local/bin/k3s-uninstall.sh
}