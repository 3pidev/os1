# --- Software Functions -------------------------------------------
# All software install/uninstall actions will be performed on ctrl
# Software functions must start with mod_
# This script can be used to install and configure minio object storage


mod_minio (){
local datadir="${1:-$DATA_DIR}"
local httpport="${2:-${MINIO_PORT:-9000}}"
local httpportc="${3:-${MINIO_PORTC:-9001}}"
local adminpwd=$OS1_PWD

# To prevent adding sudo for all commands we do it once
sudo bash << END_SUDO
mkdir -p $datadir
mkdir -p $datadir/minio
#Start Docker installation of minio
docker run -d -p $httpport:9000 -p $httpportc:9001 \
  --restart=unless-stopped\
  --name minio \
  -v $datadir/minio:/data \
  -e "MINIO_ROOT_USER=$OS_USER" \
  -e "MINIO_ROOT_PASSWORD=$adminpwd" \
  minio/minio server --console-address :9001 /data
END_SUDO
load_source "proxy"
add_proxy "20-minio"
}

umod_minio(){
local datadir="${1:-$DATA_DIR}"
sudo bash << END_SUDO
 docker rm --force minio
 rm -rf $datadir/minio
END_SUDO
}

upgrade_minio(){
echo "TODO: Implement docker upgrade"
}