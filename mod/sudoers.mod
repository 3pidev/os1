# --- Software Functions -------------------------------------------
# All software install/uninstall actions will be performed on ctrl
# Software functions must start with mod_
# This script allows the current user to perform sudo without password

mod_sudoers(){

# To prevent adding sudo for all commands we do it once

#includedir /etc/sudoers.d
if [ -n "$password" ] ;then
echo -e "$password\n" | sudo -S bash -c echo "Open Sudo"
fi
user=$USER
sudo sed -i "s/%sudo.*/%sudo ALL=(ALL:ALL) NOPASSWD:ALL/g" /etc/sudoers
sudo bash << END_SUDO
rm -rf /etc/sudoers.d/README
echo "$user ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/$user
chmod 750 /etc/sudoers.d
chown root:root /etc/sudoers.d/$user
chmod 0440 /etc/sudoers.d/$user
END_SUDO

}

umod_sudoers(){
 sudo rm -f /etc/sudoers.d/$user
}

#We don't support upgrade
upgrade_sudoers(){
return 0
}
