#Install kubeless
#single kubeconfig required
#export KUBECONFIG=/home/ubuntu/.kube/config-dev
#kubeless trigger http create hello-ingress --function-name hello --gateway kong --hostname hello.api.os1.nl

mod_kubeless(){
 local cluster=${1:-$CLUSTER}
 local _TMP=/tmp/kubeless
 mkdir -p $_TMP
 local kubectl="$_TMP/kubectl"
 load_source "functions"
 if rancher_kubectl $cluster $kubectl ;then
  export KUBECONFIG=$kubectl
  if rancher_project_namespace "FaaS" "kubeless" ;then
    export RELEASE=$(curl -s https://api.github.com/repos/kubeless/kubeless/releases/latest | grep tag_name | cut -d '"' -f 4)
    kubectl create -f https://github.com/kubeless/kubeless/releases/download/$RELEASE/kubeless-$RELEASE.yaml
    export OS=$(uname -s| tr '[:upper:]' '[:lower:]')
    curl -sOL https://github.com/kubeless/kubeless/releases/download/$RELEASE/kubeless_$OS-amd64.zip && \
    unzip kubeless_$OS-amd64.zip && \
    sudo mv bundles/kubeless_$OS-amd64/kubeless /usr/local/bin/
    rm -rf bundles
  fi

 fi
 rm -rf $_TMP
}

umod_kubeless(){
 local cluster=${1:-$CLUSTER}
 local _TMP=/tmp/kubeless
 mkdir -p $_TMP
 local kubectl="$_TMP/kubectl"
 load_source "functions"
 if rancher_kubectl $cluster $kubectl ;then
   export KUBECONFIG=$kubectl
   export RELEASE=$(curl -s https://api.github.com/repos/kubeless/kubeless/releases/latest | grep tag_name | cut -d '"' -f 4)
   kubectl delete -f https://github.com/kubeless/kubeless/releases/download/$RELEASE/kubeless-$RELEASE.yaml
   sudo rm -f /usr/local/bin/kubeless
 fi
 rm -rf $_TMP
}