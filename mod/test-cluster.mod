mod_test-cluster(){
  load_source "functions"
  CLUSTER="test"
  create_k3s_cluster "$CLUSTER" "os1-2-11" "" "$USER" "true"
}

umod_test-cluster(){
  load_source "functions"
  CLUSTER="test"
  destroy_k3s_cluster "$CLUSTER" "os1-2-11" "" "$USER"
}