#Function to install longhorn storage system

LONGHORN_VERSION=v1.1.0
mod_longhorn(){
 local cluster=${1:-$CLUSTER}
 local _TMP=/tmp/longhorn
 mkdir -p $_TMP
 load_source "functions"
 local kubectl="$_TMP/kubectl"
 load_source "functions"
 if rancher_kubectl $cluster $kubectl ;then
  export KUBECONFIG=$kubectl
  kubectl create -f  https://raw.githubusercontent.com/longhorn/longhorn/$LONGHORN_VERSION/deploy/longhorn.yaml 
  local TIMEOUT=200
  echo -n "  Waiting for storageclass longhorn to become available"
  while [ "${TIMEOUT}" -gt 0 ]; do
     if [ -n "$(kubectl get storageclass 2>/dev/null | grep longhorn)" ] ;then
       TIMEOUT=0
     else
       wait_spinner $TIMEOUT
       TIMEOUT=$?
     fi
   done
  echo ""
  kubectl patch storageclass longhorn -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}' -n longhorn-system
  #Move longhorn to system project
  rancher_project_namespace "System" "longhorn-system"
 fi
 rm -rf $_TMP
}

umod_longhorn(){
 local cluster=${1:-$CLUSTER}
 local _TMP=/tmp/longhorn
 mkdir -p $_TMP
 local kubectl="$_TMP/kubectl"
 load_source "functions"
 if rancher_kubectl $cluster $kubectl ;then
  export KUBECONFIG=$kubectl
  kubectl delete -f  https://raw.githubusercontent.com/longhorn/longhorn/$LONGHORN_VERSION/deploy/longhorn.yaml
 fi
 rm -rf $_TMP
}

upgrade_longhorn(){
 echo "TODO: Check how to upgrade longhorn"
}