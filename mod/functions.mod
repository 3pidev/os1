#Colorcoding constants
NC='\033[0m' # No Color
BLACK='\033[0;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
WHITE='\033[0;37m'

#Set of Rancher/Microstack functions, should beloaded by load_source "functions"

#Copy ssh id to new node
copy_ssh_id() {
 local ip=${1}
 local luser=${2:-${user}}
 local lpwd=${3:-${password}}
 local lloc=${4}
 if [ -z "$lloc" ];then
  lloc=" -i $lloc"
 fi
 if [ -z "$lpwd" ] ;then
     ssh-copy-id -f ${luser}@${ip} $lloc
    else
cat << EOF | expect -f -
set timeout 15
spawn ssh-copy-id ${luser}@${ip}
match_max 100000
expect {
 "*fingerprint*" {
  send -- "yes\r"
  exp_continue
 }
 "*?assword:*" {
  send -- "$pwd\r"
  expect "*added:*"
  interact
 }
}
EOF
fi
}

#Create a rancher vm boot script
create_rancher_sh(){
 local inet=${2:-$OS_NETWORK}
 local script_file=${3:-rancher.sh}
 local mtu=${OS_MTU:-\$(ip -c a | grep $inet | awk '{print \$4}')}
cat > $script_file << EOF
#!/bin/sh
ln -snf /usr/share/zoneinfo/$TIMEZONE /etc/localtime 
echo "$TIMEZONE" > /etc/timezone
apt-get update 
DEBIAN_FRONTEND=noninteractive apt-get install jq docker.io -y	
snap install yq
mtu=$mtu
if [ -n "\$mtu" ] ;then
 cp /lib/systemd/system/docker.service /etc/systemd/system/docker.service
 sed -i "/ExecStart/ s/\$/ --mtu \$mtu/g" /etc/systemd/system/docker.service
 systemctl daemon-reload
fi
systemctl restart docker		
systemctl enable docker
$1
EOF
}

#Update the DNS records by calling OS1
update_dns(){
local domain=${1:-$DOMAIN}
echo "Updating DNS"
#Run the domain update
/usr/bin/os1 'hosts' "$domain" "$DEFAULT_CLUSTER"
}

#Add a microstack node
microstack_add_node() {
 local nodeName=$1
 local pwd=${2:-$password}
 local domain=${3:-${DOMAIN}}
 echo "Adding node $nodeName"
 if [ -z "$pwd" ]; then
    echo "Please set a linux password for $user using the -p option"
    exit 1
 fi
if [ -n "$nodeName" ] ;then
 local server_ip=$SERVER_IP
 local token=$(microstack add-compute | head -n 2 | tail -n 1)  
 copy_ssh_id $nodeName
${SSH}${nodeName} bash << EOF
 if [ -n "$(snap aliases 2>/dev/null | grep openstack | awk {'print $1'})" ] ;then   
   #We assume when microstack is not install we need to run kernel settings  
   bash <(curl -sfLN "$OS1_REPO/run.sh") -p "$pwd" install "sudoers,kernel,os1,auto-update"
   sudo sed -i '/# Begin OpenStack/,/# End of OpenStack/d' /etc/hosts
   echo -e "# Begin OpenStack\n$server_ip rancher.$domain\n# End of OpenStack" | sudo tee -a /etc/hosts >/dev/null
   echo "Installing microstack"
   sudo apt-get install snapd -y
   sudo snap install microstack --devmode --beta
   sudo microstack init --auto --compute --join $token
   sudo snap alias microstack.openstack openstack
   sudo sed -i '/[DEFAULT]/a resume_guests_state_on_host_boot = True' /var/snap/microstack/common/etc/nova/nova.conf.d/nova-snap.conf
   sudo systemctl restart snap.microstack.nova-compute
 else
  echo "Microstack allready installed skipping"
  exit 1
 fi
EOF
fi
}

#Add a microstack node
microstack_remove_node(){
 local nodeName=${1}
 local pwd=${2:-${password}}
 if [ -n "$pwd" ];then
  pwd="-p '$pwd'"
 fi
 local domain=${3:-${DOMAIN}}
 echo "Removing node $nodeName"
 if [ -n "$nodeName" ] ;then 
      #Remove the node from controle node
      local n=$(host $nodeName | awk '/pointer/ { print $5 }' | awk -F. '{print $1}' | head -1)
      if [ -z "$n" ] ;then
        n=$nodeName
      fi
      ${SSH}${n} bash << EOF
        sudo snap remove microstack
        bash <(curl -sfLN "$OS1_REPO/run.sh") $pwd uninstall "os1,kernel,sudoers"
EOF
    if [ -n "$(snap aliases 2>/dev/null | grep openstack | awk {'print $1'})" ] ;then  
      echo "OpenStack remove $nodeName"
      for service in $(openstack compute service list --host $nodeName -c ID -f value) ;do
        openstack compute service delete $service
      done 
    fi
  fi
}

#Create a new vm on openstack
openstack_create(){
  local name=${1}
  local BASE_NET=${2:-local}
  local size=${3:-small}
  local sub_ip=$(echo $SERVER_IP | awk -F. '{print $3 }')
  size=$(echo $size | sed 's/\./ --availability-zone nova:/g')
  local i=0
  local ip=0
  while [ $i -lt 10 ] ;do
    local ip=$(( i + 20))
    size=$(echo $size | sed "s/node${i}/os1-${sub_ip}-${ip}/g")
    i=$(( i + 1 ))
  done
  local init_script=${4:-rancher.sh}
  local image=${5:-$OS_BASEIMAGE}
  local domain=${6:-$DOMAIN}
  local os_project=""
  if [ -n "$OS_PROJECT" ]; then
     os_project="--os-project-name $OS_PROJECT"
  fi
  local IP=$(openstack floating ip create -f value -c floating_ip_address --subnet external-subnet external $os_project)
  sed -i "s/{{IP}}/${IP}/g" "$init_script"
  openstack server create $name --image $image --key-name $BASE_NET --flavor k8s.$size --network $BASE_NET --security-group ssh_group --security-group kubernetes --user-data "$init_script" $os_project  >/dev/null
  openstack server add floating ip $name $IP $os_project >/dev/null
}

rancher_set_server(){
 local domain=${1:-$DOMAIN}
 RANCHER_SERVER="${RANCHER_SERVERNAME:-rancher}.$domain"
}

rancher_delete_token(){
  token=${1:-$RANCHER_TOKEN}
  rancher_set_server $2
  token=$(echo "$token" | cut -f1 -d":")
  curl -sX DELETE "https:/$RANCHER_SERVER/v3/tokens/$token" -H 'content-type: application/json' -H "Authorization: Bearer $RANCHER_TOKEN"  --insecure
}

rancher_logout(){
  local token=${1:-$RANCHER_TOKEN}
  rancher_set_server $2
  curl -sX POST "https:/$RANCHER_SERVER/v3/tokens?action=logout" -H 'content-type: application/json' -H "Authorization: Bearer $token"  --insecure
  if [ -n "$1" ] ;then
    unset RANCHER_TOKEN
  fi
}

#login to rancher
rancher_login(){
 if [ -n "$RANCHER_TOKEN" ] ;then
  return 0
 fi
 { #try
 rancher_set_server $1
 local username=${2:-admin}
 local adminpwd=${3:-$OS1_PWD}
 if [ -z "$adminpwd" ]; then
   echo "Token or Password required to login"
   return 1
 fi
 local timeout=${4:-15}
 local loginOnly=${5:-yes}

 # Login
 local LOGINRESPONSE=$(curl -s "https://$RANCHER_SERVER/v3-public/localProviders/local?action=login" -H 'content-type: application/json' --data-binary " {\"username\":\"$username\",\"password\":\"$adminpwd\"}" --insecure)
 local LOGINTOKEN=$(echo $LOGINRESPONSE | jq -e -r ".token" 2>/dev/null)
 if [ -z "$LOGINTOKEN" ] ; then
   echo "Login Error: $LOGINRESPONSE"
   return 1
 fi

 #Should we only login
 if [ "$loginOnly" == "yes" ] ;then
   RANCHER_TOKEN=$LOGINTOKEN
   return 0
 fi

 # Create data for API key
 local data=$(echo '{"current":false,"enabled":true,"expired":false,"isDerived":false,"ttl":#TTL#,"type":"token","description":"os1 automation"}' | sed "s/#TTL#/$(( timeout * 60000))/g")
 local APIRESPONSE=$(curl -s "https://$RANCHER_SERVER/v3/token" -H 'content-type: application/json' -H "Authorization: Bearer $LOGINTOKEN" --data-binary  "$data"  --insecure)
 #Logout
 rancher_logout $LOGINTOKEN
 # Extract and store token global
 RANCHER_TOKEN=$(echo $APIRESPONSE | jq -e -r ".token" 2>/dev/null)
 #Delete the login token
 if [ -z "$RANCHER_TOKEN" ]; then
   echo "API Error: $APIRESPONSE"
   return 1
 fi
 return 0
 } || { #catch
    return 1
 }
}

#Get the cluster id
rancher_cluster_id(){
 local name=${1:-$CLUSTER}
 local domain=${2:-$DOMAIN}
 rancher_set_server $2
 unset CLUSTERID
 unset RESPONSE
 if rancher_login $domain  ;then
  RESPONSE=$(curl -s "https://$RANCHER_SERVER/v3/clusters?limit=-1&sort=name"  -H "Authorization: Bearer $RANCHER_TOKEN" --insecure)
  CLUSTERID=$(echo $RESPONSE | jq -e -r ".data[]| select(.name == \"$name\") | .id" 2>/dev/null)
  if [ -n "$CLUSTERID" ] ; then
    return 0
  fi
 fi
 return 1
}

wait_spinner(){
 local TIMEOUT=${1:-0}
 if [ "${TIMEOUT}" -le 0 ]; then
    return 0
 fi
 local l=0
 local i=1
 local sp="/-\|"
 while [ "$l" -ne 4 ] ;do
  sleep 2
  printf "\r${sp:i++%${#sp}:1}"
  l=$((l + 1))
 done
 return $((TIMEOUT - l * 2))
}

#Check if cluster is ready
rancher_cluster_ready(){
local name=${1:-$CLUSTER}
 local domain=${2:-$DOMAIN}
 local TIMEOUT=${3:-1200}
 local i=1
 local sp="/-\|"
 echo -n "  Waiting for cluster $name to become available"
 while true ;do
   if rancher_cluster_id $name $domain ;then
     local state=$(echo $RESPONSE | jq -e -r ".data[]| select(.name == \"$name\") | .state" 2>/dev/null)
     if [ "$state" == "active" ]; then
       return 0
     fi
   fi
   if [ "${TIMEOUT}" -le 0 ]; then
      return 0
   fi
   local l=0
   while [ "$l" -ne 10 ] ;do
    sleep 2
    printf "\r${sp:i++%${#sp}:1}"
    l=$((l + 1))
   done
   TIMEOUT=$((TIMEOUT - l * 2))
 done
}

#Search for userID base on username
rancher_user_id(){
 local name=${1}
 local domain=${2:-$DOMAIN}
 rancher_set_server $2
 unset USERID
 if rancher_login $domain  ;then
  RESPONSE=$(curl -s "https://$RANCHER_SERVER/v3/user?limit=-1&sort=name"  -H "Authorization: Bearer $RANCHER_TOKEN" --insecure | jq -e -r ".data[]| select(.username == \"${name,,}\")")
  USERID=$(echo $RESPONSE | jq -e -r ".id" 2>/dev/null)
  if [ -n "$USERID" ] ; then
    return 0
  fi
 fi
 return 1
}

#Get projectid if does not exit create it
rancher_project(){
 local prj=${1}
 local name=${2:-$CLUSTER}
 local replace=${3:-no}
 rancher_set_server $4
 if rancher_cluster_id $name ;then
   if [ "$replace" != "yes" ]; then
     PROJECTID=$(curl -s "https://$RANCHER_SERVER/v3/projects"  -H "Authorization: Bearer $RANCHER_TOKEN"  --insecure |  jq -e -r ".data[] | select(.clusterId == \"${CLUSTERID}\") | select(.name == \"${prj}\") | .id" 2>/dev/null)
     if  [ -n "$PROJECTID" ] ;then
       return 0
     fi
   fi

   PROJECTID=$(curl -sX POST "https://$RANCHER_SERVER/v3/project?_replace=true" -H 'content-type: application/json'  -H "Authorization: Bearer $RANCHER_TOKEN" --data-binary '{"enableProjectMonitoring": false, "type": "project", "name": "'$prj'", "clusterId": "'$CLUSTERID'", "labels" : {}}'  --insecure |  jq -e -r ".id" 2>/dev/null)
   if  [ -n "$PROJECTID" ] && [ "$PROJECTID" != "null" ] ; then
       return 0
   fi
   echo "Failed to create Project: $prj"
 fi
 return 1
}


#Add/assign a namespace to project
rancher_project_namespace(){
 local prj=${1}
 local namespace=${2,,}
 local name=${3:-$CLUSTER}
 rancher_set_server $4
 if rancher_cluster_id "$name" ;then
  if rancher_project "$prj" "$name" "no" $4 ;then
    #Check if name space exists
    NAMESPACEID=$(curl -s "https://$RANCHER_SERVER/v3/clusters/$CLUSTERID/namespace"  -H "Authorization: Bearer $RANCHER_TOKEN"  --insecure | jq -e -r ".data[] | select(.name == \"${namespace}\")| .id" 2>/dev/null)
     if [ -n "$NAMESPACEID" ] ;then
       curl -sX POST "https://$RANCHER_SERVER/v3/cluster/$CLUSTERID/namespaces/$namespace?action=move" -H 'content-type: application/json' -H "Authorization: Bearer $RANCHER_TOKEN" --data-binary '{"projectId":"'$PROJECTID'"}' --insecure
       return 0
     fi

    NAMESPACEID=$(curl -sX POST "https://$RANCHER_SERVER/v3/clusters/$CLUSTERID/namespace" -H 'content-type: application/json' -H "Authorization: Bearer $RANCHER_TOKEN" --data-binary '{"clusterId": "'$CLUSTERID'","labels": {},"name": "'$namespace'","projectId": "'$PROJECTID'", "resourceQuota": null,"type": "namespace"}' --insecure | jq -e -r ".id" - 2>/dev/null)
    if [ "$NAMESPACEID" == "$namespace" ] && [ "$NAMESPACEID" != "null" ] ;then
      return 0
    fi
   fi
   echo "Failed to create Namespace $namespace"
 fi
 return 1
}

rancher_kubectl(){
 local name=${1:-$CLUSTER}
 local filename=${2:-~/.kube/config-${name}}
 local external=${3:-no}
 rancher_set_server $4
 local domain=${4:-$DOMAIN}
 local overwrite=${5:-no}
 local logout={$6:-yes}
 if [ "$overwrite" == "no" ]  && [ -f "$filename" ]; then
  return 0
 fi
 local ret=1
 if rancher_cluster_id $name ;then
  curl -sX POST "https://$RANCHER_SERVER/v3/clusters/$CLUSTERID?action=generateKubeconfig"  -H "Authorization: Bearer $RANCHER_TOKEN" --insecure | yq -P e ".config" - > $filename
  if [ "$external" == "yes" ] ;then
    sed -i "s/$domain/$domain:6443/g" $filename
  fi
  chmod go-rwx $filename
  echo "Downloaded kubeconfigfile $filename for cluster $name"
  ret=0
 fi
 if [ "$logout" == "yes" ] ;then
   rancher_logout
 fi
 return $ret
}

rancher_import_cluster(){
  local name=${1:-$CLUSTER}
  local masterIp=${2}
  local domain=${3:-$DOMAIN}
  rancher_set_server $domain
  local id=0
  if rancher_login $domain  ;then
    if rancher_cluster_id $name ;then
      echo -e "${RED}Cluster $name Already exists${NC}"
      return 2
    fi

    local CLUSTERRESPONSE=$(curl -s "https://$RANCHER_SERVER/v3/cluster?_replace=true" -H 'content-type: application/json' -H "Authorization: Bearer $RANCHER_TOKEN" --data-binary "{\"dockerRootDir\":\"/var/lib/docker\",\"enableNetworkPolicy\":false,\"windowsPreferedCluster\":false,\"enableClusterAlerting\": false,\"enableClusterMonitoring\": false,\"type\":\"cluster\",\"name\":\"$name\"}" --insecure)
    # Extract clusterid to use for generating the docker run command
    CLUSTERID=$(echo $CLUSTERRESPONSE | jq -e -r ".id" 2>/dev/null)

    if [ -z "$CLUSTERID" ];then
     echo -e "${RED}Failed to create cluster: $CLUSTERRESPONSE ${NC}"
     return 3
    fi

    # Create token and remove rancher port
    local CMD=$(curl -s "https:/$RANCHER_SERVER/v3/clusterregistrationtoken" -H 'content-type: application/json' -H "Authorization: Bearer $RANCHER_TOKEN" --data-binary "{\"type\":\"clusterRegistrationToken\",\"clusterId\":\"$CLUSTERID\"}" --insecure |  jq -e -r ".command" 2>/dev/null | sed 's/:8443//g')

    if [ -z "$CMD" ] ;then
      echo -e "${RED}Failed to create cluster command${NC}"
      return 4
    fi

    ${SSH}$masterIp  bash << EOF
     $CMD
EOF
    return 0

  fi
  return 1
}

destroy_k3s_cluster(){
 local name=${1:-$K3S_CLUSTER}
 local masterIp=${2:-$masterIp}
 local clientIps=${3:-$clientIps}
 local user=${4:-ubuntu}
 if [ -z "$name" ] || [ -z "$masterIp" ] ; then
   echo "Clustername $name and masterip $masterIp required";
   return 1
 fi
 local ips="$masterIp $clientIps"
 rancher_set_server $domain
 if rancher_login $domain  ;then
   #Destroy the cluster in rancher, we use the CLI for this by executing in bash we ensure token is cleared
   if rancher_cluster_id $name ;then
     local response=$(curl -sX DELETE "https://$RANCHER_SERVER/v3/clusters/$CLUSTERID"  -H "Authorization: Bearer $RANCHER_TOKEN" --insecure)

    #Destroy the k3s servers
    echo "Removing servers"
    local SSH="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ${SSH_OPTIONS} -q ${user}@"
    for ip in $ips ; do
       if [ -z "$ip" ];then
         continue
       fi
       #Now we can configure the real host

      ${SSH}$ip bash << EOF
      #Clear K3S
        if [ -f "/usr/local/bin/k3s-uninstall.sh" ] ;then
        sudo /usr/local/bin/k3s-uninstall.sh
        fi
        if [ -f "/usr/local/bin/k3s-agent-uninstall.sh" ] ;then
        sudo /usr/local/bin/k3s-agent-uninstall.sh
        fi
        sudo rm -rf /var/lib/rancher
EOF
    done
   else
    echo -e "${RED}Cluster not found $name${NC}"
   fi
 fi
}

create_k3s_cluster(){
 if [ -z "$password" ]; then
   echo "Password needs to be set"
   exit 1
 fi
 local k3s_cluster=${1:-$K3S_CLUSTER}
 local masterIp=${2:-$masterIp}
 local clientIps=${3:-$clientIps}
 local user=${4:-ubuntu}
 local skip_host=${5:-false}
 if [ -z "$k3s_cluster" ] || [ -z "$masterIp" ] ; then
   echo "Clustername $k3s_cluster and masterip $masterIp required"
   return 1
 fi
 local token
 local ips="$masterIp $clientIps"

 #Run the first installation and password changes
 setup_first_pwd "$ips" "$password" "$user"
 local SSH="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ${SSH_OPTIONS} -q ${user}@"

 if [ "$skip_host" == "false" ];then
 for ip in $ips ; do
 if [ -z "$ip" ];then
   continue
 fi
 #Now we can configure the real host
${SSH}$ip bash << EOF
  export HOSTNAME=k3s-$(echo "$ip" | awk -F. '{print $3 "-" $4}')
  echo "Start installing \$HOSTNAME"
  sudo hostname \${HOSTNAME}
  #Now we change host name for each machine you create
  sudo bash -c "echo '\${HOSTNAME}' > /etc/hostname"
  sudo sed -i '/# Begin of section OS1/,/<# End of section OS1>/d' /etc/hosts
  sudo sed -i '/\$HOSTNAME/d' /etc/hosts
  sudo bash -c "echo -e \"# Begin of section OS1\n127.0.0.1 \$HOSTNAME \${HOSTNAME}.local\n# End of section OS1\" >> /etc/hosts"

  #Update system and install required packages
  sudo apt-get update
  #Enable the usage of usb storage devices using extfat, git
  sudo apt-get install git exfat-fuse avahi-utils unattended-upgrades -y
  #
  ##Fix problem with name resolving
  sudo ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf
  sudo sed -i 's/#domain-name=local/domain-name=local/g' /etc/avahi/avahi-daemon.conf
  ##Allow running unattended updates
  sudo sed -i 's/\/\/\t"\${distro_id}:\${distro_codename}-updates";/\t"\${distro_id}:\${distro_codename}-updates";/g' /etc/apt/apt.conf.d/50unattended-upgrades
  sudo sed -i 's/\/\/Unattended-Upgrade::Remove-Unused-Kernel-Packages "false";/Unattended-Upgrade::Remove-Unused-Kernel-Packages "true";/g' /etc/apt/apt.conf.d/50unattended-upgrades
  sudo sed -i 's/\/\/Unattended-Upgrade::Remove-Unused-Dependencies "false";/Unattended-Upgrade::Remove-Unused-Dependencies "true";/g' /etc/apt/apt.conf.d/50unattended-upgrades
  sudo sed -i 's/\/\/Unattended-Upgrade::Automatic-Reboot "false";/Unattended-Upgrade::Automatic-Reboot "true";/g' /etc/apt/apt.conf.d/50unattended-upgrades
  sudo sed -i 's/\/\/Unattended-Upgrade::Automatic-Reboot-Time "02:00";/Unattended-Upgrade::Automatic-Reboot-Time "02:38";/g' /etc/apt/apt.conf.d/50unattended-upgrades
  sudo timedatectl set-timezone $TIMEZONE
  #Enable cmemory by adding cgroup_enable=memory cgroup_memory=1 to end of /boot/firmware/nobtcmd.txt
  if [ "\$(dpkg --print-architecture)" != "amd64" ] ;then
  sudo sed -i 's/ cgroup_enable=cpuset cgroup_enable=memory cgroup_memory=1//g' /boot/firmware/cmdline.txt
  sudo sed -i '1s/.*/& cgroup_enable=cpuset cgroup_enable=memory cgroup_memory=1/' /boot/firmware/cmdline.txt
  fi
EOF
  done
 fi

 #Install K3S on the masters
 ${SSH}$masterIp bash << EOF
    #Clear K3S
    if [ -f "/usr/local/bin/k3s-uninstall.sh" ] ;then
    sudo /usr/local/bin/k3s-uninstall.sh
    fi
    if [ -f "/usr/local/bin/k3s-agent-uninstall.sh" ] ;then
    sudo /usr/local/bin/k3s-agent-uninstall.sh
    fi
    sudo rm -rf /var/lib/rancher
    export K3S_KUBECONFIG_MODE="644"
    #Download k3s and run installer
    curl -sfL https://get.k3s.io |  sh -s - --kube-controller-manager-arg pod-eviction-timeout=1m
EOF

 #Install K3s of client
 for ip in $clientIps ;do
  if [ -z "$ip" ];then
   continue
  fi
  token=$(${SSH}$masterIp sudo cat /var/lib/rancher/k3s/server/node-token)
  ${SSH}$ip bash << EOF
    #Clear K3S
    if [ -f "/usr/local/bin/k3s-uninstall.sh" ] ;then
    sudo /usr/local/bin/k3s-uninstall.sh
    fi
    if [ -f "/usr/local/bin/k3s-agent-uninstall.sh" ] ;then
    sudo /usr/local/bin/k3s-agent-uninstall.sh
    fi
    sudo rm -rf /var/lib/rancher
    export K3S_KUBECONFIG_MODE="644"
    export K3S_URL="https://${masterIp}:6443"
    export K3S_TOKEN="$token"
    curl -sfL https://get.k3s.io | sh -
EOF
  done

  #Now import the cluster
  rancher_import_cluster "$k3s_cluster" "$masterIp"
}

#Create a rancher cluster
rancher_create_cluster(){
 { #try
   local name=${1:-$CLUSTER}
   local BASE_NET=${2:-local}
   local size=${3:-ctrl}
   local workers=${4}
   local domain=${5:-$DOMAIN}
   rancher_set_server $domain
   local id=0
   if rancher_login $domain  ;then
    local os_project=""
    if [ -n "$OS_PROJECT" ]; then
      os_project="--os-project-name $OS_PROJECT"
    fi

    if rancher_cluster_id $name ;then
      echo -e "${RED}Cluster $name Already exists${NC}"
      return 2
    fi

    #Fix for the network MTU inside openstack
    local mtu=${OS_MTU:-0}
     
    echo "Creating cluster $name"
    # Create cluster
    local CLUSTERRESPONSE=$(curl -s "https://$RANCHER_SERVER/v3/cluster" -H 'content-type: application/json' -H "Authorization: Bearer $RANCHER_TOKEN" --data-binary "{\"dockerRootDir\":\"/var/lib/docker\",\"enableNetworkPolicy\":false,\"type\":\"cluster\",\"rancherKubernetesEngineConfig\":{\"addonJobTimeout\":30,\"ignoreDockerVersion\":true,\"sshAgentAuth\":false,\"type\":\"rancherKubernetesEngineConfig\",\"authentication\":{\"type\":\"authnConfig\",\"strategy\":\"x509\"},\"network\":{\"mtu\": $mtu,\"type\":\"networkConfig\",\"plugin\":\"canal\"},\"ingress\":{\"type\":\"ingressConfig\",\"provider\":\"nginx\"},\"monitoring\":{\"type\":\"monitoringConfig\",\"provider\":\"metrics-server\"},\"services\":{\"type\":\"rkeConfigServices\",\"kubeApi\":{\"podSecurityPolicy\":false,\"type\":\"kubeAPIService\"},\"etcd\":{\"snapshot\":false,\"type\":\"etcdService\",\"extraArgs\":{\"heartbeat-interval\":500,\"election-timeout\":5000}}}},\"name\":\"$name\"}" --insecure)
    # Extract clusterid to use for generating the docker run command
    CLUSTERID=$(echo $CLUSTERRESPONSE | jq -e -r ".id" 2>/dev/null)

    if [ -z "$CLUSTERID" ];then
     echo -e "${RED}Failed to create cluster: $CLUSTERRESPONSE ${NC}"
     return 3
    fi

    # Create token
    curl -s "https:/$RANCHER_SERVER/v3/clusterregistrationtoken" -H 'content-type: application/json' -H "Authorization: Bearer $RANCHER_TOKEN" --data-binary "{\"type\":\"clusterRegistrationToken\",\"clusterId\":\"$CLUSTERID\"}" --insecure > /dev/null

    # Generate nodecommand
    local AGENTCMD=$(curl -s "https://$RANCHER_SERVER/v3/clusterregistrationtoken?id=$CLUSTERID" -H 'content-type: application/json' -H "Authorization: Bearer $RANCHER_TOKEN" --insecure |  jq -e -r ".data[] | select(.clusterId == \"${CLUSTERID}\")| .nodeCommand" 2>/dev/null)
    if [ -z "$AGENTCMD" ] ;then
      echo -e "${RED}Failed to create agent command${NC}"
      return 4
    fi

    local init_script="rancher.sh"
    local id=0
    echo "Deploying Controleplane ${name}$id.$BASE_NET"
    # Create the instances on OpenStack for controleplane
    create_rancher_sh "$AGENTCMD --etcd --controlplane --worker --address {{IP}}" "$OS_NETWORK" "$init_script"
    openstack_create  "${name}$id.$BASE_NET" "$BASE_NET" "$size" "$init_script"
    
    # Create the instances on OpenStack for worker
    if [ -n "$workers" ] ;then
     create_rancher_sh "$AGENTCMD --worker --address {{IP}}" "$OS_NETWORK" "$init_script"
     workers=$(echo $workers | sed 's/,/ /g')
     for worker in $workers ;do
       id=$((id + 1 ))
       echo "Deploying worker ${name}$id.$BASE_NET"
       openstack_create  "${name}$id.$BASE_NET" "$BASE_NET" "$worker" "$init_script"
     done
    fi 
    
    #remove the rancher install script
    rm -f $init_script
    update_dns
    return 0
   else
    return 1
   fi    
 } || { #catch
    return 1
 }
}

#Destory a rancher cluster
rancher_destroy_cluster(){
 local name=${1:-$CLUSTER}
 local BASE_NET=${2:-.*}
 local domain=${3:-$DOMAIN}
 rancher_set_server $domain
 if rancher_login $domain  ;then
   #Destroy the cluster in rancher, we use the CLI for this by executing in bash we ensure token is cleared
   if rancher_cluster_id $name ;then
     local response=$(curl -sX DELETE "https://$RANCHER_SERVER/v3/clusters/$CLUSTERID"  -H "Authorization: Bearer $RANCHER_TOKEN" --insecure)

    #Destroy the openstack hardware
    echo "Removing servers"
    for server in $(openstack server list --all --name "${name}.*\.${BASE_NET}" -c ID -f value --all) ;do 
     openstack server delete $server
    done
    #Clear nodes from DNS
    update_dns
   else
    echo -e "${RED}Cluster not found $name${NC}"
   fi
 fi 
}

rancher_set_setting(){
 local id=${1}
 local value=${2}
 local domain=${3:-$DOMAIN}
 rancher_set_server $3
 if rancher_login $domain  ;then
   local response=$(curl -s "https://$RANCHER_SERVER/v3/settings/$id"  -H "Authorization: Bearer $RANCHER_TOKEN" --insecure )
   local uuid=$(echo $response | jq -e -r .uuid)
   local response=$(curl -sX PUT "https://$RANCHER_SERVER/v3/settings/$id" -H 'content-type: application/json' -H "Authorization: Bearer $RANCHER_TOKEN" --data-binary "{\"baseType\":\"setting\",\"uuid\":\"$uuid\",\"value\":\"$value\"}" --insecure)
   local ruuid=$(echo $response | jq -e -r ".uuid" 2>/dev/null)
   if [ "$ruuid" == "$uuid" ] ;then
     return 0
   else   
     echo $response
     return 1
   fi   
  fi
}

rancher_add_role(){
 local id=${1}
 local role=${2:-cluster-owner}
 local cluster=${3:-$CLUSTER}
 local domain=${4:-$DOMAIN}
 rancher_set_server $4
 local pid=""
 if rancher_login $domain  ;then
   #Check if user exists
   if rancher_user_id $id ;then
    pid=$(echo $RESPONSE | jq -e -r .principalIds[0])
   else
    return 2
   fi
   
   #Now add it to cluster
   #Find cluster
   if rancher_cluster_id $cluster ;then
     #TODO: Check if role does not exist
     #Add to cluster
     local response=$(curl -sX POST "https://$RANCHER_SERVER/v3/clusterroletemplatebinding"  -H "Authorization: Bearer $RANCHER_TOKEN"  -H 'content-type: application/json' --insecure --data-binary "{\"clusterId\":\"$CLUSTERID\",\"roleTemplateId\":\"$role\",\"type\":\"clusterRoleTemplateBinding\",\"userPrincipalId\":\"$pid\"}" )
     local uuid=$(echo $response | jq -e -r ".uuid" 2>/dev/null)
     if [ -n "$uuid" ] ;then
      return 0
     fi 
   fi   
 fi
 return 1
}

keycloak_login(){
 local usr=${1:-admin}
 local pwd=${2:-$OS1_PWD}
 local domain=${4:-$DOMAIN}
 local realm=${3:-$REALM:-$domain}
 local response=$(curl -s --data "username=${admin}&password=${OS1_PWD}&grant_type=password&client_id=admin-cli" https://sso.${domain}/auth/realms/${realm}/protocol/openid-connect/token)
 TOKEN=`echo $RESULT | sed 's/.*access_token":"//g' | sed 's/".*//g'`
 KEYCLOAK_TOKEN=$(echo $response | jq -e -r ".access_token" 2>/dev/null)
 if [ -n "$KEYCLOAK_TOKEN" ] ;then
    return 0
 fi
 return 1
}

keycloak_logout(){
  local domain=${4:-$DOMAIN}
  if [ -n "$KEYCLOAK_TOKEN" ] ;then
    local responce=$(curl -sX POST https://sso.$domain/logout --header "Authorization: Bearer ${KEYCLOAK_TOKEN}" --header 'Accept: application/json')
  fi
  return 0
}

keycloak_add_user(){
 local id=${1}
 local name=${2}
 local domain=${7:-$DOMAIN}
 local email=${3:-$id@$domain}
 local pwd=${4:-$id}
 local change=${5:-true}
 local role=${6:-rancher_user}
 local realm=${8:-$REALM:-$domain}

 if keycloak_login "" "" $realm ;then
   reponse=$(curl -v https://sso.${domain}/auth/admin/realms/${realm}/users -H "Content-Type: application/json" -H "Authorization: bearer $KEYCLOAK_TOKEN"   --data '{"firstName":"'$id'","lastName":"'$name'", "email":"'$email'", "enabled":"true"}')
   #TODO Add Error Handling
   #Assign the role=group
   return 0
 fi
 return 1
}

rancher_add_user(){
 local id=${1}
 local name=${2}
 local domain=${7:-$DOMAIN}
 local email=${3:-$id@$domain}
 local pwd=${4:-$id}
 local change=${5:-true}
 local role=${6:-user}
 rancher_set_server $7

 local pid=""
 if rancher_login $domain  ;then
   #Check if user exists
   if rancher_user_id $id ;then
    return 2
   else
    #Create user
    local response=$(curl -sX POST "https://$RANCHER_SERVER/v3/user"  -H "Authorization: Bearer $RANCHER_TOKEN"  -H 'content-type: application/json' --insecure --data-binary "{\"enabled\":true,\"mustChangePassword\":$change,\"type\":\"user\", \"name\":\"$name\",\"username\":\"${id,,}\",\"password\":\"$pwd\"}" )
    pid=$(echo $response | jq -e -r ".id" 2>/dev/null)
    #Give Role Binding
    if [ -n "$pid" ] ;then
      local response=$(curl -sX POST "https://$RANCHER_SERVER/v3/globalrolebinding"  -H "Authorization: Bearer $RANCHER_TOKEN"  -H 'content-type: application/json' --insecure --data-binary "{\"globalRoleId\":\"$role\",\"type\":\"globalRoleBinding\", \"userId\":\"$pid\"}" )
      pid=$(echo $response | jq -e -r ".id" 2>/dev/null)
      if [ -n "$pid" ] ;then
        return 0
      fi
    fi  
   fi   
 fi
 return 1
}

rancher_remove_user(){
 local id=${1}
 local domain=${2:-$DOMAIN}
 rancher_set_server $2

 if [ "${id,,}" == "admin" ] ;then
   retrun 1
 fi 
 if rancher_login $domain  ;then

   #Check if user exists
   if rancher_user_id $id ;then
    #remove user
    local response=$(curl -sX DELETE "https://$RANCHER_SERVER/v3/user/$USERID"  -H "Authorization: Bearer $RANCHER_TOKEN" )
    local state=$(echo $response | jq -e -r ".state" 2>/dev/null)
     if  [  "$state" == "removing" ] ;then
      return 0
     fi  
   fi   
 fi
 return 1
}


rancher_remove_role(){
 local id=${1}
 local role=${2}
 local cluster=${3:-$CLUSTER}
 local domain=${4:-$DOMAIN}
 rancher_set_server $4

 if rancher_login $domain  ;then
   #Check if user exists
   if rancher_user_id $id ;then
    #remove user
    if rancher_cluster_id $cluster ;then
      RESPONSE=$(curl -s "https://$RANCHER_SERVER/v3/clusterRoleTemplateBindings?limit=-1&sort=name"  -H "Authorization: Bearer $RANCHER_TOKEN" --insecure)
      if [ -n "$role" ] ;then
       ROLEIDS=$(echo $RESPONSE | jq -e -r ".data[]| select(.roleTemplateId == \"$role\") | select(.clusterId == \"$CLUSTERID\") | select(.userId == \"$USERID\") | .id" 2>/dev/null)
      else
        ROLEIDS=$(echo $RESPONSE | jq -e -r ".data[]| select(.clusterId == \"$CLUSTERID\") | select(.userId == \"$USERID\") | .id" 2>/dev/null)
      fi 
      for roleid in $ROLEIDS ;do
        local response=$(curl -sX DELETE "https://$RANCHER_SERVER/v3/clusterRoleTemplateBindings/$roleid"  -H "Authorization: Bearer $RANCHER_TOKEN" )
      done
    fi   
   fi   
 fi
}

#Scripts called by drone.io

#Deploy a service using a yaml
drone_io_deploy(){
 local cluster=${PLUGIN_CLUSTER:-$CLUSTER}
 local project=${PLUGIN_PROJECT:-default}
 local domain=${PLUGIN_DOMAIN:-$DOMAIN}
 local namespaces=$(echo ${PLUGIN_NAMESPACE} ${PLUGIN_NAMESPACES} | sed 's/,/ /g' | sed 's/^[ \t]*//;s/[ \t]*$//')
 local yamls=$(echo ${PLUGIN_YAML} | sed 's/,/ /g' | sed 's/^[ \t]*//;s/[ \t]*$//')
 if [ -z "$namespaces" ]; then
   namespaces="${project}"
 fi
 RANCHER_TOKEN=${PLUGIN_TOKEN}

 if rancher_login "$domain" "${PLUGIN_USER}" "${PLUGIN_PASSWORD}" ;then
   #Create the required namespaces and assign them to the project
   for namespace in $namespaces ;do

     if rancher_project_namespace $project $namespace $cluster ;then
       echo "Project $project and namespace $namespace created"
     else
       echo "Failed to create project/namespace: $project/$namespace"
       exit 1
     fi
   done
   local filename="/tmp/kubeconfig-$cluster"
   if rancher_kubectl "$cluster" "$filename" "${PLUGIN_EXTERNAL:-no}" "$domain" "yes" "no" ;then
     export KUBECONFIG="$filename"
     local pm="--kubeconfig $filename --wait"
     if [ -n "$PLUGIN_NAMESPACE" ];then
       pm="$pm -n $PLUGIN_NAMESPACE"
     fi
     _TMPE=/tmp/os1-error

     #BEFORE Statements
     IFS='§' read -r -a BEFORES <<< $(echo ${PLUGIN_BEFORE} | sed -e ':a;s/^\(\("[^"]*"\|[^",]*\)*\),/\1§/;ta')
     for LINE in "${BEFORES[@]}"; do
        local err=0
        rm -f $_TMPE
        local result; result=$(eval ${LINE} 2>$_TMPE)
        if [ $? -ne 0 ] || [ -n "$(cat $_TMPE 2>/dev/null | grep rror)" ] || [ -n "$(cat $_TMPE 2>/dev/null | grep nvalid)" ] ;then
          err=1
          echo -e "Before ${RED}Failed${NC} : ${LINE}"
          cat $_TMPE
        else
          echo -e "Before ${GREEN}Ok${NC} : ${LINE}"
        fi
        echo -e "${result}"
        if [ $err -ne  0 ];then
          exit 1
        fi
     done

     #YAML statements
     if [ -n "$yamls" ];then
       load_source "templater"
       local err=0
       #Now got to the files to deploy
       for yaml in $yamls ;do
         if [ -f "$yaml" ];then
           rm -f $_TMPE
           local result;result=$(templater "$yaml" "${PLUGIN_CONFIGFILE}" | kubectl apply $pm -f - 2>$_TMPE)
           if [ $? -ne 0 ] || [ -n "$(cat $_TMPE 2>/dev/null | grep rror)" ] || [ -n "$(cat $_TMPE 2>/dev/null | grep nvalid)" ] ;then
             err=1
             echo -e "Deployed ${RED}Failed${NC} : ${yaml}"
             cat $_TMPE
           else
             echo -e "Deployed ${GREEN}Ok${NC} : ${yaml}"
           fi
           echo -e "${result}"
           #Validate the result
           if [ $err -ne  0 ];then
             exit 1
           fi
         else
          echo -e "File${RED}$filename${NC} does not exists"
          exit 1
         fi
       done
      fi

     #AFTER Statements
     IFS='§' read -r -a AFTERS <<< $(echo ${PLUGIN_AFTER} | sed -e ':a;s/^\(\("[^"]*"\|[^",]*\)*\),/\1§/;ta')
     for LINE in "${AFTERS[@]}"; do
        local err=0
        rm -f $_TMPE
        local result;result=$(eval ${LINE} 2>$_TMPE)
        if [ $? -ne 0 ] || [ -n "$(cat $_TMPE 2>/dev/null | grep rror)" ] || [ -n "$(cat $_TMPE 2>/dev/null | grep nvalid)" ] ;then
          err=1
          echo -e "After ${RED}Failed${NC} : ${LINE}"
          cat $_TMPE
        else
          echo -e "After ${GREEN}Ok${NC} : ${LINE}"
        fi
        echo -e "${result}"
        if [ $err -ne  0 ];then
          exit 1
        fi
     done
   fi
   rm -f $filename
   #Logout if we did not use token
   if [ -z "${PLUGIN_TOKEN}" ] ;then
     rancher_logout $RANCHER_TOKEN
   fi
   unset RANCHER_TOKEN
 else
  echo "Login failed"
  exit 1
 fi
 return 0
}

#Change first ubuntu password
#param1: ip(s) of host
setup_first_pwd() {
  local ip
  local ips="$1"
  local password="$2"
  local usr=${3:-ubuntu}
  for ip in $ips ; do
 cat << EOF | expect -f -
set timeout 15
spawn ssh ${usr}@${ip}
match_max 1000
expect {
 "*fingerprint*" {
  send -- "yes\r"
  exp_continue
 }
 "*elcome to Ubuntu*" {
   send -- "exit\r"
   close
 }
 "*?assword:*" {
  send -- "ubuntu\r"
  expect {
   "*ermission denied*" {
    send -- \x03
    close
   }
   "*?assword:*" {
    expect "*?assword:*"
    send -- "ubuntu\r"
    send -- "$password\r"
    expect "*?assword:*"
    send -- "$password\r"
    expect "*?assword updated*"
    interact
   }
  }
 }
}
EOF
#Create key pair
if [ ! -f ~/.ssh/id_rsa ]; then
  ssh-keygen -q -N "" -f ~/.ssh/id_rsa 2>/dev/null <<< y >/dev/null
fi
#now copy the ssh key
cat << EOF | expect -f -
set timeout 15
spawn ssh-copy-id ${usr}@${ip}
match_max 1000
expect {
 "*fingerprint*" {
  send -- "yes\r"
  exp_continue
 }
 "*?assword:*" {
  send -- "$password\r"
  expect "*added:*"
  interact
 }
}
EOF
#Enable sudo for the user
#echo "$usr ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/$usr
#chmod 0440 /etc/sudoers.d/$usr
  done
}
