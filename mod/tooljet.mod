tooljet_config(){
 cat > $config_dir/tooljet/.env <<EOF
 # Create .env from this example file and replace values for the environment.
# The application expects a separate .env.test for test environment configuration
# Get detailed information about each variable here: https://docs.tooljet.com/docs/setup/env-vars

TOOLJET_HOST=https://tooljet.$DOMAIN
LOCKBOX_MASTER_KEY=${LOCKBOX_MASTER_KEY:-$(echo $RANDOM | md5sum | head -c 32)}
SECRET_KEY_BASE=${SECRET_KEY_BASE:-$(echo $RANDOM | md5sum | head -c 64)}

# DATABASE CONFIG
ORM_LOGGING=all
PG_DB=tooljet_production
PG_USER=postgres
PG_HOST=postgres
PG_PASS=postgres

# Checks every 24 hours to see if a new version of ToolJet is available
# (Enabled by default. Set 0 to disable)
CHECK_FOR_UPDATES=0

# Checks every 24 hours to update app telemetry data to ToolJet hub.
# (Telemetry is enabled by default. Set value to true to disable.)
# DISABLE_APP_TELEMETRY=false

#GOOGLE_CLIENT_ID=
#GOOGLE_CLIENT_SECRET=

# DISABLE USER SIGNUPS (true or false). Default: true
DISABLE_SIGNUPS=true

# OBSERVABILITY
#APM_VENDOR=
#SENTRY_DNS=
#SENTRY_DEBUG=

# FEATURE TOGGLE
#COMMENT_FEATURE_ENABLE=
ENABLE_MULTIPLAYER_EDITING=true

#SSO
#SSO_DISABLE_SIGNUP=
#SSO_RESTRICTED_DOMAIN=
#SSO_GOOGLE_OAUTH2_CLIENT_ID=
#SSO_GIT_OAUTH2_CLIENT_ID=
#SSO_GIT_OAUTH2_CLIENT_SECRET=

#TELEMETRY
DEPLOYMENT_PLATFORM=docker
EOF

if [ -n "$NOREPLY_SMTP_PWD" ]; then
cat >> $config_dir/tooljet/.env <<EOF
# SMTP (mail sending) settings
DEFAULT_FROM_EMAIL='"tooljet" <no-reply@${DOMAIN}>'
SMTP_USERNAME=no-reply@${DOMAIN}
SMTP_PASSWORD=${NOREPLY_SMTP_PWD}
SMTP_DOMAIN=mail.${DOMAIN}
SMTP_PORT=587
EOF
fi
}

mod_tooljet(){
get_files "docker-compose.yml" $config_dir/tooljet "{{FILES_PATH}}/tooljet" "" "yes"
tooljet_config
cd $config_dir/tooljet
docker-compose up -d
docker-compose run server npm run db:seed
cd 
load_source "proxy"
add_proxy "20-tooljet"
}

umod_tooljet(){
  cd $config_dir/tooljet
  docker-compose down
  cd 
  sudo rm -rf $config_dir/tooljet
  sudo rm -rf ${DATA_DIR}/tooljet
}

upgrade_tooljet(){
  get_files "docker-compose.yml" $config_dir/tooljet "{{FILES_PATH}}/tooljet" "" "yes"
  tooljet_config 
  cd $config_dir/tooljet
  docker-compose pull
  docker-compose down
  docker-compose up -d
  cd
}