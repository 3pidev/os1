mod_energy(){
sudo mkdir -p $DATA_DIR/energy
docker run \
  -d \
  --name energy \
  --pull=always \
  -v $DATA_DIR:/app/.DB \
  -e OPENWEATHERMAP_API_KEY=$OPENWEATHERMAP_API_KEY \
  -p 15000:5000 \
  --restart unless-stopped \
  3pidev/smartbatteryscheduler:latest
  load_source "proxy"
  local_proxy "energy" "15000"
}

umod_energy(){
docker rm --force energy
}

upgrade_energy(){
    umod_energy
    mod_energy
}