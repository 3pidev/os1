#Add the set of fixed hostnames
mod_dns(){
local domain=${1:-$DOMAIN}
local server_ip=$SERVER_IP
local base=${2:-$BASE_IP}
local sub_ip=$(echo $SERVER_IP | awk -F. '{print $3}')

echo "Install DNSmasq"
sudo bash << END_SUDO

#Install a simple server
apt-get install dnsmasq -y
#sed -i 's|After=network.target|After=network-online.target|g' /lib/systemd/system/dnsmasq.service
sed -i 's|Wants=nss-lookup.target|Wants=network-online.target|g' /lib/systemd/system/dnsmasq.service
systemctl daemon-reload
cat > /etc/dnsmasq.conf << EOF
#/etc/dnsmasq.conf
domain-needed
bogus-priv
expand-hosts
 
# The address is the static IP of this server 
# You can find this ip by running ip -c a and look for the
# IP of the interface which is connected to the router.
listen-address=::1,127.0.0.1
bind-interfaces
 
# Use open source DNS servers and main router, google and cisco
server=8.8.4.4
server=208.67.222.222
EOF
systemctl restart dnsmasq
rm -rf /etc/resolv.conf
cat > /etc/resolv.conf << EOF
nameserver 127.0.0.1
search .
EOF
# To prevent adding sudo for all commands we do it once
END_SUDO
}

umod_dns(){
sudo bash << END_SUDO
echo "Removing dns"
sudo sed -i '/# Begin FixedHosts/,/# End FixedHosts/d' /etc/hosts 
apt-get remove dnsmasq -y
apt-get autoremove -y
rm -rf /etc/resolv.conf
rm -rf /etc/resolvconf.conf
ln -sf /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
END_SUDO
}

#We don't support upgrade
upgrade_dns(){
return 0
}
