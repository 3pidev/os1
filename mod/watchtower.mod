# Watchtower will update packages automaticly once a day
mod_watchtower(){
  docker run -d \
  --restart unless-stopped \
  --name watchtower \
  -v /var/run/docker.sock:/var/run/docker.sock \
  containrrr/watchtower --interval 3600
}

umod_watchtower(){
  docker rm --force watchtower
}
