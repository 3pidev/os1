# --- Software Module -------------------------------------------
# All software install/uninstall actions will be performed on ctrl
# Software functions must start with mod_
# This script can be used to letsencrypt certificates

#Following: https://acuments.com/how-to-setup-openresty-to-auto-generate-ssl-certs.html
mod_openresty_new(){
# To prevent adding sudo for all commands we do it once
sudo bash << END_SUDO
apt-get update
wget -qO - https://openresty.org/package/pubkey.gpg | apt-key --keyring /etc/apt/trusted.gpg.d/openresty.gpg add -
apt-get -y install software-properties-common make unzip gcc wget gnupg ca-certificates libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev
add-apt-repository -y "deb http://openresty.org/package/ubuntu $(lsb_release -sc) main"
apt-get -y install openresty
service openresty start
apt-get install -y luarocks
luarocks install lua-resty-auto-ssl
mkdir /etc/resty-auto-ssl
chown www-data /etc/resty-auto-ssl
sed -i 's/RANDFILE\t\t= $ENV::HOME/#RANDFILE\t\t= $ENV::HOME/g' /etc/ssl/openssl.cnf
openssl req -new -newkey rsa:2048 -days 3650 -nodes -x509 \
   -subj '/CN=sni-support-required-for-valid-ssl' \
   -keyout /etc/ssl/resty-auto-ssl-fallback.key \
   -out /etc/ssl/resty-auto-ssl-fallback.crt
# To prevent adding sudo for all commands we do it once
END_SUDO
}

#Remove OpenResty
umod_openresty_new(){
sudo bash << END_SUDO
 systemctl stop openresty
 systemctl disable openresty
 /usr/local/openresty/luajit/bin/luarocks remove lua-resty-auto-ssl
 apt-get -y remove openresty luarocks
 rm -rf /etc/resty-auto-ssl /etc/ssl/resty-auto-ssl-fallback.key /etc/ssl/resty-auto-ssl-fallback.crt /usr/local/openresty
 apt-get autoremove -y
END_SUDO
}

mod_openresty(){
#luarcocks_version="2.0.13"
luarcocks_version="3.8.0"
openresty_version="1.19.3.1"
lua_version="5.4.4"

#DNS is required for OpenResty
if ! is_installed 'dns' ; then
 install 'dns'
fi

# To prevent adding sudo for all commands we do it once
sudo bash << END_SUDO
#------------------------------------------------------

#Install Openresty. 
#Openresty is a framework for NGINX which extends the NGINX’s capablities. In our case it allows us to run code at the time of the connection.
apt-get -y install make unzip gcc wget gnupg ca-certificates libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev certbot libreadline-dev


# Install lua, when not exists
if [ ! -f /usr/local/bin/lua ] ;then
  curl -R -O http://www.lua.org/ftp/lua-${lua_version}.tar.gz
  tar -zxf lua-${lua_version}.tar.gz
  cd lua-${lua_version}
  make linux
  make install
  cd ..
fi

# Install Openresty
wget https://openresty.org/download/openresty-$openresty_version.tar.gz
tar -xvf openresty-$openresty_version.tar.gz
cd openresty-$openresty_version/
./configure -j2 --prefix=/usr/local/openresty --with-pcre-jit
make -j2
make install
wget https://raw.githubusercontent.com/openresty/openresty-packaging/master/deb/openresty/debian/openresty.service -O /usr/local/openresty/openresty.service
ln -s /usr/local/openresty/openresty.service /etc/systemd/system/openresty.service
cd ..
cp /usr/local/openresty/nginx/conf/nginx.conf /usr/local/openresty/nginx/conf/nginx.conf.default

#Install luarocks. It is required to install the plugin we’re using, using 2.0.13 for compatability reason
wget http://luarocks.org/releases/luarocks-${luarcocks_version}.tar.gz
tar -xzvf luarocks-${luarcocks_version}.tar.gz
cd luarocks-${luarcocks_version}/
./configure --prefix=/usr/local/openresty/luajit --with-lua=/usr/local/openresty/luajit
#./configure --prefix=/usr/local/openresty/luajit --with-lua=/usr/local/openresty/luajit --with-lua-include=/usr/local/openresty/luajit/include/luajit-2.1
make
make install
#make bootstrap
cd ..

rm -rf luarocks-${luarcocks_version} luarocks-${luarcocks_version}.tar.gz
rm -rf openresty-${openresty_version} openresty-${openresty_version}.tar.gz
rm -rf lua-${lua_version} lua-${lua_version}.tar.gz

#You must still define a static ssl_certificate file for nginx to start.
sed -i 's/RANDFILE\t\t= $ENV::HOME/#RANDFILE\t\t= $ENV::HOME/g' /etc/ssl/openssl.cnf 
openssl req -new -newkey rsa:2048 -days 3650 -nodes -x509 \
   -subj '/CN=sni-support-required-for-valid-ssl' \
   -keyout /etc/ssl/resty-auto-ssl-fallback.key \
   -out /etc/ssl/resty-auto-ssl-fallback.crt
    
#Now install the plugin lua-resty-auto-ssl
mkdir -p /etc/resty-auto-ssl
chown -R www-data /etc/resty-auto-ssl
/usr/local/openresty/luajit/bin/luarocks install lua-resty-core
/usr/local/openresty/luajit/bin/luarocks install lua-resty-auto-ssl
/usr/local/openresty/luajit/bin/luarocks install lua-resty-openidc
/usr/local/openresty/luajit/bin/luarocks install kong-oidc

systemctl enable openresty
systemctl restart openresty

# To prevent adding sudo for all commands we do it once
END_SUDO
#------------------------------------------------------
}

#Remove OpenResty
umod_openresty(){
sudo bash << END_SUDO
 systemctl stop openresty
 systemctl disable openresty
 systemctl daemon-reload
 /usr/local/openresty/luajit/bin/luarocks remove lua-resty-auto-ssl
 /usr/local/openresty/luajit/bin/luarocks remove kong-oidc
 rm -rf /etc/resty-auto-ssl /etc/ssl/resty-auto-ssl-fallback.key /etc/ssl/resty-auto-ssl-fallback.crt /usr/local/openresty
END_SUDO
}