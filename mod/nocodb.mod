# https://www.nocodb.com/

mod_nocodb(){
    get_files "docker-compose.yml" $config_dir/nocodb "{{FILES_PATH}}/nocodb" "" "yes"
    cd $config_dir/nocodb
    docker-compose up -d
    cd 
    load_source "proxy"
    local_proxy "nocodb" "19180"
}

umod_nocodb(){
  cd $config_dir/nocodb
  docker-compose down
  cd 
  sudo rm -rf $config_dir/nocodb
  sudo rm -rf ${DATA_DIR}/nocodb
}