mod_spaarne(){
docker run \
  -d \
  --name spaarne \
  --pull=always \
  -v $DATA_DIR/spaarne:/app/db \
  -e JSONTEAM=${SPAARNE_TEAM:-admin} \
  -e JSONPWD=${SPAARNE_PWD:-${OS1_PWD}} \
  -e TITLE="Spaarne Robot" \
  -p 19283:1323 \
  --restart unless-stopped \
  3pidev/myfleetrobot:latest
  if [ "$1" != "no_proxy" ]; then
    load_source "proxy"
    local_proxy "spaarne" "19283"
  fi
  }

  umod_spaarne(){
      docker rm --force spaarne
  }

  upgrade_spaarne(){
    umod_spaarne
    mod_spaarne "no_proxy"
  }