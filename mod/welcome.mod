# --- Software Functions -------------------------------------------
# All software install/uninstall actions will be performed on ctrl
# Software functions must start with mod_
# Show welcome text after we finished

mod_welcome (){
  local domain="${1:-$DOMAIN}"
  local adminpwd=$OS1_PWD
  echo -e "All done. You can now login to"
  if curl --connect-timeout 1 -s --head  --request GET http://localhost:9081/auth/login/?next=/ | grep "200 OK" > /dev/null; then
   echo "OpenStack: https://openstack.$domain"
  fi
  if curl -ks https://localhost:8443/ping >/dev/null; then
   echo "Rancher: https://rancher.$domain"
  else
   if curl --insecure --connect-timeout 1 -s --head  --request GET https://rancher.$domain | grep "200 OK" > /dev/null; then
    echo "Rancher: https://rancher.$domain"
   fi
  fi
  if curl --connect-timeout 1 -s --head  --request GET http://localhost:9000 | grep "MinIO" > /dev/null; then
   echo "Minio: https://minio.$domain"
  fi   
  echo "with: admin / $adminpwd"
}