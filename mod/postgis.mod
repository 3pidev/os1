mod_postgis(){
sudo mkdir -p $DATA_DIR/postgis
docker run \
  -d \
  --name postgis \
  --pull=always \
  -v $DATA_DIR/postgis:/var/lib/postgresql/data \
  -e POSTGRES_PASSWORD=${POSTGRES_PWD:-${OS1_PWD}} \
  -e POSTGRES_USER=postgis \
  -e POSTGRES_DB=postgis \
  -p 5433:5432 \
  --restart unless-stopped \
  postgis/postgis
}

umod_postgis(){
docker rm --force postgis
}

upgrade_postgis(){
    umod_postgis
    mod_postgis
}