#New version of Rancher en rke2 in one

mod_rancherd(){
local domain="${1:-$DOMAIN}"
local adminpwd=$OS1_PWD
local kubetoken="${2:-${RKE_TOKEN:-$(echo $CLUSTER | md5sum | cut -d ' ' -f 1)}}"
local localrancher=false
if contains "$CONFIG_OPTIONS" 'local' ;then
 localrancher=true
fi
if [ -z "$(snap aliases 2>/dev/null | grep openstack | awk {'print $1'})" ] ;then
 localrancher=true
fi


#Load the support functions
load_source "functions"

#local http,https and rancherhttps ports
local phttp="3080"
local phttps="3443"
local rhttps="8443"
local TIMEOUT=${3:-800}

# To prevent adding sudo for all commands we do it once
cat > rancher.sh << EOF
#!/bin/bash
apt-get install jq -y
mkdir -p /etc/rancher/rke2
cat >/etc/rancher/rke2/config.yaml <<EOC
token: $kubetoken
tls-san:
  - $domain
EOC

#Change the ingress controller ports
mkdir -p /var/lib/rancher/rke2/server/manifests
cat >/var/lib/rancher/rke2/server/manifests/rke2-ingress-nginx-config.yaml <<EOC
apiVersion: helm.cattle.io/v1
kind: HelmChartConfig
metadata:
  name: rke2-ingress-nginx
  namespace: kube-system
spec:
  valuesContent: |-
    controller:
      extraArgs:
        http-port: $phttp
        https-port: $phttps
      containerPort:
        http: $phttp
        https: $phttps
      daemonset:
        useHostPort: true
        hostPorts:
          http: $phttp
          https: $phttps

      service:
        ports:
          http: $phttp
          https: $phttps
        targetPorts:
          http: $phttp
          https: $phttps

    defaultBackend:
      enabled: true
      service:
        servicePort: 3080
EOC

#Install Rancher
curl -sfL https://get.rancher.io | sh -

systemctl enable rancherd-server.service
systemctl start rancherd-server.service

#Wait till rancher is running
i=1
sp="/-\|"
TIMEOUT=$TIMEOUT
echo -n "  Waiting for Rancher to become available"
while true ;do
  if ! curl --connect-timeout 1 -ks https://localhost:${rhttps}/ping >/dev/null; then
     if [ "\${TIMEOUT}" -le 0 ]; then
        exit 1
     fi
     l=0
     while [ "\$l" -ne 15 ] ;do
      sleep 1
      printf "\r\${sp:i++%\${#sp}:1}"
      l=\$((l + 1))
     done
     TIMEOUT=\$((TIMEOUT - l))
  else
   echo "  Rancher is ready                       "
   break
  fi
done

rancherpwd=\$(sed -n 's/.*Password: \([0-9,a-z]\+\).*/\1/p' <<< \$(rancherd reset-admin 2>&1))
# Login
LOGINRESPONSE=\$(curl -s "https://127.0.0.1:${rhttps}/v3-public/localProviders/local?action=login" -H 'content-type: application/json' --data-binary '{"username":"admin","password":"'\$rancherpwd'"}' --insecure)
LOGINTOKEN=\$(echo \$LOGINRESPONSE | jq -e -r .token)

# Change password
curl -s "https://127.0.0.1:${rhttps}/v3/users?action=changepassword" -H 'content-type: application/json' -H "Authorization: Bearer \$LOGINTOKEN" --data-binary '{"currentPassword":"'\$rancherpwd'","newPassword":"$adminpwd"}' --insecure

# Set server-url,ui-pl and domain
RANCHER_SERVER=https://rancher.$domain
curl -s "https://127.0.0.1:${rhttps}/v3/settings/server-url" -H 'content-type: application/json' -H "Authorization: Bearer \$LOGINTOKEN" -X PUT --data-binary '{"name":"server-url","value":"'\$RANCHER_SERVER'"}' --insecure > /dev/null

curl -s "https://127.0.0.1:${rhttps}/v3/settings/ui-pl" -H 'content-type: application/json' -H "Authorization: Bearer \$LOGINTOKEN" -X PUT --data-binary '{"name":"ui-pl","value":"OpenStackOne"}' --insecure > /dev/null

curl -s "https://127.0.0.1:${rhttps}/v3/settings/ingress-ip-domain" -H 'content-type: application/json' -H "Authorization: Bearer \$LOGINTOKEN" -X PUT --data-binary "{\"name\":\"ingress-ip-domain\",\"value\":\"$domain\"}" --insecure > /dev/null

curl -s "https://127.0.0.1:${rhttps}/v3/settings/telemetry-opt" -H 'content-type: application/json' -H "Authorization: Bearer \$LOGINTOKEN" -X PUT --data-binary "{\"name\":\"telemetry-opt\",\"value\":\"out\"}" --insecure > /dev/null

#Logout
curl -sX POST "https://127.0.0.1:${rhttps}/v3/tokens?action=logout" -H 'content-type: application/json' -H "Authorization: Bearer \$LOGINTOKEN"  --insecure

EOF

if [ "$localrancher" == "true" ] ;then
 sudo bash rancher.sh
 sudo sed -i '/# Begin LocalRancher/,/# End LocalRancher/d' /etc/hosts
 echo -e "# Begin LocalRancher\n127.0.0.1 rancher rancher.$domain rancher.external\n# End LocalRancher" | sudo tee -a /etc/hosts >/dev/null
else
 openstack_create 'rancher' "$BASE_NET" 'ctrl.n0' 'rancher.sh'  >/dev/null
 #Update the dns
 update_dns $domain
fi

#Remove the rancher install script
 rm -rf rancher.sh
}

umod_rancherd(){
local domain="${1:-$DOMAIN}"
local os_project=""
local localrancher=false
if contains "$CONFIG_OPTIONS" 'local' ;then
 localrancher=true
fi
if [ -z "$(snap aliases 2>/dev/null | grep openstack | awk {'print $1'})" ] ;then
 localrancher=true
fi
if [ -n "$OS_PROJECT" ]; then
  os_project="--os-project-name $OS_PROJECT"
fi
#Load the support functions
load_source "functions"
if [ "$localrancher" == "true" ] ;then
sudo bash << END_SUDO
 rancherd-uninstall.sh 
 sed -i '/# Begin LocalRancher/,/# End LocalRancher/d' /etc/hosts
 rm -rf /etc/rancher
END_SUDO
else
 openstack server delete rancher $os_project
 update_dns $domain
fi 
}


upgrade_rancherd(){
echo "TODO: Create upgrade script rancherd"
}

