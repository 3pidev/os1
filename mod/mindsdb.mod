mod_mindsdb(){
    sudo mkdir -p $DATA_DIR/mindsdb
    sudo mkdir -p $DATA_DIR/mindsdb/storage
    get_files "config.json" "mindsdb" "{{FILES_PATH}}/mindsdb" "" "yes"
    docker run -d \
     --restart=unless-stopped\
    --name mindsdb \
    -p 47334:47334 -p 47335:47335 \
    -v $DATA_DIR/mindsdb/storage:/root/mdb_storage \
    -v $DATA_DIR/mindsdb/config.json:/root/mindsdb_config.json \
    mindsdb/mindsdb

    load_source "proxy"
   local_proxy "mindsdb" "47334"
}


umod_mindsdb(){
    docker stop mindsdb
    docker rm --force mindsdb
}