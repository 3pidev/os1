mod_jupyterlab(){
    sudo mkdir -p $DATA_DIR/jupyterlab
    sudo chmod o+w $DATA_DIR/jupyterlab
    docker run -d -p 8888:8888 \
    --name jupyterlab \
    -v $DATA_DIR/jupyterlab:/home/jovyan/work \
    --user root \
    -e GRANT_SUDO=yes \
    quay.io/jupyter/datascience-notebook:latest start-notebook.py --ServerApp.root_dir=/home/jovyan/work
  #Get the token from the docker logs jupyterlab
  load_source "proxy"
  local_proxy "jupyter" "8888"
}

umod_jupyterlab(){
    docker stop jupyterlab
    docker rm --force jupyterlab
}