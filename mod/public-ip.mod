# Script wich install frp(c/s) on a server and connects ot it opening a tunnel for public traffic
#Needs to move to config file
if [ -z "$VULTR_API_KEY" ] ; then
 echo "Vultr account is required and VULTR_API_KEY needs to be set with api-token"
 exit 1
fi

if [ -z "$DYNV6_TOKEN" ] ; then
 echo "DYNNV6.com account is required and DYNV6_TOKEN needs to be set with api-token"
 exit 1
fi


mod_public-ip(){
#download the public-ip script
get_files "public-ip.sh" "/root/.os1" "{{FILES_PATH}}/public-ip" "sudo" "yes"
#Now start the job
# To prevent adding sudo for all commands we do it once
sudo bash << END_SUDO
chmod +x /root/.os1/public-ip.sh
cat > /root/.os1/public-ip.service <<EOF
[Unit]
Description=Public IP Service
After=network.target

[Service]
Type=simple
User=root
Restart=on-failure
RestartSec=5s
ExecStart=/bin/bash /root/.os1/public-ip.sh
ExecReload=/bin/bash /root/.os1/public-ip.sh

[Install]
WantedBy=multi-user.target
EOF
ln -sf /root/.os1/public-ip.service /etc/systemd/system/public-ip.service
systemctl enable public-ip
systemctl restart public-ip
# To prevent adding sudo for all commands we do it once
END_SUDO
}

umod_public-ip(){
# To prevent adding sudo for all commands we do it once
sudo bash << END_SUDO
systemctl stop public-ip
systemctl disable public-ip
systemctl stop frpc
systemctl disable frpc
rm -rf /root/.os1/public-ip.service /root/.os1/public-ip.sh /etc/frp
systemctl daemon-reload
# To prevent adding sudo for all commands we do it once
END_SUDO
}
