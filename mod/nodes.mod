
mod_nodes(){
load_source "functions"
local list=$1
if get_setting "nodes" ;then
  list="${setting} ${list}"
fi
list=$(echo "$list" | sed 's/,/ /g')
for listitem in $list ; do
  if [ -n "$listitem" ] ;then
    microstack_add_node $listitem
  fi
done
update_dns
}

umod_nodes(){
load_source "functions"
local list=$1
if get_setting "nodes" ;then
  list="${setting} ${list}"
fi
list=$(echo "$list" | sed 's/,/ /g')
for listitem in $list ; do
  if [ -n "$listitem" ] ;then
    microstack_remove_node $listitem
  fi
done
update_dns
}

#We don't support upgrade
upgrade_nodes(){
return 0
}