# --- Software Functions -------------------------------------------
# All software install/uninstall actions will be performed on ctrl
# Software functions must start with mod_
# This script can be used to install and configure microstack

mod_microstack(){
# $1 - List with networks to install
# $2 - Default port microstack listens to
# $3 - The domain name
local networks=${1:-$NETWORKS}
local httpport=${2:-$OS_UI_PORT}
local domain=${3:-$DOMAIN}
local server_ip=$SERVER_IP
local luser=$(id -u -n)
load_source 'functions'

# To prevent adding sudo for all commands we do it once
sudo bash << END_SUDO
#------------------------------------------------------

snap install microstack --devmode --beta
#Microstack allows configuring and deployment of VM's
snap set  microstack config.network.ports.dashboard=$httpport
#Set password if set
if [ -n "$OS1_PWD" ]; then
  sudo snap set microstack config.credentials.keystone-password=$OS1_PWD
  echo "setting admin pwd"
fi

#Start the microstack
microstack init --auto --control
END_SUDO

#We need to exit after init because it will exit the sudo
sudo bash << END_SUDO
snap alias microstack.openstack openstack
sed -i '/[DEFAULT]/a resume_guests_state_on_host_boot = True' /var/snap/microstack/common/etc/nova/nova.conf.d/nova-snap.conf
#TODO Change the default neutron network
#sed -i '/[DEFAULT]/a ' /var/snap/microstack/common/etc/neutron/neutron.conf.d/neutron-snap.conf
#neutron-ovn-metadata-agent

#Replace the domain name
sed -i "/dns_domain =/c\dns_domain = $domain." /var/snap/microstack/common/etc/neutron/neutron.conf.d/neutron-snap.conf
systemctl restart snap.microstack.neutron-api

END_SUDO

#Check if we should enable or diable compute
if contains "$CONFIG_OPTIONS" 'local.compute.disabled' ;then
 sudo openstack compute service set --disable $HOSTNAME nova-compute
 sudo systemctl stop snap.microstack.nova-compute
 sudo systemctl disable snap.microstack.nova-compute
else
 sudo systemctl restart snap.microstack.nova-compute
fi



#We now can use the normal user

echo "Downloading and adding ubuntu cloud images"
rm -rf images
mkdir images
curl -sLn https://cloud-images.ubuntu.com/minimal/releases/focal/release/ubuntu-20.04-minimal-cloudimg-amd64.img  --output images/ubuntu-20.04-minimal-cloudimg-amd64.img
curl -sLn https://cloud-images.ubuntu.com/minimal/releases/bionic/release/ubuntu-18.04-minimal-cloudimg-amd64.img --output images/ubuntu-18.04-minimal-cloudimg-amd64.img

#Add the image to openstack admin project
openstack image create --file images/ubuntu-20.04-minimal-cloudimg-amd64.img --public --container-format=bare --disk-format=qcow2 ubuntu-20.04 >/dev/null
openstack image create --file images/ubuntu-18.04-minimal-cloudimg-amd64.img --public --container-format=bare --disk-format=qcow2 ubuntu-18.04 >/dev/null	

#We have updloaded the images so we can remove locals
rm -rf images

echo "Creating flavors"
#Create the flavors
openstack flavor create  --ram 1024 --disk 5 --swap 0 --vcpus 1 k8s.mini >/dev/null
openstack flavor create --ram 2048 --disk 10 --swap 254 --vcpus 2 k8s.small >/dev/null
openstack flavor create  --ram 4096 --disk 15 --swap 512  --vcpus 3 k8s.third >/dev/null
openstack flavor create  --ram 6114 --disk 30 --swap 1024  --vcpus 3 k8s.ctrl >/dev/null
openstack flavor create --ram 7168 --disk 20 --swap 1024 --vcpus 4 k8s.half >/dev/null
openstack flavor create --ram 15360 --disk 30 --swap 2048 --vcpus 8 k8s.full >/dev/null

echo "Creating security keys"
#Create key pair
ssh-keygen -q -N "" -f ~/.ssh/id_rsa 2>/dev/null <<< y >/dev/null
openstack keypair create --public-key ~/.ssh/id_rsa.pub $luser >/dev/null
#Create RSA key's by networks
for name in $networks ;do
 ssh-keygen -q -N "" -f ~/.ssh/${name}_rsa 2>/dev/null <<< y >/dev/null
 openstack keypair create --public-key ~/.ssh/${name}_rsa.pub $name >/dev/null
done


local os_project=""
if [ -n "$OS_PROJECT" ]; then
 echo "Creating project"
 os_project="--project $OS_PROJECT"
 openstack project create --domain default --description "$OS_PROJECT" $OS_PROJECT >/dev/null
 openstack role add --project $OS_PROJECT --user admin admin >/dev/null
fi

echo "Configuring networks"
#Create all networks internal network
#We disable port Security so we can use metalb
local subnet=100
for name in $networks ;do
  openstack network create $name --description $name $os_project >/dev/null
  openstack subnet create $name-subnet --network $name --subnet-range 192.168.$subnet.0/24 --allocation-pool start=192.168.$subnet.2,end=192.168.$subnet.199 --dns-nameserver $server_ip $os_project >/dev/null
  openstack router create  external-$name $os_project  >/dev/null
  openstack router set external-$name --external-gateway external   >/dev/null
  openstack router add subnet external-$name $name-subnet   >/dev/null
  subnet=$((subnet + 1 ))
done

openstack security group create ssh_group --description 'SSH-ports' $os_project >/dev/null
openstack security group rule create --proto icmp ssh_group $os_project >/dev/null
openstack security group rule create --proto tcp --dst-port 22 ssh_group $os_project >/dev/null
openstack security group create --description 'Kubernetes-ports' kubernetes $os_project >/dev/null
openstack security group rule create --proto tcp --dst-port 80 kubernetes $os_project >/dev/null
openstack security group rule create --proto tcp --dst-port 443 kubernetes $os_project >/dev/null
openstack security group rule create --proto tcp --dst-port 2376 kubernetes $os_project >/dev/null
openstack security group rule create --proto tcp --dst-port 6443 kubernetes $os_project >/dev/null
openstack security group rule create --proto tcp --dst-port 2379 kubernetes $os_project >/dev/null
openstack security group rule create --proto tcp --dst-port 2380 kubernetes $os_project >/dev/null
openstack security group rule create --proto udp --dst-port 8080 kubernetes $os_project >/dev/null
openstack security group rule create --proto udp --dst-port 8443 kubernetes $os_project >/dev/null
openstack security group rule create --proto udp --dst-port 8472 kubernetes $os_project >/dev/null
openstack security group rule create --proto tcp --dst-port 9080 kubernetes $os_project >/dev/null
openstack security group rule create --proto tcp --dst-port 9443 kubernetes $os_project >/dev/null
openstack security group rule create --proto tcp --dst-port 9099 kubernetes $os_project >/dev/null
openstack security group rule create --proto tcp --dst-port 10250 kubernetes $os_project >/dev/null
openstack security group rule create --proto tcp --dst-port 10254 kubernetes $os_project >/dev/null
openstack security group rule create --proto tcp --proto udp --dst-port 30000:32767 kubernetes $os_project >/dev/null


#echo "Removing test network"
openstack router remove subnet test-router test-subnet >/dev/null
openstack subnet delete test-subnet >/dev/null
openstack router delete test-router >/dev/null
openstack network delete test >/dev/null

if get_setting "nodes" ;then
  local nodes=$setting
  for node in $nodes ;do
    microstack_add_node $node
  done
fi


}

#uninstall microstack
umod_microstack(){
if get_setting "nodes" ;then
  load_source 'functions'
  local nodes=$setting
  for node in $nodes ;do
    microstack_remove_node $node
  done
fi
 # To prevent adding sudo for all commands we do it once
sudo bash << END_SUDO
echo "Removing microstack"
snap remove microstack
END_SUDO
}


upgrade_microstack(){
 sudo snap refresh microstack
}