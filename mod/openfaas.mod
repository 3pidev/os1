# Alternative is https://fission.io/docs/installation/

# --- Software Functions -------------------------------------------
# All software install/uninstall actions will be performed on first_master
# Software functions must start with k3s_

#Serverless Functions, Made Simple. OpenFaaS® makes it simple to deploy both functions and existing code to Kubernetes.
# Install the CLI: curl -sL https://cli.openfaas.com | sudo sh
# On ARM use arkade will pull the correct images
mod_openfaas() {
 local cluster=${1:-$CLUSTER}
 local openfaas_pwd=${2:-${FAAS_PWD}}
 if get_setting "openfaas_pwd" ;then
  openfaas_pwd=$setting
 fi
 if [ -z "$openfaas_pwd" ] ;then
  openfaas_pwd=$OS1_PWD
 fi

 local _TMP=/tmp/openfaas
 mkdir -p $_TMP
 local kubectl="$_TMP/kubectl"
 load_source "functions"
 if rancher_kubectl $cluster $kubectl ;then
  rancher_project_namespace "Tools" "openfaas"
  rancher_project_namespace "Tools" "openfaas-fn"
  export KUBECONFIG=$kubectl
  helm repo add openfaas https://openfaas.github.io/faas-netes/
  kubectl apply -f https://raw.githubusercontent.com/openfaas/faas-netes/master/namespaces.yml
  kubectl -n openfaas delete secret basic-auth
  kubectl -n openfaas create secret generic basic-auth --from-literal=basic-auth-user=admin --from-literal=basic-auth-password="$openfaas_pwd"
  helm upgrade \
    --install \
    openfaas \
    openfaas/openfaas \
    --namespace openfaas  \
    --set basic_auth=true \
    --set functionNamespace=openfaas-fn \
    --set serviceType=ClusterIP \
    --wait

   #Create a secure website for the service
cat <<EOC | kubectl -n openfaas apply  -f -
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: openfaas-gateway
spec:
  rules:
  - host: faas.$DOMAIN
    http:
      paths:
      - path:
        pathType: ImplementationSpecific
        backend:
          service:
            name: gateway
            port:
              number: 8080
EOC
 fi
 rm -rf $_TMP
}


umod_openfaas() {
 local cluster=${1:-$CLUSTER}
 local _TMP=/tmp/openfaas
 mkdir -p $_TMP
 local kubectl="$_TMP/kubectl"
 load_source "functions"
 if rancher_kubectl $cluster $kubectl ;then
   export KUBECONFIG=$kubectl
   helm uninstall openfaas -n openfaas
   kubectl -n openfaas delete openfaas-gateway
   kubectl -n openfaas delete secret basic-auth
   kubectl delete ns openfaas
   kubectl delete ns openfaas-fn
 fi
 cd ~
 rm -rf $_TMP
}