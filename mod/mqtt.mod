# https://medium.com/@tomer.klein/docker-compose-and-mosquitto-mqtt-simplifying-broker-deployment-7aaf469c07ee
# Add user
# docker exec mqtt mosquitto_passwd -b /etc/mosquitto/password <user> <password>
# Encrypt password file
# docker exec mqtt mosquitto_passwd -U /etc/mosquitto/password
mod_mqtt(){
sudo mkdir -p $config_dir/mqtt/mosquitto
local NEW=0

if [ ! -f $config_dir/mqtt/mosquitto.conf ]; then
sudo bash << END_SUDO
NEW="1"
cat > $config_dir/mqtt/mosquitto.conf <<EOF
listener 1883 0.0.0.0
listener 9001 0.0.0.0
protocol websockets
persistence true
persistence_location /mosquitto/data/
password_file /etc/mosquitto/password
log_type error
log_type warning
#log_type notice
#log_type information
#log_type debug
#log_type subscribe
#log_type unsubscribe
#log_type websockets
#log_type alllog_type all
#Control the output
#log_dest syslog
log_dest file /mosquitto/log/mosquitto.log
EOF
touch $config_dir/mqtt/mosquitto/password
END_SUDO
fi

docker run \
  -d \
  --name mqtt \
  --pull=always \
  -v $config_dir/mqtt/mosquitto.conf:/mosquitto/config/mosquitto.conf \
  -v $config_dir/mqtt/mosquitto:/etc/mosquitto \
  -v $config_dir/mqtt/mosquitto/data:/mosquitto/data \
  -v $config_dir/mqtt/mosquitto/log:/mosquitto/log \
  -p 1883:1883 \
  -p 9001:9001 \
  --restart unless-stopped \
  eclipse-mosquitto

if [ "$NEW" -eq "1" ]; then
docker exec mqtt mosquitto_passwd -b /etc/mosquitto/password admin $OS1_PWD
fi

  load_source "proxy"
  local_proxy "mqtt" "9001"


}

umod_mqtt(){
docker rm --force mqtt
}

upgrade_mqtt(){
    umod_mqtt
    mod_mqtt
}