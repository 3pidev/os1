mod_auto-update(){
  update_days=${AUTO_UPDATE_DAYS:-7}
  sudo bash << END_SUDO
  #Update system and install required packages
  apt-get update
  apt-get autoremove -y
  #Enable the usage of usb storage devices using extfat, git
  #TODO: We should change time stamp to be flexible base on IP
  apt-get install unattended-upgrades -y
  #
  ##Fix problem with name resolving
  #ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf
  #sed -i 's/#domain-name=local/domain-name=local/g' /etc/avahi/avahi-daemon.conf
  ##Allow running unattended updates
  sed -i 's/\/\/\t"\${distro_id}:\${distro_codename}-updates";/\t"\${distro_id}:\${distro_codename}-updates";/g' /etc/apt/apt.conf.d/50unattended-upgrades
  sed -i 's/\/\/Unattended-Upgrade::Remove-Unused-Kernel-Packages "false";/Unattended-Upgrade::Remove-Unused-Kernel-Packages "true";/g' /etc/apt/apt.conf.d/50unattended-upgrades
  sudo sed -i 's/\/\/Unattended-Upgrade::Remove-Unused-Dependencies "false";/Unattended-Upgrade::Remove-Unused-Dependencies "true";/g' /etc/apt/apt.conf.d/50unattended-upgrades
  sed -i 's/\/\/Unattended-Upgrade::Automatic-Reboot "false";/Unattended-Upgrade::Automatic-Reboot "true";/g' /etc/apt/apt.conf.d/50unattended-upgrades
  sed -i 's/\/\/Unattended-Upgrade::Automatic-Reboot-Time "02:00";/Unattended-Upgrade::Automatic-Reboot-Time "02:38";/g' /etc/apt/apt.conf.d/50unattended-upgrades
  cat > /etc/apt/apt.conf.d/20auto-upgrades <<EOF
APT::Periodic::Update-Package-Lists "$update_days";
APT::Periodic::Download-Upgradeable-Packages "$update_days";
APT::Periodic::AutocleanInterval "21";
APT::Periodic::Unattended-Upgrade "$update_days";
EOF
  timedatectl set-timezone $TIMEZONE
  unattended-upgrade -v
END_SUDO
}