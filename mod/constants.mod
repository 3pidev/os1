# -- Constants ---------------------------------------
# Configuration of global constants used 

#Project to create within OpenStack
OS_PROJECT=${OS_PROJECT:-os1}

#Set the openstack base image
OS_BASEIMAGE=${OS_BASEIMAGE:-ubuntu-22.04}

#With our configuration the openstack network MTU smaller
#This causes network issues when running docker within Openstack
if [ -n "$(snap aliases 2>/dev/null | grep openstack | awk {'print $1'})" ] ;then
 OS_MTU=1442
else
 OS_MTU=1500
fi

#OpenstackLogin
OS_USER=${OS_USER:-admin}
if get_setting "os_user" ;then
  OS_USER=$setting
fi

OS1_PWD=${OS1_PWD:-}
if get_setting "os_password" ;then
  OS1_PWD=$setting
  if [ "${OS1_PWD:0:1}" == "~" ] ;then
    epassword=$(echo "${OS1_PWD:1}" | base64 --decode 2>/dev/null)
    if [ "$?" == "0" ] ;then
     OS1_PWD=$epassword
    fi     
  fi
fi
if [ -z "$OS1_PWD" ] && [ -n "$(snap aliases 2>/dev/null | grep openstack | awk {'print $1'})" ] ;then
  OS1_PWD=$(sudo snap get microstack config.credentials.keystone-password)
fi

#The Openstack network
OS_NETWORK=${OS_NETWORK:-ens3}
if get_setting "os_network" ;then
  OS_NETWORK=$setting
fi  

#OpenStack dashboard port OS_UI_PORT
OS_UI_PORT=${OS_UI_PORT:-9081}
if get_setting "openstack_ui_port" ;then
  OS_UI_PORT=$setting
fi

#Default data directory
DATA_DIR=${DATA_DIR:-/data}
if get_setting "data_dir" ;then
  DATA_DIR=$setting
fi


#Domain name we will use
DOMAIN=${DOMAIN:-change.me}
if get_setting "domain" ;then
  DOMAIN=$setting
fi

#Email of admin for Certificates
OS_EMAIL=$OS_EMAIL:-${OS_USER}@${DOMAIN}}
if get_setting "email" ;then
  OS_EMAIL=$setting
fi

SSH_OPTIONS=${SSH_OPTIONS:-}
#Additional ssh options
if get_setting "ssh_options" ;then
  SSH_OPTIONS=$setting
fi

BASE_NET=${BASE_NET:-local}
if get_setting "base_network" ;then
  BASE_NET=$setting
fi

NETWORKS=${NETWORKS:-$BASE_NET dev tst acc prd}
#Additional ssh options
if get_setting "networks" ;then
  NETWORKS="$BASE_NET $setting"
fi

#TimeZone
TIMEZONE=${TIMEZONE:-Europe/Amsterdam}
if get_setting "timezone" ;then
  TIMEZONE=$setting
fi

#Extra config items
DEF_CONFIG_OPTIONS='local local.compute.disabled'
CONFIG_OPTIONS=${CONFIG_OPTIONS:-$DEF_CONFIG_OPTIONS}
if get_setting "config" ;then
  CONFIG_OPTIONS="$CONFIG_OPTIONS $setting"
fi
CONFIG_OPTIONS=$(echo $CONFIG_OPTIONS | sed 's/,/ /g')

#Default rancher settings
RANCHER_HOST=${RANCHER_HOST:-localhost}
RANCHER_PORT=${RANCHER_PORT:-443}

#We add some default software to the install list
include_list="sudoers,kernel,dns,os1,microstack,docker,openresty,keycloak,rancher,minio,proxy,auto-update,welcome"


