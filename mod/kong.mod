#Install kong API Gateway

mod_kong(){
 local cluster=${1:-$CLUSTER}
 local _TMP=/tmp/kong
 rm -rf $_TMP
 mkdir -p $_TMP
 local kubectl="$_TMP/kubectl"
 load_source "functions"
 if rancher_kubectl $cluster $kubectl ;then
  export KUBECONFIG=$kubectl
     #https://github.com/pantsel/konga
     #https://medium.com/faun/kong-api-gateway-with-konga-dashboard-ae95b6d1fec7
     #https://www.jerney.io/secure-apis-kong-keycloak-1/
   if rancher_project_namespace "API-Gateway" "kong" ;then
      helm repo add bitnami https://charts.bitnami.com/bitnami
      helm install kong bitnami/kong -n kong 
      #--set service.exposeAdmin=true
      local KONG_PWD=$(kubectl get secret kong-postgresql -n kong  -o jsonpath="{.data.postgresql-password}" | base64 --decode)
      cat <<EOC | kubectl -n kong apply  -f -
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: kong-proxy-gateway
spec:
  rules:
  - host: api.$DOMAIN
    http:
      paths:
      - path:
        pathType: ImplementationSpecific
        backend:
          service:
            name: kong
            port: http-proxy
EOC
      #install konga
      mkdir -p $_TMP/konga
      cd $_TMP/konga
      curl -Ls https://api.github.com/repos/pantsel/konga/tarball | tar xz --wildcards "*/charts/konga" --strip-components=3
      #get_files "values.yaml" "$_TMP/konga" "{{FILES_PATH}}/faas/konga" "" "yes"
      helm install konga -n kong $_TMP/konga
      cat <<EOC | kubectl -n kong apply  -f -
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: konga-gateway
spec:
  rules:
  - host: konga.$DOMAIN
    http:
      paths:
      - path:
        pathType: ImplementationSpecific
        backend:
          service:
            name: konga
            port: http
EOC

      #Within konga add kong: http://kong:8001
#echo '
#def foo(event, context):
#    return "Hello !"
#' > hello.py
      #kubeless function update hello --from-file hello.py --handler hello.foo --runtime python3.7
      #kubeless function update hello --from-file hello.py  --handler hello.foo  --runtime python3.7
      #kubeless trigger http create hello-ingress --function-name hello --gateway kong --hostname kong.os1.nl --path hello

      #Add keycloack
      #kubectl port-forward svc/kong 8001:8001 -n kong

     cd
   fi
 fi
 rm -rf $_TMP
}
 
umod_kong(){
 local cluster=${1:-$CLUSTER}
 local _TMP=/tmp/kong
 mkdir -p $_TMP
 local kubectl="$_TMP/kubectl"
 load_source "functions"
 if rancher_kubectl $cluster $kubectl ;then
   helm uninstall -n kong konga
   helm uninstall -n kong kong
   #TODO delete the storage claims
   kubectl delete -n kong
 fi
 rm -rf $_TMP
}