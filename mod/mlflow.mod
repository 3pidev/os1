#https://towardsdatascience.com/mlflow-part-2-deploying-a-tracking-server-to-minikube-a2d6671e6455

umod_mlflow(){
local mlflow_pwd="mlflow_pwd"

#Create Namespace
kubectl create ns mlflow

#Create database
cat <<EOF | kubectl apply -n mlflow -f -
apiVersion: v1
kind: ConfigMap
metadata:
  name: mlflow-postgres-config
  labels:
    app: mlflow-postgres
data:
  POSTGRES_DB: mlflow_db
  POSTGRES_USER: mlflow_user
  POSTGRES_PASSWORD: $mlflow_pwd
  PGDATA: /var/lib/postgresql/mlflow/data
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: mlflow-postgres
  labels:
    app: mlflow-postgres
spec:
  selector:
    matchLabels:
      app: mlflow-postgres
  serviceName: "mlflow-postgres-service"
  replicas: 1
  template:
    metadata:
      labels:
        app: mlflow-postgres
    spec:
      containers:
      - name: mlflow-postgres
        image: postgres:11
        ports:
        - containerPort: 5432
          protocol: TCP
        envFrom:
        - configMapRef:
            name: mlflow-postgres-config
        resources:
          requests:
            memory: "1Gi"
            cpu: "500m"
        volumeMounts:
        - name: mlflow-pvc
          mountPath: /var/lib/postgresql/mlflow
  volumeClaimTemplates:
  - metadata:
      name: mlflow-pvc
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 100Mi
---
apiVersion: v1
kind: Service
metadata:
  name: mlflow-postgres-service
  labels:
    svc: mlflow-postgres-service
spec:
  type: NodePort
  ports:
  - port: 5432
    targetPort: 5432
    protocol: TCP
  selector:
    app: mlflow-postgres
EOF


# Creating MLflow deployment
cat <<EOF | kubectl apply -n mlflow -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mlflow-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: mlflow-deployment
  template:
    metadata:
      labels:
        app: mlflow-deployment
    spec:
      containers:
      - name: mlflow-deployment
        image: johanpbj/mlflow-server:latest
        imagePullPolicy: Always
        args:
        - --host=0.0.0.0
        - --port=5000
        - --backend-store-uri=postgresql://mlflow_user:$mlflow_pwd@mlflow-postgres-service:5432/mlflow_db
        - --default-artifact-root=s3://mlflow/
        - --workers=2
        env:
        - name: MLFLOW_S3_ENDPOINT_URL
          value: https://s3.minio.$DOMAIN/
        - name: AWS_ACCESS_KEY_ID
          value: "$OS_USER"
        - name: AWS_SECRET_ACCESS_KEY
          value: "$OS1_PWD"
        ports:
        - name: http
          containerPort: 5000
          protocol: TCP
---
apiVersion: v1
kind: Service
metadata:
  name: mlflow-service
spec:
  type: NodePort
  ports:
    - port: 5000
      targetPort: 5000
      protocol: TCP
      name: http
  selector:
    app: mlflow-deployment
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: mlflow-ingress
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.il/add-base-url: "true"
spec:
  rules:
  - host: mlflow.$DOMAIN
    http:
      paths:
        - backend:
            serviceName: mlflow-service
            servicePort: 5000
          path: /
EOF
}