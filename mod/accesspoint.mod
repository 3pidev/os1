

#Using: https://askubuntu.com/questions/180733/how-to-create-a-wi-fi-hotspot-in-access-point-mode
#TODO build SID

sudo bash << END_SUDO
apt-get install iw hostapd isc-dhcp-server -y
cat > /etc/hostapd/hostapd.conf<<EOF
interface=wlp2s0
driver=nl80211
ssid=n1.os1.nl
hw_mode=g
channel=1
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=3
wpa_passphrase=09a09a1969
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
EOF

sed -i '/DAEMON_CONF=/c\DAEMON_CONF="/etc/hostapd/hostapd.conf"' /etc/default/hostapd
systemctl unmask hostapd
systemctl enable hostapd

sed -i '/INTERFACESv4=/c\INTERFACESv4="wlp2s0"' /etc/default/isc-dhcp-server

if [ ! -f /etc/dhcp/dhcpd.conf.bak ] ;then
 cp /etc/dhcp/dhcpd.conf /etc/dhcp/dhcpd.conf.bak
fi
cat > /etc/dhcp/dhcpd.conf<<EOF
ddns-update-style none;
#We use the Google en Cisco Open DNS server
subnet 10.10.0.0 netmask 255.255.255.0 {
        range 10.10.0.2 10.10.0.16;
        option domain-name-servers 8.8.4.4, 208.67.222.222;
        option routers 10.10.0.1;
}
EOF

#TODO: Add bridge
cat > /etc/netplan/01-wlp2s0.yaml <<EOF
network:
    version: 2
    renderer: networkd
    ethernets:
        wlp2s0:
            dhcp4: no
            dhcp6: no
            addresses: [10.10.0.1/24]
EOF
netplan apply

service isc-dhcp-server restart
service hostapd restart
iptables -t nat -A POSTROUTING -s 10.10.0.0/16 -o eno0 -j MASQUERADE
END_SUDO

iptables -t nat -D POSTROUTING -s 10.10.0.0/16 -o eno0 -j MASQUERADE


mod_accesspoint(){
apt-get install hostapd bridge-utils -y
#Change to replace line
sed -i 's/#DAEMON_CONF=""/DAEMON_CONF="/etc/hostapd/hostapd.conf"/g" /etc/default/hostapd  
cat > /etc/hostapd/hostapd.conf <<EOF
### Wireless network name ###
interface=wlan0
 
### Set your bridge name ###
bridge=wlan-br0

driver=nl80211
country_code=NL
hw_mode=g
channel=6
wpa=2
wpa_passphrase=MyWiFiPassword

## Key management algorithms ##
wpa_key_mgmt=WPA-PSK
 
## Set cipher suites (encryption algorithms) ##
## TKIP = Temporal Key Integrity Protocol
## CCMP = AES in Counter mode with CBC-MAC
wpa_pairwise=TKIP
rsn_pairwise=CCMP
 
## Shared Key Authentication ##
auth_algs=1
 
## Accept all MAC address ###
macaddr_acl=0

EOF

}

umod_accesspoint(){

}

#We don't support upgrade
upgrade_accesspoint(){
return 0
}