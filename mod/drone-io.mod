# --- Software Functions -------------------------------------------
# All software install/uninstall actions will be performed on first_master
# Software functions must start with k3s_

#The Drone.io CI/CD environment
#We wanted to use alternativee for Jenkis that is more light weight and decided for Drone.io

mod_drone-io() {
 local cluster=${1:-$CLUSTER}
 local ci_type=${2:-$CICD_TYPE}
 if get_setting "cicd_type" ;then
  ci_type=$setting
 fi
 local ci_client=${3:-$CICD_CLIENT}
 if  get_setting "cicd_client" ;then
  ci_client=$setting
 fi
 local ci_secret=${4:-$CICD_SECRET}
 if get_setting "cicd_secret" ;then
  ci_secret=$setting
 fi
 local ci_docker_usr=${5:-$DOCKER_USER}
 if get_setting "docker_user" ;then
  ci_docker_usr=$setting
 fi
 local ci_docker_pwd=${6:-$DOCKER_PASSWORD}
 if get_setting "docker_password" ;then
  ci_docker_pwd=$setting
 fi
 if [ "${ci_docker_pwd:0:1}" == "~" ] ;then
  local epassword=$(echo "${ci_docker_pwd:1}" | base64 --decode 2>/dev/null)
  if [ "$?" == "0" ] ;then
    ci_docker_pwd=$epassword
  fi
 fi
 local ci_name=${7:-${CICD_NAME:-drone}}
 if get_setting "cicd_name" ;then
  ci_name=$setting
 fi
 
 #Default mtu is auto detect
 local mtu=${OS_MTU:-0}

 load_source "functions"
 local _TMP=/tmp/cicd
 rm -rf $_TMP
 mkdir -p $_TMP
 local kubectl="$_TMP/kubectl"
 
 if [ -n "$ci_type" ] && [ -n "$ci_client" ] && [ -n "$ci_secret" ] && rancher_kubectl $cluster $kubectl ;then
   export KUBECONFIG=$kubectl
   local secret=$(openssl rand -hex 16)
   for yml in drone-values drone-secrets-values drone-runner-values ;do
     get_files "$yml.yaml" "$_TMP" "{{FILES_PATH}}/cicd"
   done
   
  cat << EOC >> $_TMP/drone-values.yaml
  DRONE_SERVER_HOST: ${ci_name}.${cluster}.${DOMAIN}
  DRONE_SERVER_PROTO: https
  DRONE_RPC_SECRET: $secret
  DRONE_DATABASE_SECRET: $secret
  DRONE_${ci_type}_CLIENT_ID: $ci_client 
  DRONE_${ci_type}_CLIENT_SECRET: $ci_secret
EOC
  cat << EOC >> $_TMP/drone-runner-values.yaml
  ## Fix for running Drone within openstack
  DRONE_RUNNER_ENVIRON: "PLUGIN_MTU:$mtu"
  DRONE_RPC_HOST: $ci_name
  DRONE_RPC_SECRET: $secret
  DRONE_SECRET_PLUGIN_TOKEN: $secret
  DRONE_SECRET_PLUGIN_ENDPOINT: http://${ci_name}-secrets-drone-kubernetes-secrets:3000
EOC
  cat << EOC >> $_TMP/drone-secrets-values.yaml
  SECRET_KEY: $secret
EOC
 helm repo add drone https://charts.drone.io
 helm repo update 
 rancher_project_namespace "Tools" "cicd"
 rancher_project_namespace "Tools" "cicd-build"
 helm upgrade -i $ci_name drone/drone --namespace cicd --values $_TMP/drone-values.yaml --wait
 
 #Create a secure website for the service
cat <<EOC | kubectl -n cicd apply  -f -
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: $ci_name-gateway
spec:
  rules:
  - host: $ci_name.$DOMAIN
    http:
      paths:
      - path:
        pathType: ImplementationSpecific
        backend:
          service:
            name: $ci_name
            port:
              number: 80
EOC

 ### Install drone runners
 helm upgrade -i $ci_name-runner drone/drone-runner-kube --namespace cicd --values $_TMP/drone-runner-values.yaml
 helm upgrade -i $ci_name-secrets drone/drone-kubernetes-secrets --namespace cicd --values $_TMP/drone-secrets-values.yaml
 
 #Add the docker secret
 if [ -n "$ci_docker_usr" ] && [ -n "$ci_docker_pwd" ] ;then
 cat << EOC | kubectl -n cicd-build apply -f -
apiVersion: v1
kind: Secret
type: Opaque
metadata:
  name: $ci_name-docker
  annotations:
    X-Drone-Events: push,tag,promote
data:
  username: $(echo -n "${ci_docker_usr}" | base64 --wrap=0)
  password: $(echo -n "${ci_docker_pwd}" | base64 --wrap=0)
EOC
fi

local TOKEN=$(cat $kubectl | yq -P e ".users[].user.token"  - )
local CERT=$(cat $kubectl | yq -P e ".clusters[].cluster.certificate-authority-data"  - | base64 --decode)
local usr=$(echo $TOKEN | cut -f1 -d:)
local pwd=$(echo $TOKEN | cut -f2 -d:)
#TODO: Create tokens https://uala.io/how-to-create-ci-cd-pipeline-with-drone-io/
local url=$(cat $kubectl | yq -P e ".clusters[].cluster.server"  - )

cat << EOC | kubectl -n cicd-build apply -f -
apiVersion: v1
kind: Secret
type: Opaque
metadata:
  name: $ci_name-kubectl
  annotations:
    X-Drone-Events: push,tag,promote
data:
  cert: $(echo -n $CERT | base64 --wrap=0)
  token: $(echo -n $TOKEN | base64 --wrap=0)
  url: $(echo -n "$url" | base64 --wrap=0)
  usr: $(echo -n "$usr" | base64 --wrap=0)
  pwd: $(echo -n "$pwd" | base64 --wrap=0)
EOC
 else 
  echo "Not all required parameters set: cicd_type cicd_client cicd_secret"
 fi
 
 #Remove temp files
 rm -rf $_TMP
}

umod_drone-io() {
 local cluster=${1:-$CLUSTER}
 local ci_name=${2:-${CICD_NAME:-drone}}
 if get_setting "cicd_name" ;then
  ci_name=$setting
 fi
 local _TMP=/tmp/cicd
 mkdir -p $_TMP
 local kubectl="$_TMP/kubectl"
 load_source "functions"
 if rancher_kubectl $cluster $kubectl ;then
   export KUBECONFIG=$kubectl
   helm delete $ci_name --namespace cicd 
   helm delete $ci_name-runner  --namespace cicd 
   helm delete $ci_name-secrets --namespace cicd 
   kubectl -n cicd delete ingress $ci_name-gateway
   kubectl -n cicd-build delete secret $ci_name-kubectl
   kubectl -n cicd-build delete secret $ci_name-docker
   #For now we leave the name spaces
   #kubectl delete ns cicd
   #kubectl delete ns cicd-build
 fi
 rm -rf $_TMP
}

upgrade_drone-io(){
  #Just run the installer
  mod_drone-io $1 $2 $3 $4 $5 $6 $7
}

