
k3s_configure(){
    if [ -f "/usr/bin/kubectx" ]; then
     kubectx default
    fi


    helm repo add rancher-latest https://releases.rancher.com/server-charts/latest
    kubectl create namespace cattle-system
    kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.5.1/cert-manager.crds.yaml
    helm repo add jetstack https://charts.jetstack.io
    helm repo update
    helm install cert-manager jetstack/cert-manager --namespace cert-manager  --create-namespace --version v1.5.1
    helm install rancher rancher-latest/rancher --namespace cattle-system --set hostname=rancher.${DOMAIN} --set bootstrapPassword=${OS1_PWD}
    
    if [ "$K3S_AUTOUPGRADE" == "true" ]; then
      k3s_configure_upgrade
    fi
}

k3s_configure_upgrade(){
       #Auto upgrade
    kubectl apply -f https://github.com/rancher/system-upgrade-controller/releases/latest/download/system-upgrade-controller.yaml
cat <<EOC | kubectl apply  -f -
# Server plan
apiVersion: upgrade.cattle.io/v1
kind: Plan
metadata:
  name: server-plan
  namespace: system-upgrade
spec:
  concurrency: 1
  cordon: true
  nodeSelector:
    matchExpressions:
    - key: node-role.kubernetes.io/master
      operator: In
      values:
      - "true"
  serviceAccountName: system-upgrade
  upgrade:
    image: rancher/k3s-upgrade
  channel: https://update.k3s.io/v1-release/channels/stable
---
# Agent plan
apiVersion: upgrade.cattle.io/v1
kind: Plan
metadata:
  name: agent-plan
  namespace: system-upgrade
spec:
  concurrency: 1
  cordon: true
  nodeSelector:
    matchExpressions:
    - key: node-role.kubernetes.io/master
      operator: DoesNotExist
  prepare:
    args:
    - prepare
    - server-plan
    image: rancher/k3s-upgrade
  serviceAccountName: system-upgrade
  upgrade:
    image: rancher/k3s-upgrade
  channel: https://update.k3s.io/v1-release/channels/stable
EOC
}

mod_k3s-rancher-docker(){
local phttp="3080"
local phttps="8443"
    curl -s https://raw.githubusercontent.com/k3d-io/k3d/main/install.sh | bash
    k3d cluster create rancher -p "${phttp}:80@loadbalancer"   -p "${phttps}:443@loadbalancer" --agents 0
    cp -f .kube/config .kube/config-default
   
    # Configure k3s 
    k3s_configure
}

mod_k3s-rancher(){
    mkdir -p .kube
    curl -sfL https://get.k3s.io | sh -
    sudo cp -f /etc/rancher/k3s/k3s.yaml .kube/config-default
    sudo chown $user.$user .kube/config-default

    # Configure k3s
    k3s_configure
}

umod_k3s-rancher(){
    if [ -f "/usr/local/bin/k3s-agent-uninstall.sh" ] ; then
     /usr/local/bin/k3s-agent-uninstall.sh
    fi
    if [ -f "/usr/local/bin/k3s-uninstall.sh" ] ; then
     /usr/local/bin/k3s-uninstall.sh
    fi
}

umod_k3s_rancher-docker(){
    sudo rm -rf /usr/local/bin/k3d
}