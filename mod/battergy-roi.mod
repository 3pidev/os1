mod_battergy-roi(){
local installdir=$config_dir/battergy-roi
sudo mkdir -p $installdir
docker run \
  -d \
  --name battergy-roi \
  --pull=always \
  -v $installdir:/app/static/config \
  -p 5115:5000 \
  --restart unless-stopped \
  3pidev/battergy-roi:latest

  load_source "proxy"
  local_proxy "roi" "5115" "http" "battergy.app"
}

umod_battergy-roi(){
docker rm --force battergy-roi
}

upgrade_battergy-roi(){
    umod_battergy-roi
    mod_battergy-roi
}