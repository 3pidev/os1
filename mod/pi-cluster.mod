#Script will install K3S on 3 hosts
PI_cluster="3pidev"
PI_masterIp="10.0.3.10"
PI_clientIps="10.0.3.11 10.0.3.12"

mod_pi-cluster(){
 if get_setting "pi_cluster" ;then
   PI_cluster=$setting
 fi
 if get_setting "pi_master" ;then
   PI_masterIp=$setting
 fi
 if get_setting "pi_clients" ;then
   PI_clientIps=$setting
 fi
 load_source "functions"
 create_k3s_cluster "$PI_cluster" "$PI_masterIp" "$PI_clientIps" "ubuntu"
}

umod_pi-cluster(){
 if get_setting "pi_cluster" ;then
   PI_cluster="$setting"
 fi
 if get_setting "pi_master" ;then
   PI_masterIp="$setting"
 fi
 if get_setting "pi_clients" ;then
   PI_clientIps="$setting"
 fi
 load_source "functions"
 destroy_k3s_cluster "$PI_cluster" "$PI_masterIp" "$PI_clientIps" "ubuntu"
}


