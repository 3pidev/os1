mod_postgres(){
sudo mkdir -p $DATA_DIR/postgres
docker run \
  -d \
  --name postgres \
  --pull=always \
  -v $DATA_DIR/postgres:/var/lib/postgresql/data \
  -e POSTGRES_PASSWORD=${POSTGRES_PWD:-${OS1_PWD}} \
  -e POSTGRES_USER=postgres \
  -e POSTGRES_DB=postgres \
  -p 5432:5432 \
  --restart unless-stopped \
  postgres:latest
}

umod_postgres(){
docker rm --force postgres
}

upgrade_postgres(){
    umod_postgres
    mod_postgres
}