
mod_webvirtcloud(){

if [ -z "$SECRET_KEY" ]; then
  SECRET_KEY=$(echo -n "$USER@$HOST-$SERVER_IP" | md256sum | cut -d ' ' -f 1)
fi

git clone https://github.com/retspen/webvirtcloud
cd webvirtcloud
get_files "settings.py" "webvirtcloud" "{{FILES_PATH}}/webvirtcloud" "" "yes"
#Path the admin password
sed -i "s/admin = User.objects.create_superuser('admin', None, 'admin')/admin = User.objects.create_superuser('admin', None, '$OS1_PWD')/g" accounts/apps.py
#Build the docker
docker build -t local/webvirtcloud:latest .
cd ..
#remove the build directroy
rm -rf webvirtcloud
#run the docker
docker run -d -p 6081:80 -p 6080:6080  \
  --restart=unless-stopped\
  --name webvirtcloud \
  local/webvirtcloud
#Download the remote proxy
get_files "30-webvirtcloud.conf" "/usr/local/openresty/nginx/conf/rproxy/http/available" "{{FILES_PATH}}/webvirtcloud" "sudo" "yes"
#Recreate the links and restart openresty
sudo ln -sf /usr/local/openresty/nginx/conf/rproxy/http/available/*.conf /usr/local/openresty/nginx/conf/rproxy/http/enabled
sudo service openresty restart

docker exec -it webvirtcloud bash <<EOF
chown -R www-data.www-data /var/www
su -s /bin/bash www-data
ssh-keygen -t rsa -b 2048
cat /var/www/.ssh/id_rsa.pub
ssh-copy-id os1@os1.nl
EOF

#sudo DEBIAN_FRONTEND=noninteractive apt-get install -yq qemu qemu-kvm libvirt-daemon bridge-utils virt-manager virtinst

}

umod_kvm(){
sudo bash << END_SUDO
 DEBIAN_FRONTEND=noninteractive apt-get install -yq qemu qemu-kvm libvirt-clients libvirt-daemon-system bridge-utils virt-manager sasl2-bin python3-guestfs supervisor
 sed -i 's/libvirtd_opts="-d"/libvirtd_opts="-d -l"/g' /etc/default/libvirtd
 sed -i 's/#listen_tls/listen_tls/g' /etc/libvirt/libvirtd.conf
 sed -i 's/#listen_tcp/listen_tcp/g' /etc/libvirt/libvirtd.conf
 sed -i 's/#auth_tcp/auth_tcp/g' /etc/libvirt/libvirtd.conf
 sed -i 's/#[ ]*vnc_listen.*/vnc_listen = "0.0.0.0"/g' /etc/libvirt/qemu.conf
 sed -i 's/#[ ]*spice_listen.*/spice_listen = "0.0.0.0"/g' /etc/libvirt/qemu.conf
 wget -O /usr/local/bin/gstfsd https://raw.githubusercontent.com/retspen/webvirtcloud/master/conf/daemon/gstfsd
 chmod +x /usr/local/bin/gstfsd
 wget -O /etc/supervisor/conf.d/gstfsd.conf https://raw.githubusercontent.com/retspen/webvirtcloud/master/conf/supervisor/gstfsd.conf
 sudo systemctl enable libvirtd
 sudo systemctl start libvirtd
 service supervisor stop > /dev/null 2>&1
 service supervisor start
END_SUDO
}

umod_webvirtcloud(){
 docker stop webvirtcloud
 docker rm webvirtcloud
 #Remove the image
 docker rmi local/webvirtcloud
}