mod_tagui(){
docker run \
  -d \
  --name tagui \
  --restart unless-stopped \
  -p 19282:1880 \
  -p 5859:5859 \
  openiap/nodered-tagui:latest
  load_source "proxy"
  local_proxy "tagui" "19282"
}

umod_tagui(){
    docker rm --force tagui
}