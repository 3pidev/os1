# --- Software Functions -------------------------------------------
# All software install/uninstall actions will be performed on ctrl
# Software functions must start with mod_
# This script will reboot the system

mod_reboot(){
local list=$1
if [ -z "$1" ];then
  sudo reboot
else
list=$(echo "$list" | sed 's/,/ /g')
for ip in $list ; do
  if [ -n "$ip" ] ;then
  echo "Rebooting $ip"
${SSH}$ip bash << EOF
sudo reboot
EOF
  fi
done
fi
}

#We don't support upgrade
upgrade_reboot(){
return 0
}