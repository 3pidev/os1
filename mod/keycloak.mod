# --- Software Functions -------------------------------------------
# All software install/uninstall actions will be performed on ctrl
# Software functions must start with mod_
# This script can be used to install and configure keycloak user and accesmanagement

#https://www.keycloak.org/getting-started/getting-started-kube
#https://docs.syseleven.de/metakube/de/tutorials/setup-ingress-auth-to-use-keycloak-oauth
#https://www.openshift.com/blog/adding-authentication-to-your-kubernetes-web-applications-with-keycloak

#Setup follow instructions
#https://rancher.com/docs/rancher/v2.x/en/admin-settings/authentication/keycloak/
#openssl req -x509 -sha256 -nodes -days 3650 -newkey rsa:2048 -keyout sso.key -out sso.cert
#https://docs.openstack.org/vitrage/rocky/contributor/keycloak-config.html
#https://sso.os1.nl/auth/realms/master/protocol/saml/descriptor
#https://number1.co.za/using-keycloak-identity-provider-for-rancher-sso/
#https://gist.github.com/PhilipSchmid/506b33cd74ddef4064d30fba50635c5b
#http://www.mastertheboss.com/jboss-frameworks/keycloak/keycloak-with-docker
#Don't forgt to send admin username, email and surname

#Configure KeyCloak https://github.com/minio/minio/blob/master/docs/sts/keycloak.md

#TODO Create export of OS1 of keycloak and convert them to template
#Import template during creation of KeyCloak
#Create the sso key automaticly
#Add script to install keycloack into rancher
#Add users to keycloak and roles
#Add keycloak user scripts and roles
#Add all roles to admin user so that they are available in rancher

mod_keycloak (){
local datadir="${1:-$DATA_DIR}"
local httpport="${2:-8180}"
local httpsport="${3:-8143}"
local domain=${4:-$DOMAIN}
local realm=${5:-${REALM:-${domain}}}
if get_setting "realm" ;then
  realm=$setting
fi

local adminpwd=$OS1_PWD
local server_ip=$SERVER_IP

# To prevent adding sudo for all commands we do it once
get_files "docker-compose.yml" "$config_dir/keycloak" "{{FILES_PATH}}/keycloak" "sudo" "yes"
get_files "realm-os1.json" "$data_dir/keycloak/imports" "{{FILES_PATH}}/keycloak" "sudo" "yes"
# TODO: Copy the login theme
sudo bash << END_SUDO
#sed -i '/# Begin KeyCloak/,/# End KeyCloak/d' /etc/hosts
#echo -e "# Begin KeyCloak\n$server_ip sso.$domain\n# End KeyCloak" | sudo tee -a /etc/hosts >/dev/null
if [ "$ARCH" != "arm"  ] &&  [ "$ARCH" != "arm64"  ];then
keycloak_img="jboss/keycloak"
#Get the compose file 
docker-compose -f "$config_dir/keycloak/docker-compose.yml"  up -d

else
keycloak_img="3pidev/keycloak:latest"
if [[ "\$(docker images -q \$keycloak_img 2> /dev/null)" == "" ]]; then
git clone https://github.com/keycloak/keycloak-containers.git
cd keycloak-containers/server
#git checkout latest
docker build -t \$keycloak_img  .
cd 
rm -rf keycloak-containers
#docker build -t \$keycloak_img --build-arg GIT_REPO=keycloak/keycloak --build-arg GIT_BRANCH=master .
fi
#Start Docker installation of keycloak
  docker run -d \
    --name keycloak\
    -p $httpport:8080\
    -p $httpsport:8443\
    --restart=unless-stopped\
    -e KEYCLOAK_USER=admin \
    -e KEYCLOAK_PASSWORD=$adminpwd \
    -e PROXY_ADDRESS_FORWARDING=true\
    -e DB_VENDOR=h2\
    -v $data_dir/keycloak/data:/opt/keycloak/data \
    -v $data_dir/keycloak/conf:/opt/keycloak/conf \
    \$keycloak_img  
fi
END_SUDO

#Wait till keycloak is running ,before updating
printf 'Waiting for keycloak '
until $(curl --output /dev/null --silent --head --fail http://localhost:$httpport/auth); do
    printf '.'
    sleep 5
done
}

backup_keycloak(){
 local data_dir="${1:-$DATA_DIR}"
 local realm=${2:-${REALM:-${DOMAIN}}}
 local backup_loc=$data_dir/backup
 local export_mode=${3:-SAME_FILE}
 sudo mkdir -p $backup_loc
 get_files "keycloak-export.sh" "$data_dir/keycloak/conf" "{{FILES_PATH}}/keycloak" "" "yes"
 chmod +x $data_dir/keycloak/conf/keycloak-export.sh
 # Execute the script inside of the container
 sudo docker exec -it keycloak /opt/jboss/keycloak/conf/keycloak-export.sh
 #Copy the base config
 sudo cp $data_dir/keycloak/conf/export/${realm}-realm.json $backup_loc
 if [ "$export_mode" != "SKIP" ]; then
  sudo cp $data_dir/keycloak/conf/export/${realm}-users*.json $backup_loc
 fi
}

restore_keycloak(){
 local data_dir="${1:-$DATA_DIR}"
 local realm=${2:-${REALM:-${DOMAIN}}}
 local backup_loc=$data_dir/backup
 local export_mode=${3:-SAME_FILE}
 sudo mkdir -p $backup_loc
 get_files "keycloak-import.sh" "$data_dir/keycloak/conf" "{{FILES_PATH}}/keycloak" "" "yes"
 chmod +x $data_dir/keycloak/conf/keycloak-import.sh
 # Execute the script inside of the container
 sudo docker exec -it keycloak /opt/jboss/keycloak/conf/keycloak-import.sh
}

umod_keycloak(){
local datadir="${1:-$DATA_DIR}"
local realm=${2:-${REALM:-${DOMAIN}}}
local adminpwd=$OS1_PWD
if get_setting "realm" ;then
  realm=$setting
fi
#On uninstall we only backup
#backup_keycloak "$datadir" "$realm" "SKIP"
docker-compose down -f "$config_dir/keycloak/docker-compose.yml" 
docker-compose rm -f "$config_dir/keycloak/docker-compose.yml" 
}

upgrade_keycloak(){
 get_files "docker-compose.yml" $config_dir/keycloak "{{FILES_PATH}}/keycloak" "sudo" "yes"
 cd $config_dir/keycloak
 docker-compose pull
 docker-compose down
 docker-compose up -d
 cd
}