# --- Software Functions -------------------------------------------
# All software install/uninstall actions will be performed on ctrl
# Software functions must start with mod_
# Add rancher to the installed openstack

mod_rancher (){
local domain="${1:-$DOMAIN}"
local adminpwd=$OS1_PWD
local rancher_version=${2:-${RANCHER_VERSION:-latest}}

#Load the support functions
load_source "functions"

local phttp="80"
local phttps="443"
local inet=$OS_NETWORK
local mtu
if contains "$CONFIG_OPTIONS" 'local' ;then
 phttp="4080"
 phttps="8443"
 inet='enp1s0'
else
 mtu=${OS_MTU:-\$(ip -c a | grep $inet | awk '{print \$4}')}
fi

cat > rancher.sh << EOF
#!/bin/bash
mtu=$mtu
if [ -n "\$mtu" ] ;then
 cp /lib/systemd/system/docker.service /etc/systemd/system/docker.service
 sed -i "/ExecStart/ s/\$/ --mtu \$mtu/g" /etc/systemd/system/docker.service
 systemctl daemon-reload
 systemctl start docker		
 systemctl enable docker
fi
	

docker run -d --restart=unless-stopped -p $phttp:80 -p $phttps:443 -v /opt/rancher:/var/lib/rancher --name rancher --privileged  rancher/rancher:${rancher_version}
echo "Waiting for Rancher to be ready"
while ! curl -k -s https://localhost:${phttps}/ping >/dev/null; do sleep 3; done
echo "Rancher is ready, changing passwords"

#admin_pwd=admin
admin_pwd=\$(docker exec -ti rancher reset-password | tail -n1)
admin_pwd="\${admin_pwd//[$'\t\r\n ']}"
#echo "Temp admin:\${admin_pwd}"
# Login
LOGINRESPONSE=\$(curl -s "https://127.0.0.1:${phttps}/v3-public/localProviders/local?action=login" -H 'content-type: application/json' --data-binary '{"username":"admin","password":"'\${admin_pwd}'"}' --insecure)
LOGINTOKEN=\$(echo \$LOGINRESPONSE | jq -e -r .token)

# Change password
curl -s "https://127.0.0.1:${phttps}/v3/users?action=changepassword" -H 'content-type: application/json' -H "Authorization: Bearer \$LOGINTOKEN" --data-binary '{"currentPassword":"'\${admin_pwd}'","newPassword":"$adminpwd"}' --insecure

# Set server-url,ui-pl and domain
RANCHER_SERVER=https://rancher.$domain
curl -s "https://127.0.0.1:${phttps}/v3/settings/server-url" -H 'content-type: application/json' -H "Authorization: Bearer \$LOGINTOKEN" -X PUT --data-binary '{"name":"server-url","value":"'\$RANCHER_SERVER'"}' --insecure > /dev/null

curl -s "https://127.0.0.1:${phttps}/v3/settings/ui-pl" -H 'content-type: application/json' -H "Authorization: Bearer \$LOGINTOKEN" -X PUT --data-binary '{"name":"ui-pl","value":"OpenStackOne"}' --insecure > /dev/null

curl  -s "https://127.0.0.1:${phttps}/v3/settings/ingress-ip-domain" -H 'content-type: application/json' -H "Authorization: Bearer \$LOGINTOKEN" -X PUT --data-binary "{\"name\":\"ingress-ip-domain\",\"value\":\"$domain\"}" --insecure > /dev/null

curl -s "https://127.0.0.1:${phttps}/v3/settings/telemetry-opt" -H 'content-type: application/json' -H "Authorization: Bearer \$LOGINTOKEN" -X PUT --data-binary "{\"name\":\"telemetry-opt\",\"value\":\"out\"}" --insecure > /dev/null


#Logout
curl -sX POST "https://127.0.0.1:${phttps}/v3/tokens?action=logout" -H 'content-type: application/json' -H "Authorization: Bearer \$LOGINTOKEN"  --insecure
EOF


if contains "$CONFIG_OPTIONS" 'local' ;then
 sudo bash rancher.sh
 sudo sed -i '/# Begin LocalRancher/,/# End LocalRancher/d' /etc/hosts
 echo -e "# Begin LocalRancher\n127.0.0.1 rancher rancher.external\n# End LocalRancher" | sudo tee -a /etc/hosts >/dev/null
else
 openstack_create 'rancher' "$BASE_NET" 'ctrl.n0' 'rancher.sh'  >/dev/null
 #Update the dns
 update_dns $domain
fi

#Remove the rancher install script
rm -rf rancher.sh

ranchercli=2.4.13
curl -sLf https://github.com/rancher/cli/releases/download/v${ranchercli}/rancher-linux-amd64-v${ranchercli}.tar.gz | tar --strip-components=1 -zx
sudo mv rancher-v${ranchercli}/rancher /usr/bin/rancher
rm -rf rancher-v${ranchercli}
echo "ranchercli $ranchercli installed"
#------------------------------------------------------
}

#Uninstall the rancher by deleting the server
umod_rancher (){
local domain="${1:-$DOMAIN}"
local os_project=""
if [ -n "$OS_PROJECT" ]; then
  os_project="--os-project-name $OS_PROJECT"
fi
#Load the support functions
load_source "functions"
if contains "$CONFIG_OPTIONS" 'local' ;then

sudo docker rm -f rancher
sudo bash << END_SUDO
 sed -i '/# Begin LocalRancher/,/# End LocalRancher/d' /etc/hosts
 rm -rf /etc/kubernetes/ && rm -rf /opt/rancher
END_SUDO
else
 openstack server delete rancher $os_project
 update_dns $domain
fi 

sudo bash << END_SUDO

#Remove Ranchercli
rm -f /usr/bin/rancher
# To prevent adding sudo for all commands we do it once
END_SUDO
}

upgrade_rancher(){
echo "TODO: Create upgrade script rancher"
}
