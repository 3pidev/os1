#Software module to create a specific cluster


mod_yan-cluster(){
 load_source "functions"
 CLUSTER="yan"
 #Create yan cluster on dev network
 rancher_create_cluster $CLUSTER 'dev' 'ctrl.node0' 'half.node1'
 #Wait for cluster ready
 if rancher_cluster_ready ;then
   #Install local kube_config
   rancher_kubectl "${CLUSTER}" "" "no" "$DOMAIN" "yes"
   echo "Adding users"
   rancher_add_user "Peter" "Peter Venema" "Venema"
   rancher_add_user "Yanick" "Yanick Mampaey" "Mampaey"
   rancher_add_user "Sierk" "Sierk Hoeksma" "Hoeksma"
   echo "Assigning roles"
   rancher_add_role "Peter" "cluster-owner"
   rancher_add_role "Yanick" "cluster-owner"
   rancher_add_role "Sierk"  "cluster-owner"
   METALLB_START=45 #Special within dev network
   install "metallb,longhorn,drone-io"
 fi
 rancher_logout
}

umod_yan-cluster(){
 echo "Removing cluster"
 load_source "functions"
 CLUSTER="yan"
 rancher_destroy_cluster
 echo "Removing users"
 rancher_remove_role "Peter"
 rancher_remove_role "Yanick"
 rancher_remove_role "Sierk"
 rancher_logout
}

#We don't support upgrade
upgrade_yan-cluster(){
return 0
}