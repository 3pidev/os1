mod_joget(){
    kubectl create namespace joget
    kubectl apply -n joget -f https://k8s.io/examples/application/mysql/mysql-pv.yaml
    MYSQL_ROOT_PASSWORD=$OS1_PWD
    kubectl apply -n joget -f https://k8s.io/examples/application/mysql/mysql-deployment.yaml
    kubectl apply -n joget -f https://dev.joget.org/community/download/attachments/66814344/joget-dx7-tomcat9-deployment.yaml #?version=2&modificationDate=1648114028000&api=v2
}

umod_joget(){
  kubectl delete -n joget -f https://dev.joget.org/community/download/attachments/66814344/joget-dx7-tomcat9-deployment.yaml #?version=2&modificationDate=1648114028000&api=v2
  kubectl delete -n joget -f https://k8s.io/examples/application/mysql/mysql-deployment.yaml
  kubectl delete -n joget -f https://k8s.io/examples/application/mysql/mysql-pv.yaml
  kubectl delete namespace joget 
}