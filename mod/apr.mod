mod_apr(){
docker run \
  -d \
  --name apr \
  --pull=always \
  -v $DATA_DIR/apr:/app/db \
  -p 19284:1324 \
  --restart unless-stopped \
  3pidev/apr:latest
  if [ "$1" != "no_proxy" ]; then
    load_source "proxy"
    local_proxy "apr" "19284"
  fi
  docker run \
  -d \
  --name apr.dev \
  --pull=always \
  -v $DATA_DIR/apr.dev:/app/db \
  -p 19285:1324 \
  --restart unless-stopped \
  3pidev/apr:dev
  if [ "$1" != "no_proxy" ]; then
    load_source "proxy"
    local_proxy "apr.dev" "19285"
  fi
  }

  umod_apr(){
      docker rm --force apr
      docker rm --force apr.dev
  }

  upgrade_apr(){
    umod_apr
    mod_apr "no_proxy"
  }