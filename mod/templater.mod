#!/bin/bash
#
# Very simple templating system that replaces {{VAR}} by the value of $VAR.
# Supports default values by writting {{VAR=value}} in the template.
#
# Copyright (c) 2017 Sébastien Lavoie
# Copyright (c) 2017 Johan Haleby
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# See: https://github.com/johanhaleby/bash-templater
# Version: https://github.com/johanhaleby/bash-templater/commit/5ac655d554238ac70b08ee4361d699ea9954c941

# Replaces all {{VAR}} by the $VAR value in a template file and outputs it

templater_patch(){
    if [ -d "${1}" ] ; then
        for filename in ${1}/*.*; do
            templater "$filename" "$2" "$3" "$4" "$5" > templater.patch
            ${3} mv templater.patch "$filename"
        done
    else
        templater "$1" "$2" "$3" "$4" "$5" > templater.patch
        ${3} mv templater.patch "$1"
    fi
}

templater() {
    template="${1}"
    local config_file=${2:-<none>}
    if [ -z "$2" ] && [ -f ".templater.conf" ]; then
      config_file=".templater.conf"
    fi
    local doSudo=${3}
    local print_only=${4:-false}
    local silent=${5:-true}

    local vars=$($doSudo grep -oE '\{\{[A-Za-z0-9_]+\}\}' "${template}" | sort | uniq | sed -e 's/^{{//' -e 's/}}$//')

    if [[ -z "$vars" ]]; then
        if [ "$silent" == "false" ]; then
            echo "Warning: No variable was found in ${template}, syntax is {{VAR}}" >&2
        fi
    fi

    # Load variables from file if needed
    if [ "${config_file}" != "<none>" ]; then
        if [[ ! -f "${config_file}" ]]; then
          echo "The file ${config_file} does not exists" >&2    
          return 1
        fi
        source "${config_file}"
    fi    

    var_value() {
        eval echo \$$1
    }

    local replaces=""

    # Reads default values defined as {{VAR=value}} and delete those lines
    # There are evaluated, so you can do {{PATH=$HOME}} or {{PATH=`pwd`}}
    # You can even reference variables defined in the template before
    local defaults=$($doSudo grep -oE '^\{\{[A-Za-z0-9_]+=.+\}\}' "${template}" | sed -e 's/^{{//' -e 's/}}$//')

    for default in $defaults; do
        var=$(echo "$default" | grep -oE "^[A-Za-z0-9_]+")
        current=`var_value $var`

        # Replace only if var is not set
        if [[ -z "$current" ]]; then
            eval $default
        fi

        # remove define line
        replaces="-e '/^{{$var=/d' $replaces"
        vars="$vars
    $current"
    done

    vars=$(echo $vars | sort | uniq)

    if [[ "$print_only" == "true" ]]; then
        for var in $vars; do
            value=`var_value $var`
            echo "$var = $value"
        done
        exit 0
    fi

    # Replace all {{VAR}} by $VAR value
    for var in $vars; do
        value=$(var_value $var | sed -e "s;\&;\\\&;g" -e "s;\ ;\\\ ;g") # '&' and <space> is escaped 
        if [[ -z "$value" ]]; then
            if [ $silent == "false" ]; then
                echo "Warning: $var is not defined and no default is set, replacing by empty" >&2
            fi
        fi

        # Escape slashes
        value=$(echo "$value" | sed 's/\//\\\//g');
        replaces="-e 's/{{$var}}/${value}/g' $replaces"    
    done

    local escaped_template_path=$(echo $template | sed 's/ /\\ /g')
    if [ -n "$replaces" ]; then
      eval $doSudo sed "$replaces" "$escaped_template_path"
    else
     $doSudo cat "$escaped_template_path"
    fi
}