
umod_short-url(){
local _TMP=/tmp/short-url
mkdir -p $_TMP
short_url_files "$1" "$2" "$3" "$4" "$5" "$6" "$7" "$_TMP" "no"
cd $_TMP
docker-compose down
cd
sudo systemctl restart openresty
rm -rf $_TMP
}

mod_short-url(){
local _TMP=/tmp/short-url
mkdir -p $_TMP
short_url_files "$1" "$2" "$3" "$4" "$5" "$6" "$7" "$_TMP" "yes"
cd $_TMP
docker-compose up -d
cd
sudo systemctl restart openresty
rm -rf $_TMP
}

short_url_files(){
 local short_url_domain=${1:-${SHORT_URL_DOMAIN:-$DOMAIN}} #public for template
 local short_url_email=${2:-${SHORT_URL_EMAIL:-no-reply}}
 local short_url_pwd=${3:-${SHORT_URL_PWD:-$OS1_PWD}}
 local short_url_host=${4:-${SHORT_URL_HOST:-smtp.$short_url_domain}}
 local short_url_smtp=${5:-${SHORT_URL_SMTP:-465}}
 local short_url_admin=${6:-${SHORT_URL_ADMIN:-admin@$short_url_domain}}
 local short_url_site=${7:-${SHORT_URL_SITE:-$short_url_domain}}
 local _TMP=${8}
 local install=${9}

#The Composer file creating all required services
get_files "docker-compose.yml" "$_TMP" "{{FILES_PATH}}/short-url" "" "yes"
#Envrionment file
get_files ".env" "$_TMP" "{{FILES_PATH}}/short-url" "" "yes"

#get the proxy
local loc_rproxy="/usr/local/openresty/nginx/conf/rproxy/http"
local dest="${loc_rproxy}/available"
local file="20-short-url.conf"
if [ $install == "yes" ]; then
  get_files "$file" "$dest" "{{FILES_PATH}}/short-url" "sudo" "yes"
  sudo ln -sf ${dest}/$file ${loc_rproxy}/enabled/$file
  echo "$short_url_domain" | sudo tee -a /usr/local/openresty/nginx/domains >/dev/null
else
  sudo rm -rf ${loc_rproxy}/enabled/$file ${dest}/$file
  sudo sed -i "/$short_url_domain/d" /usr/local/openresty/nginx/domains
fi
}
