mod_lowcoder(){
sudo mkdir -p $DATA_DIR/lowcoder
get_files "docker-compose.yml" $config_dir/lowcoder "{{FILES_PATH}}/lowcoder" "" "yes"
cd $config_dir/lowcoder
docker-compose up -d
cd 
load_source "proxy"
local_proxy "lowcoder" "3000"
local_proxy "openblocks" "3000"
}

umod_lowcoder(){
  cd $config_dir/lowcoder
  docker-compose down
  cd 
  sudo rm -rf $config_dir/lowcoder
}

upgrade_lowcoder(){
  get_files "docker-compose.yml" $config_dir/lowcoder "{{FILES_PATH}}/lowcoder" "" "yes"
  cd $config_dir/lowcoder
  docker-compose pull
  docker-compose down
  docker-compose up -d
  cd
}