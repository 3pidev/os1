#Install kubectl aliases
#https://github.com/ahmetb/kubectl-aliases
#https://github.com/ahmetb/kubectx
#https://github.com/jonmosco/kube-ps1
#hey http load testing

#whois installs mkpasswd, needed to create linux password: linux_pwd_hash=$(echo $linux_pwd | mkpasswd -m sha-512 -s)

ctx_version=0.9.4
mod_tools(){
  sudo apt-get install -y httpie unzip whois
  curl -sLn https://raw.githubusercontent.com/ahmetb/kubectl-aliases/master/.kubectl_aliases -o ~/.kubectl_aliases
  # Install prompt
  curl -sLn  https://github.com/jonmosco/kube-ps1/archive/master.zip -o master.zip
  unzip -q master.zip
  rm master.zip
  sudo mv kube-ps1-master /usr/local/
  sed -i '/# Begin kubectl_aliases/,/# End kubectl_aliases/d' ~/.bash_aliases 
  touch ~/.bash_aliases
  cat >> ~/.bash_aliases <<EOF
  # Begin kubectl_aliases
  [ -f ~/.kubectl_aliases ] && source <(cat ~/.kubectl_aliases | sed -r 's/(kubectl.*) --watch/watch \1/g')
  # set a fancy prompt (non-color, unless we know we "want" color)
  case "\$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
  esac
  source /usr/local/kube-ps1-master/kube-ps1.sh
  if [ "\$color_prompt" = yes ]; then
    PS1='\${debian_chroot:+(\$debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:[\W\$(kube_ps1)]\\$ '
  else
    PS1='\${debian_chroot:+(\$debian_chroot)}[\u@\h:\W\$(kube_ps1)]\\$ '
  fi
  
  line=''
  for FILE in ~/.kube/config-* ;do
    if [ -n "\$FILE" ] ;then
      line="\$line:\$FILE"
    else
      line="\$FILE"
    fi
  done
  if [ -n "\$line" ] ;then
    export KUBECONFIG="\$line"
  fi
  # End kubectl_aliases
EOF

sudo bash << END_SUDO
# Install kubectx (Switch between Kubernetes contexts/namespaces)
curl -sSL http://github.com/ahmetb/kubectx/archive/v${ctx_version}.tar.gz | sudo tar -C /usr/local/ -xz
sudo ln -sf /usr/local/kubectx-${ctx_version}/kubectx /usr/local/bin/kubectx
sudo ln -sf /usr/local/kubectx-${ctx_version}/kubens /usr/local/bin/kubens

#Install hey traffic generator
sudo curl -sSL https://hey-release.s3.us-east-2.amazonaws.com/hey_linux_${ARCH} -o /usr/local/bin/hey
sudo chmod +rx /usr/local/bin/hey

#Installing KubeCtl and Helm on the local machine
#snap install kubectl --classic
curl -sLO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/$ARCH/kubectl" \
  && chmod +x kubectl && sudo mv kubectl /usr/local/bin/kubectl 
#snap install helm --classic
 curl -s https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
# To prevent adding sudo for all commands we do it once
END_SUDO
}

umod_tools(){
  sudo apt-get remove -y httpie
  sed -i '/# Begin kubectl_aliases/,/# End kubectl_aliases/d' ~/.bash_aliases
sudo bash << END_SUDO
  rm -rf  /usr/local/kubectx-${ctx_version} /usr/local/kube-ps1-master
  rm -f  /usr/local/bin/kubectx /usr/local/bin/kubens /usr/local/bin/hey
  #Remove KubeCtl and Helm on the local machine
  #snap remove kubectl
  rm -rf /usr/local/bin/kubectl
  #snap remove helm
  rm -rf /usr/local/bin/helm
# To prevent adding sudo for all commands we do it once
END_SUDO
}