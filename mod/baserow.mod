mod_baserow(){
docker run \
  -d \
  --name baserow \
  -e BASEROW_PUBLIC_URL=https://baserow.$DOMAIN \
  -v $DATA_DIR/baserow:/baserow/data \
  -p 19280:80 \
  -p 19243:443 \
  --restart unless-stopped \
  baserow/baserow:latest
  load_source "proxy"
  local_proxy "baserow" "19280"
  }

  umod_baserow(){
      docker rm --force baserow
  }