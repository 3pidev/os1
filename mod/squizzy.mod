mod_squizzy(){
docker run \
  -d \
  --name squizzy \
  --pull=always \
  -v $DATA_DIR/squizzy/db:/squizzy/backend/db \
  -v $DATA_DIR/squizzy/sessions:/squizzy/backend/sessions \
  -v $DATA_DIR/squizzy/config:/squizzy/backend/config \
  -p 19286:3900 \
  --restart unless-stopped \
  3pidev/squizzy:latest
  if [ "$1" != "no_proxy" ]; then
    load_source "proxy"
    local_proxy "squizzy" "19286" "" "nl" "32" "10s" "6000s" "250M"
  fi
  }

  umod_squizzy(){
      docker rm --force squizzy
  }

  upgrade_squizzy(){
    umod_squizzy
    mod_squizzy "no_proxy"
  }