#!/bin/bash

# ------------------------------------------------------------------
# [S.J.Hoeksma] os1.sh
# Automated deployment script for our OpenStack/Kubernetes enviroment
# ------------------------------------------------------------------

readonly PROGNAME=$(basename $0)
# --- Contants ---------------------------------------------------
#Check if current directory contains run and we have constants, we assume local install
if [ -f $PWD/run.sh ] &&  [ -f $PWD/mod/constants.mod ] ; then
  OS1_REPO=$PWD
fi
#URL towards the raw file of this project
if [ -z "$OS1_REPO" ];then
  OS1_REPO="https://gitlab.com/3pidev/os1/-/raw/master"
fi
VERSION="0.1.1"
SUBJECT=3pidev-os1
LOCK_FILE=/tmp/$SUBJECT.lock
TMP=/tmp/$SUBJECT
USAGE="Usage: $PROGNAME -iephvcu action
 -c <config file> to load
 -r reset lock
 -h show this help
 -v show version number
 -s <setting> an option for one of the modules
 -e <option> is excluded from standard install
 -i <option> is added to install
 -p password used for installation (can be encoded)
 -u user the linux user used during installationg

Currently we support the following:
 install <list of software packages>      : Install the software module(s)
 uninstall <list of software packages>    : Uninstall the software module(s)
 upgrade <list of software packages>      : Upgrade the software module(s)
 backup <list of software packages>       : Backup the data of software module(s)
 restore <list of software packages>      : Restore the data of software module(s)
 encode <value>                           : Encode a value
 decode <encode>                          : Decode a value
 create <name> <network> <Ctrl size> <Workers size[.node]> : Add a cluster to OpenStack and Rancher
 destroy <name> <network>                 : Remove a cluster from OpenStack and Rancher
 add <name>                               : Add node
 remove <name>                            : Remove node
 token <domain> <timeout>                 : Get access token
 drone_io                                 : Function used by drone plugin
 kubectl <cluster> <filename>             : Download the kubeconfig file for cluster
 welcome                                  : Show urls to login and username and password
"

#List with file to exclude
exclude_list=""
#List with software to install
include_list=""
#List with settings
settings_list=""
#Password if used
password=""
#user
user=$(whoami)
#User config files
config_dir="~/.os1"
eval config_dir="$config_dir"
config_files=""

#Get the architecture of machine
ARCH=""
case $(uname -m) in
    i386)   ARCH="386" ;;
    i686)   ARCH="386" ;;
    x86_64) ARCH="amd64" ;;
    aarch64) ARCH="arm64" ;;
    arm)    dpkg --print-architecture | grep -q "arm64" && ARCH="arm64" || ARCH="arm" ;;
esac
PLATFORM=$(uname)

# --- Options processing -------------------------------------------
if [ "$#" -eq "0" ] ;then
    echo "$USAGE"
    exit 1;
fi

while getopts ":s:i:p:u:e:c:vhr" optname
  do
    case "$optname" in
      "v")
        echo "Version $VERSION"
        exit 0
        ;;
      "r")
        rm -rf $LOCK_FILE $TMP
        ;;
      "c")
        config_files="$config_files $OPTARG"
      ;;
      "p")
        if [ -z "$OPTARG" ] ;then
          read -s -p "Please enter password: " password
        else
           password=$OPTARG
        fi
        if [ "${password:0:1}" == "~" ] ;then
          epassword=$(echo "${password:1}" | base64 --decode 2>/dev/null)
          if [ "$?" == "0" ] ;then
             password=$epassword
          fi
        fi
      ;;
      "u")
        user="$OPTARG"
      ;;
      "e")
        if [ -z "$exclude_list" ]; then
          exclude_list="$OPTARG"
        else
          exclude_list="${exculde_list} $OPTARG"
        fi
        ;;
      "s")
        if [  -z "$settings_list" ]; then
          settings_list="$OPTARG"
        else
          settings_list="${settings_list} $OPTARG"
        fi
        #Also set it as global VAR
        eval $OPTARG
        ;;
      "i")
        if [ -z "$include_list" ]; then
          include_list="$OPTARG"
        else
          include_list="${include_list} $OPTARG"
        fi
        ;;
      "h")
        echo "$USAGE"
        exit 0
        ;;
      ":")
        echo "No argument value for option $OPTARG"
        exit 0
        ;;
      *)
        echo "Unknown error while processing options"
        exit 0
        ;;
    esac
  done

shift $(($OPTIND - 1))

# --- Helper Functions --------------------------------------------
#Function to remotely load additional source files
#param 1:Name of source
sources=""
load_source(){
  local file
  local list=$(echo $1 | sed 's/,/ /g')
  local path="${2:-${MOD_PATH}}"
  for file in $list ;do
    if [[ ${sources} != *"${file},"* ]] ;then #Check if file is not in sources
      sources="${sources}${file},"
      if [[ ${OS1_REPO} != *"https:"* ]] ;then #Check if file is local dir
        file="${OS1_REPO}/${path}/${file}.mod"
        if [ -f "$file" ]; then
          #echo "Loading source: $file"
          source ${file}
        else
          echo "Source not found: $file"
          exit 1
        fi
      else
        file="${OS1_REPO}/${path}/${file}.mod"
        #echo "Loading source: $file"
        curl -sfL ${file}  -o $TMP/${SUBJECT}.mod
        if [ -f "$TMP/${SUBJECT}.mod" ]; then
            source $TMP/${SUBJECT}.mod
            rm $TMP/${SUBJECT}.mod
        else
          echo "Source not found: $file"
          exit 1
        fi
      fi
    fi
  done
}

#function to copy file to tmp directory
#param 1:List with files
#param 2:Destination directory (default /tmp/$SUBJECT)
#param 3:Source directroy (default files)
#param 4:should we do this as sudo
get_files() {
  local file
  local list=$(echo $1 | sed 's/,/ /g')
  local dest="${2:-$TMP}"
  local path=${3:-$FILES_PATH}
  path=$(echo $path | sed "s|{{FILES_PATH}}|${FILES_PATH}|g")
  local SUDO=$4
  local template=${5:-no}
  local err=0
  if [ "$template" == "yes" ] ;then
    load_source "templater"
  fi

  for file in $list ;do
      src="${OS1_REPO}/${path}/${file}"
      $SUDO mkdir -p ${dest}
      if [[ ${OS1_REPO} != *"https:"* ]] ;then #Check if file is local dir
         $SUDO cp -f $src ${dest}/${file}
      else
        #echo "Loading source: $file"
        $SUDO curl -sfL ${src} -o  ${dest}/${file}
      fi
      local res=$?
      err=$(( err + res ))
      if [ "$template" == "yes" ] && [ -s "${dest}/${file}" ];then
        local result=$(templater "${dest}/${file}" "" "$SUDO")
        echo "$result" | $SUDO tee ${dest}/${file} >/dev/null
      fi
  done
  return $err
}

# Function to get value from settings list
setting=""
setting_count=0
get_setting() {
  setting="" #Set the global value to empty
  local arr=(${settings_list})
  local i
  local values=""
  local found=0
  for i in "${arr[@]}" ;do
    local kv=(${i//=/ })
    if [ "$kv" == "${1}" ] ;then
      found=1
      if [ -z "$values" ] ;then
      values="${kv[1]}"
      else
        values="${values} ${kv[1]}"
      fi
    fi
  done
  setting=$(echo $values | sed 's/,/ /g')
  setting_count=0
  for i in $setting ;do
    setting_count=$((setting_count + 1 ))
  done
  if [ "$found" -eq "0" ] ;then
    return 1
  fi
  return 0
}

#Function to show time out message
timeout() {
 local secs=$1
  local msg="$2"
  if [ -n "$msg" ] ;then
    msg="${msg} "
  fi
  printf "\r${msg}wait ${secs} "
  while [ $secs -gt 0 ]; do
    sleep 1
    secs=$((secs - 1 ))
    printf "\r${msg}wait ${secs} "
  done
  echo
}

#Check is function is loaded
is_loaded () {
local func=$1
if [ -n "$(LC_ALL=C type -t $func)" ] && [ "$(LC_ALL=C type -t $func)" = function ]; then
  return 0;
else
 return 1;
fi
}

#Get a json value
#param 1: The key to find
#param 2: Array pos defaults to value (2)
jsonValue() {
 KEY=$1
 num=${2:-2}
 awk -F"[,:}]" '{for(i=1;i<=NF;i++){if($i~/'$KEY'\042/){print $(i+1)}}}' | tr -d '"' | sed -n ${num}p
}

#Load a module config if exists
load_config(){
 local mod=${1}
 if [ -n "$mod" ]; then
   mod="-$mod"
 fi
 local cluster=${2:-${CLUSTER:-${DEFAULT_CLUSTER:-local}}}
 file="$config_dir/config${mod}"
 if [ -f "$file" ]; then
  source "$file"
 fi

 #Check if there is a cluster specific
 if [ -n "$cluster" ]; then
   file="$config_dir/config-${cluster}${mod}"
   if [ -f "$file" ]; then
    source "$file"
   fi
 fi
}

#Install the software module
install(){
  local soft
  mkdir -p $config_dir/installed
  for soft in $(echo ${1:-$include_list} | sed 's/,/ /g' |sed 's/^[ \t]*//;s/[ \t]*$//') ;do
    if [[ ("$soft" =~ ^($exculde_list)$) ]] ;then
     echo "Skipping $soft"
    else
      load_source "$soft"
      if is_loaded "mod_${soft}" ;then
         load_config "$soft"
         echo "Installing $soft"
         mod_${soft} "$2" "$3" "$4" "$5" "$6"
         touch $config_dir/installed/${soft}
      else
        echo "Module not found ${soft}"
      fi
    fi
  done
}

#Backup data of the software module
backup(){
  local soft
  for soft in $(echo ${1:-$include_list} | sed 's/,/ /g' |sed 's/^[ \t]*//;s/[ \t]*$//') ;do
    if [[ ("$soft" =~ ^($exculde_list)$) ]] ;then
     echo "Skipping $soft"
    else
      load_source "$soft"
      if is_loaded "backup_${soft}" ;then
         load_config "$soft"
         echo "Backup $soft"
         backup_${soft} "$2" "$3" "$4" "$5" "$6"
      else
        echo "Module not found ${soft}"
      fi
    fi
  done
}

#restore data of the software module
restore(){
  local soft
  for soft in $(echo ${1:-$include_list} | sed 's/,/ /g' |sed 's/^[ \t]*//;s/[ \t]*$//') ;do
    if [[ ("$soft" =~ ^($exculde_list)$) ]] ;then
     echo "Skipping $soft"
    else
      load_source "$soft"
      if is_loaded "restore_${soft}" ;then
         load_config "$soft"
         echo "Restore $soft"
         restore_${soft} "$2" "$3" "$4" "$5" "$6"
      else
        echo "Module not found ${soft}"
      fi
    fi
  done
}

#Uninstall a module
uninstall(){
  local soft
  for soft in $(echo ${1:-$include_list} | sed 's/,/ /g' |sed 's/^[ \t]*//;s/[ \t]*$//') ;do
    if [[ ("$soft" =~ ^($exculde_list)$) ]] ;then
     echo "Skipping $soft"
    else
      load_source "$soft"
      if is_loaded "umod_${soft}" ;then
         load_config "$soft"
         echo "Uninstalling $soft"
         umod_${soft} "$2" "$3" "$4" "$5" "$6"
         rm -f $config_dir/installed/${soft}
      fi
    fi
  done
}

is_installed(){
 if [ -f "$config_dir/installed/$1" ]; then
   return 0
 else
   return 1
 fi
}

#Uninstall a module
upgrade(){
  local soft
  for soft in $(echo ${1:-$include_list} | sed 's/,/ /g' |sed 's/^[ \t]*//;s/[ \t]*$//') ;do
    if [[ ("$soft" =~ ^($exculde_list)$) ]] ;then
     echo "Skipping $soft"
    else
      load_source "$soft"
      if is_loaded "upgrade_${soft}" ;then
        echo "Upgrading $soft"
        upgrade_${soft} "$2" "$3" "$4" "$5" "$6"
      elif is_loaded "mod_${soft}" ;then
        echo "Upgrading $soft"
        if is_loaded "umod_${soft}" ;then
         umod_${soft} "$2" "$3" "$4" "$5" "$6"
        fi
        mod_${soft} "$2" "$3" "$4" "$5" "$6"
      else
        echo "Module not found ${soft}"
      fi
    fi
  done
}

#Check if item is contained in list
contains() {
    [[ $1 =~ (^|[[:space:]])$2($|[[:space:]]) ]] && return 0 || return 1
}

# --- Locks & Temp ------------------------------------------------
if [ -f "$LOCK_FILE" ]; then
   echo "Script is already running"
   exit 1
fi

#Create lockfile and temp directory
trap "rm -rf $LOCK_FILE $TMP" EXIT
mkdir -p $TMP
touch $LOCK_FILE

# --- Body --------------------------------------------------------

#Check if there was a overwrite for the repo
if get_setting "OS1_REPO" ;then
  OS1_REPO=$setting
fi

#Check if we should overwrite modules path
MOD_PATH="mod"
if get_setting "MODULE_PATH" ;then
  MOD_PATH=$setting
fi

#Check if we should overwrite files path
FILES_PATH="files"
if get_setting "FILES_PATH" ;then
  FILES_PATH=$setting
fi

HOME_DIR=$(eval echo "~")

#BASE IP OF the host
if [ "$(uname)" == "Darwin" ] ;then
SERVER_IP=$(ip -c a | grep "inet " | grep -Fv 127.0.0.1 | awk '{print $2}')
else
SERVER_IP=$(hostname -I | awk '{print $1}')
fi
BASE_IP=$(echo $SERVER_IP | awk -F. '{print $1 "." $2 "." $3}')

#Load the module constants and userconfig for project
load_config
for file in $config_files ;do
  load_config $file
done
load_source "constants"

#Name of default cluster
DEFAULT_CLUSTER=${DEFAULT_CLUSTER:-local}
if get_setting "default_cluster" ;then
  DEFAULT_CLUSTER="$setting"
fi
#Get the cluster name using tools
if get_setting "cluster" ; then
  CLUSTER=$settings
else
  CLUSTER=$DEFAULT_CLUSTER
  if [ -f "/usr/bin/kubectx" ]; then
    CLUSTER=$(kubectx -c 2>/dev/null)
  fi
fi

if [ "$CLUSTER" != "$DEFAULT_CLUSTER" ] ; then
  CLUSTER_DOMAIN="${CLUSTER}.${DOMAIN}"
else
  CLUSTER_DOMAIN=$DOMAIN
fi

#SSH command to execute
SSH="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ${SSH_OPTIONS} -q ${user}@"

#Explode the lists
include_list=$(echo $include_list | sed 's/,/ /g' | sed 's/^[ \t]*//;s/[ \t]*$//')
exculde_list=$(echo $exculde_list | sed 's/,/ /g' | sed 's/^[ \t]*//;s/[ \t]*$//')
backup_list=$(echo $backup_list | sed 's/,/ /g' | sed 's/^[ \t]*//;s/[ \t]*$//')
list_1=$(echo $2 | sed 's/,/ /g' |sed 's/^[ \t]*//;s/[ \t]*$//')

#  SCRIPT LOGIC GOES HERE
case "$1" in

  "install") echo "Installing software module(s)"
    if [ -n "$list_1" ]; then
      include_list=$list_1
    elif [ -z "$password" ] ;then
      echo "Full install requires linux password, please specifiy using -p option"
      exit 1
    fi
    install
  ;;

  "backup") echo "Backing up data of software module(s)"
    if [ -n "$list_1" ]; then
      include_list=$list_1
    else
     include_list=${backup_list:-$include_list}
    fi
    backup
  ;;

  "restore") echo "Restore up data of software module(s)"
    if [ -n "$list_1" ]; then
      include_list=$list_1
    else
     echo "Please specify the modules to restore"
     exit 1
    fi
    restore
  ;;

  "uninstall") echo "Uninstall software module(s)"
    if [ -n "$list_1" ]; then
      include_list=$list_1
    else
      #Reverse the include list so uninstall works
      list=""
      for soft in $include_list ;do
        list="${soft} ${list}"
      done
      include_list=$list
    fi
    uninstall
  ;;

  "upgrade") echo "Upgrade software module(s)"
    if [ -n "$list_1" ]; then
      include_list=$list_1
    fi
    upgrade
  ;;

  "encode")
   if [ -z "$2" ]; then
     read -s -p "Please enter secret to encode: " secret
   else
     secret=$2
   fi
   echo "~$(echo '$secret' | base64)"
  ;;

  "decode")
    if [ "${2:0:1}" == "~" ] ;then
       secret=${2:1}
     else
      echo "Not a secret"
      exit 1
     fi
     echo "$secret" | base64 --decode
  ;;

  "create") echo "Add cluster"
    load_source "functions"
    rancher_create_cluster "$2" "$3" "$4" "$5" "$6" "$7"
    rancher_logout
  ;;

  "destroy") echo "Destroy cluster"
   load_source "functions"
   rancher_destroy_cluster "$2" "$3" "$4" "$5" "$6" "$7"
   rancher_logout
  ;;

  "add") echo "Adding node(s) $2"
    load_source "nodes"
    mod_nodes "$2"
   ;;

   "reboot") echo "Rebooting node(s) $2"
    load_source "reboot"
    mod_reboot "$2"
   ;;

  "remove") echo "Removing node(s) $2"
    load_source "nodes"
    umod_nodes $2
   ;;

  "token")
    load_source "functions"
    if rancher_login "$2" "$3" "$4" "${5:-720}" "${6:-no}" ;then
      echo "$RANCHER_TOKEN"
    else
      #Exit with error
      exit 1
    fi
  ;;

  "kubectl")
   load_source "functions"
   rancher_kubectl "${2:-$CLUSTER}" "$3" "$4" "$DOMAIN" "yes"
   ;;

  "print")
    if [ -n "$2" ] ;then
      echo "Value for $2=${!2}"
    fi
  ;;

  "run")
    load_source "functions"
    $2 "$3" "$4" "$5" "$6" "$7" "$8" "$9"
  ;;

  "get_files")
    get_files "$2" "$3" "$4" "$5" "$6" "$7" "$8" "$9"
  ;;

  "drone_io")
    load_source "functions"
    drone_io_deploy
  ;;

  "welcome")
    install "welcome"
   ;;

  "installed")
   if is_installed "$2" ;then
     echo "yes"
   else
     echo "no"
   fi
   ;;

  *)
    if [ -n "$1" ]; then
      include_list=$(echo $1 | sed 's/,/ /g')
      echo "Installing software module(s)"
      install
    else
      echo "$USAGE"
    fi
  ;;

esac
