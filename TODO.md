# TODO
* Implement API - Krakend , gravitti.io
* Volumes for Ranger
* Backup using Velero or Rancher
* Functions to read/save variables in global or cluster or module
* Minio multi user
* Data Sience & protal
* KubeFlow
* HAProxy
* Robotics: https://ubuntu.com/blog/exploring-ros-2-with-kubernetes
* Pub / Sub function , KubeMQ [, RabbitMQ] https://kafka.apache.org/
* Dashboarding graphana
* Logging console instead of echo, can be disabled
* Access point on the server https://raspberrypi.stackexchange.com/questions/109425/ubuntu-server-18-wifi-hotspot-setup
* Website staticstics https://matomo.org/matomo-on-premise/
* BI https://www.metabase.com/start/ or redash
* Develop driver for Metabase using JSON files based on existing HTTP
* Self hosted git. (https://sysadmins.co.za/ci-cd-with-drone-and-gitea-using-kubernetes-on-civo-post-1/)
* Script adding kubecost
* Add ingress options to do authentication (keycloak)
* Habor as private docker registry
* Change longhorn to backup data to minio
* Expose loadbalancer IPs on public and proxy
* Robotics application (https://github.com/kelaberetiv/TagUI) or (UIPath payed)
* Automated KeyCloack installation
* https://spark.apache.org/
* Integrate KeyCloak into ingress allowing user to access urls based on groups
* AppGyver (KeyCloak) & API
* SWAP file during install larger
* KUBEEDGE: https://github.com/kubeedge/kubeedge
* https://labelstud.io/guide/
* create log file which packages are installed, delete on remove
* create a docker cluster for k3s - Keep table with server name and port - in function library https://github.com/rancher/k3d https://github.com/rancher/k3c

# BUG
* Test new version of Gitlab integration, current helm chart not working
* Jboss/KeyCloack is replaced by quay.io/keycloak/keycloak

# DOC
* Create PowerPoint explaining concepts
* Doc that kubectl should have port 6443 on outside world
* Add public api list to documenation
